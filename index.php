<?php
//网站路径
define('SITE_PATH', getcwd());
//应用名称
define('APP_NAME', 'MoCha');
//应用路径
define('APP_PATH', './MoCha/');
//框架路径
define('THINK_PATH','./_Core/');
/* 扩展目录*/
define('EXTEND_PATH', APP_PATH . 'Extend/');
//缓存路径
define("RUNTIME_PATH", './Data/');
//配置文件目录
//define('CONF_PATH', './Data/Config/');
//ENGINE_PATH
define('ENGINE_PATH', EXTEND_PATH. 'Engine/');
//APP ENGINE
define('ENGINE_NAME','cluster');
define('IO_NAME','auto');
//开启调试模式
define("APP_DEBUG", true);
//载入框架核心文件
require THINK_PATH . 'ThinkPHP.php';
?>