<?php

class Increment {
	public $top;
	
	public function __construct() {
		$this->top = new Top();
	}
	
	/**
	 * 查询应用为用户开通的增量消息服务
	 * @param $nicks 查询用户的昵称。当为空时通过分页方式查询appkey开通的所有用户。可传入数组或是字符串，最多填入20个昵称。
	 * @param $type 主动通知的消息主题。该主题必须是应用订阅的主题，如果应用未订阅该主题，则系统会自动过滤掉该主题。各个主题间用";"分隔。 如果不填写，会默认开通应用所订阅的所有消息。
	 * @param $page_no 页码。此参数只有当nicks为空时起作用。
	 * @param $page_size 每页条数。此参数只有当nicks为空时起作用。
	 */
	public function get_customers($nicks = "", $type = "get,notify", $page_no = 1, $page_size = 200) {
		$fields = "nick,created,status,type";
		$where = " page_no=$page_no and page_size=$page_size";
		if ($nicks) {
			if (is_array($nicks)) $nicks = implode(',', $nicks);  //将数组转换成字符串
			$where .=" and nicks=$nicks";
		}
		if ($type) $where .=" and type=$type";
		
		$params['ql'] = "select $fields from increment.customers where".$where;
		$customers = $this->top->tql($params);
		
		return $customers;
	}
	
	/**
	 * 开通增量消息服务
	 * @param $type 需要开通的功能。值可为get,notify,syn，分别表示增量api取消息，主动发送消息和同步数据功能。当重复开通时，只会在已经开通的功能上，开通新的功能，不会覆盖旧的开通。
	 * @param $topics 主动通知的消息主题。该主题必须是应用订阅的主题，如果应用未订阅该主题，则系统会自动过滤掉该主题。各个主题间用";"分隔。 如果不填写，会默认开通应用所订阅的所有消息。
	 * @param $status 与topics相对应的消息状态。各个消息主题间用";"分隔，各个状态间用","分隔，消息主题必须和topics一致。如果为all时，表示开通应用订阅的所有的消息。 如果不填写,会默认开通应用所订阅的所有消息。
	 */
	public function permit_customer($nick, $type = "get,notify", $topics = "item;trade;refund", $status = "all;all;all") {
		$fields = "type,topics,status";
		$values = "#type#,$topics,#status#";
		
		$session = M('User')->getFieldByNick($nick, 'sessionkey');
		$params['ql'] = "insert into taobao.increment.customer.permit (type,topics,status) values (#type#,$topics,#status#)";
		$params['type'] = $type;
		$params['status'] = $status;
		$resp = $this->top->tql($params, $session);
		$nick = $resp['app_customer']['nick'];
		
		return $nick;
	}
	
	/**
	 * 关闭用户的增量消息服务
	 * @param $nick 用户昵称
	 * @param $type 需要关闭用户的功能。用户关闭相应功能前,需应用已为用户经开通了相应的功能。在关闭时，type里面的参数会根据应用订阅的类型进行相应的过虑。
	 */
	public function stop_customer($nick, $type = "get,notify") {
		$session = D('User')->nick2session($nick);
		$params['ql'] = "delete from taobao.increment.customer.stop where nick=$nick and type=$type";
		$customers = $this->top->tql($params, $session);
	
		return $nick;
	}
	
	/**
	 * 获取哪些用户丢弃了消息
	 * @param int $start 指定起始时间。（格式：yyyy-MM-dd HH:mm:ss）
	 * @param int $end 指定截止时间。如果不传则为服务端当前时间（格式：yyyy-MM-dd HH:mm:ss）
	 * @param int $user_id 用户ID。
	 * @param string $types 指定多个消息类型。item,trade,refund
	 *  $start和$end的日期不能超过1小时
	 */
	public function syn_discardinfo($start, $end, $user_id = "", $types = "") {
		if ((strtotime($end)-strtotime($start)) > 3600) return false;
		$where = " start=$start and end=$end";
		if ($user_id) $where .= " and user_id=$user_id";
		if ($types) $where .= " and types=$types";
		$params['ql'] = "select user_id from taobao.comet.discardinfo.get where".$where;
		$infos = $this->top->tql($params);
		$infos = $infos['discard_info_list']['discard_info'];
		return $infos;
	}
	/**
	 * 获取商品变更通知信息
	 * @param $nick 用户昵称。自用型AppKey的昵称默认为自己的绑定昵称，$nick参数无效
	 * @param $start_modified 操作时间的最小值。如果设置了end_modified，默认为与 end_modified同一天的00:00:00，否则默认为调用接口当天的00:00:00。（格式：yyyy-MM-dd HH:mm:ss）
	 * @param $end_modified 操作时间的最大值。果设置了start_modified，默认为与start_modified同一天的23:59:59；否则默认为调用接口当天的23:59:59。（格式：yyyy-MM-dd HH:mm:ss）
	 * @param $status 商品操作状态。默认查询所有状态的数据
	 * @param $page_no 页码
	 * @param $page_size 每页条数。最大值:200
	 * start_modified和end_modified的日期必须在必须在同一天内
	 */
	public function syn_items($nick, $start_modified = "", $end_modified = "", $status = "", $page_no = 1, $page_size = 200) {
		$where = " page_no=$page_no and page_size=$page_size and nick=$nick";
		if ($status) $where .= " and status=$status";
		if ($start_modified && $end_modified) $where .= " and start_modified=$start_modified and end_modified=$end_modified";
		$params['ql'] = "select num_iid from increment.items where".$where;
		$items = $this->top->tql($params);
		$items = $items['notify_items']['notify_item'];
		
		return $items;
	}
	
	/**
	 *获取交易和评价变更通知信息
	 * @param $nick 用户昵称。自用型AppKey的昵称默认为自己的绑定昵称，$nick参数无效
	 * @param $start_modified 操作时间的最小值。如果设置了end_modified，默认为与 end_modified同一天的00:00:00，否则默认为调用接口当天的00:00:00。（格式：yyyy-MM-dd HH:mm:ss）
	 * @param $end_modified 操作时间的最大值。果设置了start_modified，默认为与start_modified同一天的23:59:59；否则默认为调用接口当天的23:59:59。（格式：yyyy-MM-dd HH:mm:ss）
	 * @param $status 交易或评价的状态。默认查询所有状态的数据
	 * @param $page_no 页码
	 * @param $page_size 每页条数。最大值:200
	 * @param $type 交易所属的类型。默认查询所有类型的数据
	 * start_modified和end_modified的日期必须在必须在同一天内
	 */
	public function syn_trades($nick, $start_modified = "", $end_modified = "", $status = "", $page_no = 1, $page_size = 200, $type = "") {
		$where = " page_no=$page_no and page_size=$page_size and nick=$nick";
		if ($status) $where .= " and status=$status";
		if ($type) $where .= " and type=$type";
		if ($start_modified && $end_modified) $where .= " and start_modified=$start_modified and end_modified=$end_modified";
		$params['ql'] = "select tid from increment.trades where".$where;
		$trades = $this->top->tql($params);
		$trades = $trades['notify_trades']['notify_trade'];
		
		return $trades;
	}
	
	/**
	 *获取退款变更通知信息
	 * @param $nick 用户昵称。自用型AppKey的昵称默认为自己的绑定昵称，$nick参数无效
	 * @param $start_modified 操作时间的最小值。如果设置了end_modified，默认为与 end_modified同一天的00:00:00，否则默认为调用接口当天的00:00:00。（格式：yyyy-MM-dd HH:mm:ss）
	 * @param $end_modified 操作时间的最大值。果设置了start_modified，默认为与start_modified同一天的23:59:59；否则默认为调用接口当天的23:59:59。（格式：yyyy-MM-dd HH:mm:ss）
	 * @param $status 交易或评价的状态。默认查询所有状态的数据
	 * @param $page_no 页码
	 * @param $page_size 每页条数。最大值:200
	 * start_modified和end_modified的日期必须在必须在同一天内
	 */
	public function syn_refunds($nick, $start_modified = "", $end_modified = "", $status = "", $page_no = 1, $page_size = 200) {
		$where = " page_no=$page_no and page_size=$page_size and nick=$nick";
		if ($status) $where .= " and status=$status";
		if ($start_modified && $end_modified) $where .= " and start_modified=$start_modified and end_modified=$end_modified";
		$params['ql'] = "select rid from increment.refunds where".$where;
		$refunds = $this->top->tql($params);
		$refunds = $refunds['notify_refunds']['notify_refund'];
		
		return $refunds;
	}
	
}

