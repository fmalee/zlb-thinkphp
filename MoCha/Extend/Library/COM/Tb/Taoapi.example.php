<?php
//当前版本:Taoapi TOP PHP SDK 2.2
header("Content-type:text/html; charset=UTF-8");
include_once '../../library/Taoapi.php';

$Taoapi_Config = Taoapi_Config::Init();
$Taoapi_Config->setCharset('UTF-8');

$Taoapi = new Taoapi;

//搜索交易公开信息(taobao.trades.get)
$Taoapi->method = 'taobao.trades.get';
$Taoapi->fields = 'seller_nick,buyer_nick,title,type,refund_status,created,iid,price,pic_path,num,tid,buyer_message,sid,shipping_type,alipay_no,payment,discount_fee,adjust_fee,snapshot_url,status,seller_rate,buyer_rate,buyer_memo,seller_memo,pay_time,end_time,modified,buyer_obtain_point_fee,point_fee,real_point_fee,total_fee,post_fee,buyer_alipay_no,receiver_name,receiver_state,receiver_city,receiver_district,receiver_address,receiver_zip,receiver_mobile,receiver_phone,consign_time,buyer_email,commission_fee,seller_alipay_no,seller_mobile,seller_phone,seller_name,seller_email,available_confirm_fee,has_postFee,received_payment,cod_fee,timeout_action_time,orders,sku_id,sku_properties_name,item_meal_name,outer_iid,outer_sku_id';
$Taoapi->iid = 'cadb024945eb330d11d8d5d558ac4e4a';
$Taoapi->seller_nick = '中性日韩非主流';

//需要更多的字段可以登陆 taoapi.com 进行配置生成
$TaobaokeData = $Taoapi->Send('get','xml')->getArrayData();

echo '<pre>';

//检测API是否遇到错误
if($Taoapi->getErrorInfo())
{
	echo "API获取数据遇到错误,错误提示是:";
	
	print_r($Taoapi->getErrorInfo());
}

//打印获取到的API数据结果
print_r($TaobaokeData);

//查看提交到API的参数
echo '提交到API的参数:';
print_r($Taoapi->getParam());

//查看提交到淘宝的URL地址
echo '提交到淘宝的URL地址:'.$Taoapi->getUrl();
echo'</pre>';

/**
* 注意事项:
* 1.上面头部的 header("Content-type:text/html; charset=UTF-8"); 必须有,如果您的程序中已经有了可以省略,
* 2.如果在Taoapi_Config.inc.php 配置文件中已经设置过编码,那么上面的这二行可以省略
*
* $Taoapi_Config = Taoapi_Config::Init();
* $Taoapi_Config->setCharset('UTF-8');
*
* 现在只要您的本身的程序正常,那么API返回的数据就是正常的,无须任何转换
*
* http://www.taoapi.com 淘宝TOP外部测试平台 值的信赖
*
* 如需要提供增值服务或购买淘客系统的可以联系旺旺: 浪子arvin QQ:8769852
*/