<?php
set_time_limit(0);
class CometTrade {
	private $Trade, $Order, $trades, $tid, $modified, $update, $where;
	
	/**
	 * 架构函数
	 * @access public
	 * @param array $items  宝贝数据
	 */
	public function __construct($items) {
		$this->Trade = D('Trade');
		$this->Order = D('Order');
	}
	
	/**
	 * 传入数据
	 * @access public
	 * @param array $trades  订单数据
	 */
	public function Import($trades) {
		//Log::write(json_encode($trades), Log::ERR);
		if (!is_array($trades)) return false;
		
		$this->trades = $this->format($trades);
		$status = $this->trades['status'];
		$this->tid = trim($this->trades['tid']);
		$this->nick = $this->trades['nick'];
		$this->modified = $this->trades['modified'];
		
		$this->update = array('modified'=>$this->modified);  //更新数据
		$this->where = array('tid'=>trim($this->tid));  //更新条件
		
		$this->$status();  //调用执行接口
	}
	
	/**
	 * 创建交易
	 */
	private function TradeCreate() {
		return $this->Trade->synTrade($this->tid, "", $this->nick);
	}

	/**
	 * 创建支付宝订单
	 */
	private function TradeAlipayCreate() {
		$this->syn("WAIT_BUYER_PAY");
	}
	
	/**
	 * 修改交易费用
	 */
	private function TradeModifyFee() {
		//TODO 修改交易费用
		$this->fields = "seller_cod_fee, total_fee, cod_fee, payment, post_fee, orders.oid, orders.total_fee, orders.payment, orders.discount_fee, orders.adjust_fee";
		return $this->Trade->synTrade($this->tid, $this->fields, $this->nick);
	}
	
	/**
	 * 关闭或修改子订单
	 */
	private function TradeCloseAndModifyDetailOrder() {
		//TODO 关闭或修改子订单 没确认
		Log::write("关闭或修改子订单:".$this->tid, Log::ERR);
		return $this->Trade->synTrade($this->tid, "", $this->nick);
		
	}
	
	/**
	 * 关闭交易	
	 */
	private function TradeClose() {
		$this->syn("TRADE_CLOSED_BY_TAOBAO");
	}
	
	/**
	 * 买家付款
	 */
	private function TradeBuyerPay() {
		$this->syn("WAIT_SELLER_SEND_GOODS");
	}
	
	/**
	 * 卖家发货
	 */
	private function TradeSellerShip() {
		$fields = "status, orders.oid, orders.status";
		return $this->Trade->synTrade($this->tid, $fields, $this->nick);
	}
	
	/**
	 * 交易成功
	 */
	private function TradeSuccess() {
		$this->syn("TRADE_FINISHED");
	}
	
	/**
	 * 延长收货时间
	 */
	private function TradeDelayConfirmPay() {
		$this->fields = "timeout_action_time, orders.timeout_action_time";
		return $this->Trade->synTrade($this->tid, $this->fields, $this->nick);
	}
	
	/**
	 * 子订单退款成功
	 */
	private function TradePartlyRefund() {
		//TODO 子订单退款成功    两个退款，只有一个消息
		$this->Trade->synTrade($this->tid, "", $this->nick);
		Log::write("子订单退款成功:".$this->tid, Log::ERR);
		return true;
	}
	
	/**
	 * 子订单打款成功
	 */
	private function TradePartlyConfirmPay() {
		//TODO 子订单打款成功 没确认
		$this->Trade->synTrade($this->tid, "", $this->nick);
		Log::write("子订单退款成功:".$this->tid, Log::ERR);
		return true;
	}
	
	/**
	 * 交易超时提醒
	 */
	private function TradeTimeoutRemind() {
		$this->Trade->synTrade($this->tid, "", $this->nick);
		Log::write("交易超时提醒:".$this->tid, Log::ERR);
	}
	
	/**
	 * 交易评价变更
	 */
	private function TradeRated() {
		$this->fields = "seller_rate, buyer_rate, orders.seller_rate, orders.buyer_rate";
		
		return $this->Trade->synTrade($this->tid, $this->fields, $this->nick);
	}
	
	/**
	 * 交易备注修改
	 */
	private function TradeMemoModified() {
		$this->fields = "buyer_message, seller_memo, seller_flag";
		
		return $this->Trade->synTrade($this->tid, $this->fields, $this->nick);
	}
	
	/**
	 * 修改收货地址
	 */
	private function TradeLogisticsAddressChanged() {
		$this->fields = "receiver_name, receiver_state, receiver_city, receiver_district, receiver_address, receiver_zip, receiver_mobile, receiver_phone";
		
		return $this->Trade->synTrade($this->tid, $this->fields, $this->nick);
	}
	
	/**
	 * 修改订单信息（SKU等）
	 */
	private function TradeChanged() {
		//TODO 修改订单信息（SKU等） 似乎改收货地址也弹出这个
		//交易确认后 不规律的时候也会弹出这个
		return $this->Trade->synTrade($this->tid, "", $this->nick);
	}
	
	/**
	 * 比较淘宝和本地订单最后更新时间
	 */
	private function syn($fields) {
		if (!$this->loc_time()) {  //记录在数据库中不存在
			$this->Trade->synTrade($this->tid, "", $this->nick);
		} else if ($this->modified < $this->loc_time()) {  //淘宝的变更时间在数据库变更时间前
			$this->Trade->synTrade($this->tid, $fields, $this->nick);
			return false;
		} else { //变更状态
			$this->update['status'] = $fields;
			$this->Trade->where($this->where)->save($this->update);
			
			$update = "status='$fields'";
			$this->Order->where($this->where)->save($update);
		}
	}
	
	/**
	 * 获取本地订单最后更新时间
	 */
	private function loc_time() {
		$modified = $this->Trade->where($this->where)->getField('modified');
		if (!$modified) return false;
		return $modified;
	}
	
	/**
	 * 数据检查
	 * @param $trades 交易数据
	 */
	private function format($trades) {
		//if ($trades['tid']) $trades['tid'] = number_format($trades['tid'],0,'','');
		//if ($trades['oid']) $trades['oid'] = number_format($trades['oid'],0,'','');
		if ($trades['modified']) $trades['modified'] = strtotime($trades['modified']);
	
		return $trades;
	}
}
?>