<?php
class Top {
	public $appkey, $secretKey;
	
	public $gatewayUrl = "http://gw.api.taobao.com/router/rest";
	public $TQLUrl = "http://gw.api.taobao.com/tql/2.0/json";
	public $format = "json";
	public $checkRequest = true;
	
	protected $signMethod = "md5";
	protected $apiVersion = "2.0";
	protected $sdkVersion = "top-sdk-php-20130312";
	
	function __construct() {
		//C(F('top')); //初始化淘宝TOP配置
		$this->appkey = C('APP_KEY');
		$this->secretKey = C('SECRET_KEY');
	}
	
	protected function generateSign($params) {
		ksort ($params);
		$stringToBeSigned = $this->secretKey;
		
		foreach ($params as $k => $v) {
			if (is_null($v)) continue; // fma 判断键和键值非空
			if ("@" != substr ( $v, 0, 1 )) // 判断是不是文件上传
			{
				$stringToBeSigned .= "$k$v";
			}
		}
		unset($k, $v);
		$stringToBeSigned .= $this->secretKey;
		return strtoupper(md5($stringToBeSigned));
	}
	
	public function curl($url, $postFields = null, $timeout = 30) {
		$ch = curl_init ();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FAILONERROR, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		// https 请求
		if (strlen($url) > 5 && strtolower(substr($url, 0, 5)) == "https") {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		}
		
		if (is_array($postFields) && 0 < count($postFields)) {
			$postBodyString = "";
			$postMultipart = false;
			foreach ($postFields as $k => $v) {
				if (is_null($v)) continue; // fma 判断键和键值非空
				if ("@" != substr($v, 0, 1)) // 判断是不是文件上传
				{
					$postBodyString .= "$k=" . urlencode($v) . "&";
				} else // 文件上传用multipart/form-data，否则用www-form-urlencoded
				{
					$postMultipart = true;
				}
			}
			
			unset($k, $v);
			curl_setopt($ch, CURLOPT_POST, true);
			if ($postMultipart) {
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
			} else {
				curl_setopt($ch, CURLOPT_POSTFIELDS, substr($postBodyString, 0, - 1));
			}
		}
		$reponse = curl_exec($ch);
		
		if (curl_errno($ch)) {
			throw new Exception(curl_error($ch), 0);
		} else {
			$httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if (200 !== $httpStatusCode) {
				throw new Exception($reponse, $httpStatusCode);
			}
		}
		curl_close($ch);
		return $reponse;
	}
	
	/**
	 * 获取TOP数据
	 * @param array $request 业务参数
	 * @param string $session SessionKey
	 * @param boolean $api 调用方式：默认为TQL,为true时是SDK
	 */
	public function tql($request, $session = null, $api = false) {
		$result = array();
		if ($api && $this->checkRequest) { //校验参数
			try {
				import("COM.Tb.RequestCheckUtil");
				$request->check();
			} catch ( Exception $e ) {
				$result->code = $e->getCode();
				$result->msg = $e->getMessage();
				return $result;
			}
		}
		// 组装系统参数
		$sysParams["app_key"] = $this->appkey;
		$sysParams["sign_method"] = $this->signMethod;
		$sysParams["timestamp"] = date("Y-m-d H:i:s");
		if (null != $session) $sysParams["session"] = $session; //赋值session
		
		if ($api) { //通过SDK请求
			$sysParams["v"] = $this->apiVersion;
			$sysParams["format"] = $this->format;
			$sysParams["method"] = $request->getApiMethodName();
				
			$request = $request->getApiParas(); // 获取业务参数
		} else { //TQL业务参数检查
			if (!is_array($request) || !array_key_exists('ql', $request)) return '业务参数错误';
		}
		
		// 签名
		$sysParams["sign"] = $this->generateSign(array_merge($request, $sysParams));
		
		// 系统参数放入GET请求串
		$requestUrl = $api ? $this->gatewayUrl . "?" : $this->TQLUrl . "?";
		foreach ($sysParams as $sysParamKey => $sysParamValue) {
			$requestUrl .= "$sysParamKey=" . urlencode($sysParamValue) . "&";
		}
		$requestUrl = substr($requestUrl, 0, - 1);
		// 发起HTTP请求
		try {
			$resp = $this->curl($requestUrl, $request);  //获取数据
		} catch (Exception $e) {
			$apiname = $api ? $sysParams["method"] : $request["ql"];
			$this->logger($apiname, $requestUrl, "HTTP_ERROR_" . $e->getCode(), $e->getMessage());
			$result['code'] = $e->getCode();
			$result['msg'] = $e->getMessage();
			return $result;
		}
		
		// 解析TOP返回结果
		$respWellFormed = false;
		
		$respObject = json_decode($resp, true);
		if (null !== $respObject) {
			$respWellFormed = true;
			foreach ( $respObject as $propKey => $propValue ) {
				$respObject = $propValue;
			}
		} else if ("xml" == $this->format) {
			$respObject = @simplexml_load_string($resp);
			if (false !== $respObject) {
				$respWellFormed = true;
			}
		}
		
		// 返回的HTTP文本不是标准JSON或者XML，记下错误日志
		if (false === $respWellFormed) {
			$apiname = $api ? $sysParams["method"] : $request["ql"];
			$this->logger($apiname, $requestUrl, "Formed Error", array2string($resp));
			$result['code'] = 0;
			$result['msg'] = array2string($resp);
			return $result;
		}
		
		// 如果TOP返回了错误码，记录到业务错误日志中
		if (isset($respObject['code'])) {
			$apiname = $api ? $sysParams["method"] : $request["ql"];
			$this->logger($apiname, $requestUrl, $respObject['code'], $respObject['msg']);
		}
		return $respObject;
	}
	
	/**
	 * 将数组转换为TQL语句
	 * @param array $where 要生成的数组
	 * @param string $font 连接串。
	 */
	public function sqls($where, $font = ' AND ') {
		if (is_array($where)) {
			$sql = '';
			foreach ($where as $key=>$val) {
				if (is_null($val)) continue; //键值非空
				$sql .= $sql ? "$font$key=$val" : "$key=$val";
			}
			return $sql;
		} else {
			return $where;
		}
	}
	
	/**
	 * 校验授权信息是否合法
	 * @param array $container 授权信息
	 */
	final public function check_sign($container) {
		if ($container['top_appkey'] != $this->appkey) return false;
		//签名规则为base64(md5(top_appkey+top_parameters+top_session+app_secret))
		$verify = $container['top_appkey'].$container['top_parameters'].$container['top_session'].$this->secretKey; //组成签名字符串
		$sign = base64_encode(md5($verify, true));  //转成16位MD5再转成base64格式
		if ($sign == $container['top_sign']) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 获取授权信息并转成数组
	 * @param string $parameters 授权信息
	 */
	final public function Parameters($parameters) {
		$container = array();
		$parameters = base64_decode($parameters);
		$par_arr = explode("&", $parameters);
		foreach ($par_arr as $par_arr) {
			$keys = explode("=", $par_arr);
			$container[$keys[0]] = $keys[1];
		}
		return $container;
	}
	
	/**
	 * 记录错误日志
	 */
	protected function logger($apiName, $requestUrl, $errorCode, $responseTxt) {
		Log::record('API->' . $apiName, Log::EMERG);
		Log::record('ERRCODE->' . $errorCode . ',MSG->' . $responseTxt, Log::EMERG);
		Log::save();
	}
	
}