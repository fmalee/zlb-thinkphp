<?php

/**
 * 淘宝Action Base
 */
class TbbaseAction extends MemberbaseAction {

    public $top, $nick, $sessionKey, $sid, $memberinfo, $shoplist; //会员模型相关配置

    function _initialize() {
        parent::_initialize();
        if (substr(ACTION_NAME, 0, 7) != 'public_') {
            $this->check_user(); //登陆用户情况'
        }
        
        $this->shoplist = M('User')->where(array('userid'=>$this->memberinfo['userid']))->field('user_id,nick,avatar')->select();
        $this->assign('shoplist',$this->shoplist);
    }
	
    /**
     * 判断用户是否已经登陆
     */
    final public function check_user() {
		global $nick, $sid, $sessionKey;
		//验证用户，获取用户信息
		$map = array();
		$map['userid'] = $this->memberinfo['userid'];
		$map['major'] = 1;
		$_memberinfo = M('User')->where($map)->field('user_id, nick, type, alipay_account, major, sessionKey, syn_trade, syn_product, has_setting')->find();
		
		if (in_array(strtolower(GROUP_NAME), array('seller','fenxiao')) && strtolower(MODULE_NAME) == 'shop' && in_array(strtolower(ACTION_NAME), array('shops','container','choose'))) {
			if (isset($_memberinfo['group']) && $_memberinfo['group']== 0) {
				$this->error("会员权限出错", U("Member/Index/"));
			}
			return true;
		} else {
			if ($_memberinfo['sessionKey']) {
				if (strtolower(MODULE_NAME) == 'content' && $_memberinfo ['group'] == 0) $this->error("会员权限出错", U("Seller/Shop/shops/"));
				if (strtolower(MODULE_NAME) <> 'shop' && strtolower(ACTION_NAME) <> 'setting') { //设置一键上传 TODO shop下无效
					!$_memberinfo['has_setting'] && $this->error("请先设置好一键上传的默认选项", U("Seller/Shop/setting/"));
				}
				//获取会员信息
				$this->nick = $_memberinfo['nick'];
				define("USERID", $_memberinfo['user_id']);
				define("NICK", $_memberinfo['nick']);
				define("SESSIONKEY", $_memberinfo['sessionKey']);
				$this->sessionKey = $_memberinfo['sessionKey'];
				$shopinfo = M('Shop')->where(array("nick"=>$this->nick))->field('sid, shop_title, pic_path')->find();
				$this->memberinfo = array_merge($shopinfo, $_memberinfo, $this->memberinfo);
				$this->sid = $this->memberinfo['sid'];
				//define("SID", $this->memberinfo['sid']);
				$this->assign('memberinfo',$this->memberinfo);
			} else {
				if (strtolower(MODULE_NAME) == 'shop' && strtolower(ACTION_NAME) == 'manager') {
					return true;
				}
				if ($_memberinfo && !$_memberinfo['sessionKey']) $this->error("默认店铺授权失效，请重新授权！", U("Seller/Shop/manager/"));
				$_user = M('User')->where(array('userid'=>$this->_userid))->getField('major');
				if ($_user) {
					$this->error("请指定一个默认账户", U("Seller/Shop/manager/"));
				} else {
					$this->error("请绑定一个淘宝卖家账户", U("Seller/Shop/manager/"));
				}
			}
		}
    }
}
?>
