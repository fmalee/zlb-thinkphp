<?php

/**
 * 前台Action
 * Some rights reserved：abc3210.com
 * Contact email:admin@abc3210.com
 */
class BaseAction extends Action {

    //各种缓存 比如当前登陆用户信息等
    public static $Cache = array();

    protected function _initialize() {
        //消除所有的magic_quotes_gpc转义
        Input::noGPC();
    }
	
    /**
     * 写入操作日志
     * @param type $info 操作说明
     * @param type $status 状态,1为写入，2为更新，3为删除
     * @param type $data 数据
     * @param type $options 条件
     */
    final public function addLogs($info, $status = 1, $data = array(), $options = array()) {
        $uid = self::$Cache['uid'];
        if (!$uid) {
            return false;
        }
        $data = sreialize($data);
        $options = serialize($options);
        $get = $_SREVER['HTTP_REFERER'];
        $post = "";
        M("Operationlog")->add(array(
            "uid" => $uid,
            "time" => date("Y-m-d H:i:s"),
            "ip" => get_client_ip(),
            "status" => $status,
            "info" => $info,
            "data" => $data,
            "options" => $options,
            "get" => $get,
            "post" => $post
        ));
    }

    /**
     * 验证码验证
     * @param type $verify 
     */
    static public function verify($verify) {
        if (session('verify') == strtolower($verify)) {
            session('verify', NULL);
            return true;
        } else {
            return false;
        }
    }

    
    public function _empty() {
        $this->_404();
    }
    
    protected function _404($url = '') {
        if ($url) {
            redirect($url);
        } else {
            send_http_status(404);
            $this->display(C('TEMPLATE_PATH') . '404.html');
            exit;
        }
    }
}

?>
