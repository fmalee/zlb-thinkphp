<?php

/**
 * 前台会员中心Action Base
 */
class MemberbaseAction extends BaseAction {

    public $userid, $username, $memberinfo = array();

    function _initialize() {
        C("USER_AUTH_MODEL", "Member");
        parent::_initialize();
        //所有以public_开头的方法都无需检测是否登陆
        if (substr(ACTION_NAME, 0, 7) != 'public_') {
            //登陆检测
            $this->check_member();
        }
    }
    
    /**
     * 判断用户是否已经登陆
     */
    final public function check_member() {
    	$member_auth = cookie('auth');
    	if (GROUP_NAME == 'Member' && MODULE_NAME == 'Index' && in_array(ACTION_NAME, array('login', 'register', 'logout','checkLogin'))) {
    		return true;
    	} else {
    		//判断是否存在auth cookie
    		if ($member_auth) {
    			//$auth_code = md5(C('AUTHCODE').$_SERVER['HTTP_USER_AGENT']);
    			$auth_code = md5(C('AUTHCODE'));
    			list($userid, $password) = explode("\t", authcode($member_auth, 'DECODE', $auth_code));
    			//验证用户，获取用户信息
    			$map = array();
    			$map['userid'] = $userid;
    			$this->memberinfo = D('Member')->where($map)->find();
    			
    			if(!$this->memberinfo || $this->memberinfo['password'] <> $password) {
    				cookie('auth', null);
    				cookie('userid', null);
    				cookie('username', null);
    				cookie('groupid', null);
    			} else {
    				$this->userid = $this->memberinfo['userid'];
    				$this->username = $this->memberinfo['username'];
    				$this->assign('memberinfo',$this->memberinfo);
    			}
    			unset($userid, $password, $member_auth, $auth_code);
    		} else {
    			$forward= isset($_GET['forward']) ?  urlencode($_GET['forward']) : urlencode(get_url());
    			$this->error("您的会话已过期，请重新登录。！", U("Member/Index/login")."?forward=$forward");
    		}
    	}
    }
}

?>
