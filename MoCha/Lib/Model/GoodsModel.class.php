<?php
class GoodsModel extends Model {
	protected $tablePrefix = 'tb_'; // 数据库前缀
	public $top;
	
	// 自动验证设置
	protected $_validate = array(
			array('type', 'require', '请填写类别！', 1),//1为必须验证
			array('user', 'require', '请填写操作人！', 1),//1为必须验证
	);
	
	// 自动填充设置
	protected $_auto = array(
			array('nick', NICK, 1),
			array('sellted', '0', 1),
			array('modified', 'time', 3, 'function'),
	);
	
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * 获取档口信息
	 * @param $num_iid 商品编辑
	 */
	public function iid2shop($num_iid) {
		$nick = $this->getFieldById($num_iid, 'nick');
		$shop = M('Shop')->where(array('nick'=>$nick))->field('title,location')->find();
		return $shop['location'].$shop['title'];
	}
}