<?php
class CreditModel extends Model {
	// 自动验证设置
	protected $_validate = array(
			array('type', 'require', '请填写类别！', 1),//1为必须验证
			array('user', 'require', '请填写操作人！', 1),//1为必须验证
	);
	
	// 自动填充设置
	protected $_auto = array(
			array('sellted', '0', 1),
			array('modified', 'time', 3, 'function'),
	);
}