<?php

/**
 * 淘宝评价
 */
class RateModel {
	public $top;
	public function __construct() {
		$this->top = new Top();
	}
	
    /**
	* 搜索评价信息
	* @param $where 评价搜索条件
	* @param $page_no 页码 翻页获取的条数（page_no*page_size）
	* @param $page_size 每页条数 最大值:150
	* @param $fields 需返回的字段
	* 参数：rate_type,role,result,page_no,page_size,start_date,end_date,tid,use_has_next,num_iid
	*/
	public function get_Rates($where, $page_no = 1, $page_size = 40, $fields = "") {
		if (!is_array($where)) return false;
		
		$where = $this->top->sqls($where, ' and ');
		$where .=" and page_no=$page_no and page_size=$page_size";
		if (!$fields) {
			$fields = "num_iid, valid_score, tid, oid, role, nick, result, created, rated_nick, item_title, item_price, content, reply";
		} else {
			$fields .=", tid, oid, num_iid, created";
		}
		$params['ql'] = "select $fields from taobao.traderates.get where $where";
		$result = $this->top->tql($params, SESSIONKEY);
		$rates['rates'] = $result['trade_rates']['trade_rate'];
		$rates['total'] = $result['total_results'];
		
		return $rates;
	}
}
?>
