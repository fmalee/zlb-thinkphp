<?php
class CreditViewModel extends ViewModel {
	public $viewFields = array(
			'Credit'=>array('tid','nick','user','contact','qq','memo','is_send','type','settled','created','modified','_type'=>'LEFT'),
			'Trade'=>array('status','created','pay_time','consign_time','buyer_nick','payment','seller_flag','post_fee','seller_rate','buyer_rate','_on'=>'Credit.tid=Trade.tid'),
			//'Order'=>array('title','num_iid','num','outer_iid','outer_sku_id', '_on'=>'Refund.oid=Orders.oid','_as'=>'Orders'),
	);
}