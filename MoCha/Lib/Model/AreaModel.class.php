<?php
class AreaModel extends Model {
	protected $tablePrefix = 'mc_'; // 自动填充设置
	public $top;
	
	public function __construct() {
		parent::__construct();
		$this->top = new Top();
	}
	
	/**
	 * 获取并更新地区表
	 * 不需要经常更新
	 */
	public function synArea() {
		$fields = "id,type,name,parent_id,zip";
		$params['ql'] = "select $fields from areas";
		$areas = $this->top->tql($params);
		if (!$areas['areas']) return false;
		
		$this->execute("truncate table __TABLE__ "); // 清空areas表
		foreach ($areas['areas']['area'] as $area) {
			$this->add($area);  //更新areas表
		}
		return true;
	}
}