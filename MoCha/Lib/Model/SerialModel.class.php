<?php
/**
 * 邦编码与关系库操作类
 * 货源 IID
 * serial 编码
 * nick 店铺
 * num_iid 商品
 * 在alias表：
 * serial 是唯一值
 * 一个serial 可对应多个iid
 * 在主表：
 * 一个serial iid 唯一对应
 * 一个serial 可对应多个num_iid
 */
class SerialModel extends Model {
	protected $tablePrefix = 'tb_'; // 自动填充设置
	public $top;
	
	public function __construct() {
		parent::__construct();
		$this->top = new Top();
	}
	
	/**
	 * 结合商品ID产生编码信息
	 * @param $num_iid 商品ID
	 * 同时返回编码及商品信息
	 */
	public function get_item($num_iid) {
		$fields = "cid,outer_id,pic_url,price,title,type";
		$item = D('Item')->synItem($num_iid, 0, $fields); // 更新宝贝信息
		$serial = M('Serial_alias')->where(array('iid'=>$num_iid, 'nick'=>NICK))->field('serial,model,price,memo,status')->find();
		if (!$serial) {
			$item['serial'] = $this->create_serial();  //生成唯一编码
		} else {
			$item = array_merge($item, $serial);
		}
	
		return $item;
	}
	
	/**
	 * 编辑编码
	 * @param $info 编码数据
	 */
	public function edit($info) {
		if (!isset($info['iid']) || empty($info['iid'])) return false;
		$serial['iid'] = $info['iid'];  //重新赋值，因为有可能info数组有其他值
		$serial['serial'] = $info['serial'];
		$serial['price'] = $info['price'];
		$serial['model'] = $info['model'];
		$serial['nick'] = NICK;
		$serial['modified'] = NOW_TIME;
		unset($info);
		
		$Alias = M('Serial_alias');
		$alias = $Alias->where(array('iid'=>$serial['iid'],'nick'=>NICK))->Field('id,status')->find();
		if (!$alias) {
			$serial['status'] = 77;
			$Alias->add($serial);
		} else {
			$serial['id'] = $alias['id'];
			$Alias->save($serial);
			if ($alias['status'] ==99) D('Serial')->set_major($serial['iid']);
		}
		return true;
	}
	
	/**
	 * 设置为默认货源
	 * @param $iid 货源编码
	 * @param $num_iid 商品编码
	 */
	public function set_major($iid, $num_iid = "") {
		$alais = M('Serial_alias')->where(array('iid'=>$iid, 'nick'=>NICK))->field('serial,price,model')->find();
		if (!$alais) return false;
		$alais['iid'] = $iid;
		$alais['barcode'] = $this->get_barcode($iid);
		$alais['status'] = 99;
		$alais['modified'] = NOW_TIME;
		$id = $this->where(array('serial'=>$alais['serial'], 'nick'=>NICK))->getField('id');
		if (!$id && $num_iid) { //新增编码记录
			$alais['nick'] = NICK;
			$alais['num_iid'] = $num_iid;
			$this->add($alais);
		} else { //更新编码记录
			$alais['id'] = $id;
			$this->save($alais);
		}
		M('Serial_alias')->where(array('iid'=>$iid, 'nick'=>NICK))->save(array('status'=>99));//设置为首选供应商
		M('Serial_alias')->where(array('iid'=>array('neq',$iid), 'serial'=>$alais['serial'], 'nick'=>NICK))->save(array('status'=>88));//更新所有相同编码的货源状态
		
		return true;
	}
	
	/**
	 * 随机生成编码
	 * @param $str 已经存在的编码 如A01,Z99
	 */
	public function create_serial($str = "") {
		$numbers = rand(01, 99); // 生成随机数字
		$alphabets = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','P','Q','R','S','T','V','W','X','Y','Z');
		$arr = rand(0, 23); // 生成随机数组指标
		$serial = $alphabets[$arr].sprintf("%02d", $numbers);
		if (!array_key_exists ($serial,$str)) {
			$r = $this->where(array('serial'=>$serial,'nick'=>NICK))->getField('id');
			if (!$r) return $serial;
		}
		$str[$serial] = 1; // 记录存在编码
		$this->create_serial($str); // 进行递归
	}
	
	/**
	 * 生成供应商识别码
	 * @param $id 传入IID        	
	 * @param $type 字段类型 1=扩展
	 */
	public function get_barcode($iid) {
		$nick = M('Goods')->getFieldById($iid,'nick');
		$title = M('Shop')->getFieldByNick($nick,'title');
		$alias = M('Serial_alias')->where(array('iid'=>$iid))->field('model,price')->find();
		$barcode = $title . "-" . $alias['model'] . "-" . $alias['price'];
		return $barcode;
	}
	
	/**
	 * 删除商家编码
	 * @param $id 传入IID        	
	 * @param $type 字段类型 1=扩展
	 */
	public function public_delete($id) {
		$a_db = pc_base::load_model('taobao_serial_alias_model');
		$serial = $a_db->get_one(array('id' => $id), 'serial,iid,status');
		if (!$serial) return false;
		if ($serial ['status'] < 99) {
			$a_db->delete(array('id'=>$id));
		} else {
			$def = $this->delete(array('serial'=>$serial['serial'], 'nick'=>NICK));
			if ($def) {
				$a_db->update(array('status' => 0), array('id' => $id, 'nick' => $this->nick));
				$a_db->delete("status>0 AND nick='$this->nick' AND serial=".$serial ['serial']);
			}
		}
		return true;
	}
}