<?php
class TradeModel extends Model {
	protected $tablePrefix = 'tb_'; // 数据库前缀
	public $Taoapi;
	
	public function __construct() {
		parent::__construct();
		$this->Taoapi = new Taoapi();
	}
	
	/**
	 * 同步淘宝和本地交易数据
	 * @param $tid 传入TID
	 * @param $field 需要更新的字段
	 * @param $nick 商家昵称 用于获取SESSIONKEY
	 * 包含以下字段的返回会增加TOP的后台压力:commission_fee, buyer_alipay_no, buyer_email, timeout_action_time, item_memo, trade_memo, available_confirm_fee
	 */
	public function synTrade($tid, $fields = "", $nick = "") {
		$this->Taoapi->method = 'trade.fullinfo';
		$this->Taoapi->session = $nick ? D('User')->nick2session($nick) : SESSIONKEY;
		if (!$fields) {
			$this->Taoapi->fields = "seller_nick, buyer_nick, type, created, tid, seller_rate, buyer_rate, status, payment, post_fee, total_fee, pay_time, end_time, modified, consign_time, buyer_obtain_point_fee, point_fee, real_point_fee, received_payment, commission_fee, cod_fee, cod_status, shipping_type, receiver_name, receiver_state, receiver_city, receiver_district, receiver_address, receiver_zip, receiver_mobile, receiver_phone, seller_memo, alipay_no, buyer_message, buyer_alipay_no, buyer_email, seller_flag, available_confirm_fee, has_post_fee, trade_memo, is_3D, invoice_name, promotion_details, orders, trade_from";
		} else {
			$this->Taoapi->fields = $fields . ",tid,modified";
		}
		$this->Taoapi->where = "tid=$tid";
		$trade = $this->Taoapi->select()->get();

		if (isset($trade['trade_fullinfo_get_response']['trade'])) {
        	$this->saveTrade($trade['trade_fullinfo_get_response']['trade']); //入库

        	return true;
    	} else {
    		return false;
    	}
	}

	/**
	 * 批量同步交易数据
	 * @param $where 查询条件  start_created，end_created，status，page_no，page_size，buyer_nick，type，ext_type，rate_status，tag，use_has_next，is_acookie
	 * @param $field 需要更新的字段
	 * @param $nick 商家昵称 用于获取SESSIONKEY
	 */
	public function synTrades($where, $fields = "", $nick = "") {
		$this->Taoapi->session = $nick ? D('User')->nick2session($nick) : SESSIONKEY;

		$this->Taoapi->method = 'trades.sold';
		$this->Taoapi->fields = 'tid';
		$this->Taoapi->select($where);

		$this->Taoapi->method = 'trade.fullinfo';
		if (!$fields) {
			$this->Taoapi->fields = "seller_nick, buyer_nick, type, created, tid, seller_rate, buyer_rate, status, payment, post_fee, total_fee, pay_time, end_time, modified, consign_time, buyer_obtain_point_fee, point_fee, real_point_fee, received_payment, commission_fee, cod_fee, cod_status, shipping_type, receiver_name, receiver_state, receiver_city, receiver_district, receiver_address, receiver_zip, receiver_mobile, receiver_phone, seller_memo, alipay_no, buyer_message, buyer_alipay_no, buyer_email, seller_flag, available_confirm_fee, has_post_fee, trade_memo, is_3D, invoice_name, promotion_details, orders, trade_from";
		} else {
			$this->Taoapi->fields = $fields . ",tid,modified";
		}
		$this->Taoapi->select(array('tid'=>'__SUBSELECT__'));

		$result = $this->Taoapi->sub()->get();

		foreach ($result as $trade) {
			if (isset($trade['trade_fullinfo_get_response']['trade'])) {
	        	$this->saveTrade($trade['trade_fullinfo_get_response']['trade']); //入库
	    	}
    	}

    	return true;
	}

	/**
	 * 批量同步交易数据
	 * @param $where 查询条件  start_created，end_created，status，page_no，page_size，buyer_nick，type，ext_type，rate_status，tag，use_has_next，is_acookie
	 * @param $field 需要更新的字段
	 * @param $nick 商家昵称 用于获取SESSIONKEY
	 */
	public function synSold($where, $fields = "", $nick = "") {
		$this->Taoapi->session = $nick ? D('User')->nick2session($nick) : SESSIONKEY;
		$this->Taoapi->method = 'trades.sold';
		if (!$fields) {
			$this->Taoapi->fields = "tid,num,num_iid,status,title,type,price,discount_fee,point_fee,total_fee,is_lgtype,is_brand_sale,is_force_wlb,lg_aging,lg_aging_type,created,pay_time,modified,end_time,alipay_id,alipay_no,seller_flag,buyer_nick,buyer_area,has_yfx,yfx_fee,yfx_id,yfx_type,has_buyer_message,credit_card_fee,nut_feature,step_trade_status,step_paid_fee,mark_desc,send_time,shipping_type,adjust_fee,buyer_obtain_point_fee,cod_fee,trade_from,cod_status,service_orders,commission_fee,buyer_rate,trade_source,seller_can_rate,seller_nick,pic_path,payment,seller_rate,real_point_fee,post_fee,receiver_name,receiver_state,receiver_city,receiver_district,receiver_address,receiver_zip,receiver_mobile,receiver_phone,consign_time,received_payment,orders";
		} else {
			$this->Taoapi->fields = $fields;
		}
		$this->Taoapi->where = $where;

		$result = $this->Taoapi->select()->get();

    	return $result;
	}

	/**
	 * 保存交易数据
	 * @param array $trade
	 */
	public function saveTrade($trade) {
		$trade = $this->format($trade); //格式化
		$tid = $trade['tid'];
		if (isset($trade['orders'])) {
			D('Order')->saveOrder($tid, $trade['orders']['order']);  //更新订单数据
			unset($trade['orders']);
		}
		if (in_array($trade['status'], array('WAIT_BUYER_CONFIRM_GOODS','TRADE_BUYER_SIGNED','TRADE_FINISHED'))) { // 物流信息
			$shipping = $this->synShipping($tid, $nick);
			$trade = array_merge($trade, $shipping);
		}

		$tid = $this->getFieldByTid($tid, 'tid');
		if ($tid) {
			$this->save($trade);  //更新数据
		} else {
			$tid = $this->add($trade);  //新增数据
		}

		return $tid;
	}
	
	/**
	 * 优惠信息
	 * @param $promotion 优惠信息
	 */
	public function promotion($promotion) {
		foreach ($promotion as $v) {
			//$v['id'] = number_format($v['id'],0,',','');
			$details[] = $v;
		}
		// TODO fma.为了效率，暂不插入promotion表，手动更新。
		return serialize($details);
	}
	
	/**
	 * 同步订单物流信息
	 * @param $tid 传入TID
	 * @param $nick 商家昵称  用于主动通知
	 */
	public function synShipping($tid, $nick = "") {
		$this->Taoapi->method = 'logistics.orders';
		$this->Taoapi->session = $nick ? D('User')->nick2session($nick) : SESSIONKEY;
		$this->Taoapi->fields = "delivery_start, delivery_end, out_sid, created, modified, status, freight_payer, company_name";
		$this->Taoapi->where = "tid=$tid";
		$shibping = $this->Taoapi->select()->get();

		$shipping = $this->format($shibping['logistics_orders_get_response']['shippings']['shipping']);
		$shipping = reset($shipping);
			
		$trade['delivery_start'] = strtotime($shipping['delivery_start'] );
		$trade['delivery_end'] = strtotime($shipping['delivery_end'] );
		$trade['out_sid'] = $shipping['out_sid'];
		$trade['delivery_company'] = $shipping['company_name'];
		$trade['delivery_created'] = strtotime($shipping['created'] );
		$trade['delivery_modified'] = strtotime($shipping['modified'] );
		$trade['delivery_status'] = $shipping['status'];

		unset($shipping);
		
		return $trade;
	}
	
	/**
	 * 更改订单备注
	 * @param $tid 传入TID
	 * @param $memo 备注信息
	 * @param $nick 商家昵称  用于主动通知
	 */
	public function synMemo($tid, $memo, $nick = "") {
		$this->Taoapi->method = 'trade.memo';
		$this->Taoapi->session = $nick ? D('User')->nick2session($nick) : SESSIONKEY;
		$this->Taoapi->where = array('tid'=>$tid);

		$update = array('flag'=>$memo['flag']);
		if (array_key_exists('memo', $memo)) {
			$seller_memo = new_addslashes(trim($memo['memo']));
			if ($seller_memo) {
				$update['memo'] = "#memo#";
				$this->Taoapi->memo = $seller_memo;
			} else {
				$update['reset'] = 'true';
			}
		}
		$result = $this->Taoapi->update($update)->get();
		
		return isset($result['trade_memo_update_response']) ? true : false;
	}
	
	/**
	 * 延长收货时间
	 * @param $tid 交易编号
	 * @param $delay 延长天数 可选值 ：3, 5, 7, 10
	 * @param $nick 商家昵称  用于主动通知
	 */
	public function synDelay($tid, $delay, $nick = "") {
		$this->Taoapi->method = 'taobao.trade.receivetime.delay';
		$this->Taoapi->session = $nick ? D('User')->nick2session($nick) : SESSIONKEY;
		$this->Taoapi->where = "tid=$tid";
		$result = $this->Taoapi->update(array('days'=>$delay))->get();

		return isset($result['trade']) ? true : false;
	}
	
	/**
	 * 关闭交易
	 * @param $tid 交易编号
	 * @param $close 延长天数 可选值 ：3, 5, 7, 10
	 * @param $nick 商家昵称  用于主动通知
	 */
	public function synClose($tid, $close, $nick = "") {
		$this->Taoapi->method = 'taobao.trade.close';
		$this->Taoapi->session = $nick ? D('User')->nick2session($nick) : SESSIONKEY;
		$this->Taoapi->where = "tid=$tid";
		$result = $this->Taoapi->update(array('close_reason'=>$close))->get();
	
		return isset($result['trade']) ? true : false;
	}
	
	/**
	 * 更新销售属性
	 * @param $oid 订单编号
	 * @param $sku_id 销售属性编号
	 * @param $sku_props 销售属性组合串 格式：p1:v1;p2:v2
	 */
	public function synSku($oid, $sku_id, $sku_props, $nick = "") {
		$this->Taoapi->method = 'trade.ordersku';
		$this->Taoapi->session = $nick ? D('User')->nick2session($nick) : SESSIONKEY;
		$this->Taoapi->where = "oid=$oid";
		$result = $this->Taoapi->update(array('sku_id'=>$sku_id,'sku_props'=>$sku_props))->get();
	
		return isset($result['trade_ordersku_update_response']) ? true : false;
	}
	
	/**
	 * 转换交易数据格式
	 * @param $trade 传入交易数据
	 */
	public function format($trade) {
		//if ($trade['tid']) $trade['tid'] = number_format($trade['tid'],0,',','');
		//if ($trade['oid']) $trade['oid'] = number_format($trade['oid'],0,',','');
		
		if ($trade['created']) $trade['created'] = strtotime($trade['created']);
		if ($trade['modified']) $trade['modified'] = strtotime($trade['modified']);
		if ($trade['end_time']) $trade['end_time'] = strtotime($trade['end_time']);
		if ($trade['pay_time']) $trade['pay_time'] = strtotime($trade['pay_time']);
		if ($trade['consign_time']) $trade['consign_time'] = strtotime($trade['consign_time']);
		
		if ($trade['promotion_details']) $trade['promotion_details'] = $this->promotion($trade['promotion_detail']);
		
		if (isset($order['has_post_fee'])) $trade['has_post_fee'] = str2bool($trade['has_post_fee']);
		if (isset($order['is_lgtype'])) $trade['is_lgtype'] = str2bool($trade['is_lgtype']);
		if (isset($order['is_brand_sale'])) $trade['is_brand_sale'] = str2bool($trade['is_brand_sale']);
		if (isset($order['is_force_wlb'])) $trade['is_force_wlb'] = str2bool($trade['is_force_wlb']);
		if (isset($order['has_yfx'])) $trade['has_yfx'] = str2bool($trade['has_yfx']);
		if (isset($order['has_buyer_message'])) $trade['has_buyer_message'] = str2bool($trade['has_buyer_message']);
		if (isset($order['can_rate'])) $trade['can_rate'] = str2bool($trade['can_rate']);
		if (isset($order['buyer_rate'])) $trade['buyer_rate'] = str2bool($trade['buyer_rate']);
		if (isset($order['seller_can_rate'])) $trade['seller_can_rate'] = str2bool($trade['seller_can_rate']);
		if (isset($order['is_part_consign'])) $trade['is_part_consign'] = str2bool($trade['is_part_consign']);
		if (isset($order['seller_rate'])) $trade['seller_rate'] = str2bool($trade['seller_rate']);
		if (isset($order['is_3D'])) $trade['is_3D'] = str2bool($trade['is_3D']);
		
		return $trade;
	}
}