<?php
class OrderModel extends Model {
	protected $tablePrefix = 'tb_'; // 数据库前缀
	
	/**
	 * 同步淘宝和本地订单数据
	 * @param $tid 交易编号
	 * @param $orders 订单数据
	 */
	public function saveOrder($tid, $orders) {
		foreach ($orders as $order) {
			$order = $this->format($order);
			$order['tid'] = $tid;
			$oid = $this->getFieldByOid($order['oid'], 'oid');
			if ($oid) {
				$this->save($order);  //更新数据
			} else {
				$oid = $this->add($order);  //新增数据
			}
		}
	}
	
	/**
	 * 转换订单数据格式
	 * @param $order 传入订单数据
	 */
	public function format($order) {
		//if ($order['oid']) $order['oid'] = number_format($order['oid'],0,',','');
		if ($order['title']) $order['title'] = new_addslashes($order['title']);
		if ($order['end_time']) $order['end_time'] = strtotime($order['end_time']);
		if (isset($order['buyer_rate'])) $order['buyer_rate'] = str2bool($order['buyer_rate']);
		if (isset($order['seller_rate'])) $order['seller_rate'] = str2bool($order['seller_rate']);
		if (isset($order['is_oversold'])) $order['is_oversold'] = str2bool($order['is_oversold']);
		
		return $order;
	}
}