<?php

/**
 * 淘宝用户
 */
class ShopModel extends Model {
	protected $tablePrefix = 'tb_'; // 数据库前缀
	public $top;
	
	public function __construct() {
		parent::__construct();
		$this->top = new Top();
	}
	
	/**
	 * 下载单个店铺基本信息
	 * @param $nick 用户昵称
	 * @param $fields 需要获取的字段
	 * @param $add 是否更新数据库
	 */
	public function syn_Shop($nick, $fields = "", $add = true) {
		if (!$fields) {  //生成需要获取的字段
			$fields = "sid, cid, nick, title, desc, bulletin, pic_path, created, modified, shop_score";
		} else {
			$fields .= ", sid, nick, modified";
		}
		$params['ql'] = "select $fields from shop where nick=$nick";
		$shop = $this->top->tql($params, SESSIONKEY);
		$shop = $shop['shop'];
	
		if ($add) $this->save_shop($shop); //更新数据库
		
		return $shop;
	}
	
	/**
	 * 更新店铺基本信息
	 * @param $nick 用户昵称
	 * @param $fields 需要获取的字段
	 * @param $add 是否更新数据库
	 */
	public function update_Shop($shop, $add = true) {
		$sql = $this->top->sqls($shop);
		$params['ql'] = "update shop set $sql";
		$shop = $this->top->tql($params, SESSIONKEY);
		$shop = $shop['shop'];
	
		if ($add) $this->save_shop($shop); //更新数据库
	
		return $shop;
	}
	
	/**
	 * 更新店铺表
	 * @param $shop 店铺数据
	 */
	private function save_shop($shop) {
		unset($shop['desc'], $shop['bulletin']);
		$shop = $this->format($shop);  //内容转换
		$id = $this->getFieldByNick($shop['nick'], 'id');
		if (!$id) {
			$this->add($shop);
		} else {
			$this->where(array('nick'=>$shop['nick']))->save($shop);
		}
	}
	
	/**
	 * 橱窗数量
	 * 获取卖家店铺剩余橱窗数量
	 * 返回 remain_count，all_count，used_count
	 */
	public function get_showcase() {
		$params['ql'] = "select shop from shop.remainshowcase";
		$result = $this->top->tql($params, SESSIONKEY);
		
		return $result['shop'];
	}
	
	/**
	 * 授权登录
	 * 获取授权登录信息
	 */
	public function check_container($container) {
		$sign = $this->top->check_sign($container);  //检查授权签名是否合法
		if (!$sign) return false;
		$parameters = $this->top->Parameters($container['top_parameters']);
		if (!is_array($parameters)) return false;
		$nick = $parameters['visitor_nick'];
		$userid = cookie('userid');  //获取用户ID
		
		$User = D('User');
		$User->syn_user($nick, "", $container['top_session']); //同步淘宝用户信息
		$User->where(array('nick'=>$nick, 'userid'=>$userid))->save(array('sessionKey'=>$container['top_session'], "group"=>1));  //绑定淘宝用户到系统
		$this->syn_Shop($nick); //同步淘宝店铺信息
		$this->where(array("nick"=>$nick))->save(array("userid"=>$userid, "title"=>$nick, "status"=>77));  //更新店铺类型
		
		import('COM.Tb.Increment');
		Increment::permit_customer($nick);  //订阅主动通知
		
		return true;
	}
	
	/**
	 * 字段检查
	 * @param $shop 店铺数据
	 */
	public function format($shop) {
		if ($shop['created']) $shop['created'] = strtotime($shop['created']);
		if ($shop['modified']) $shop['modified'] = strtotime($shop['modified']);
		if ($shop['title']) {
			$shop['shop_title'] = new_addslashes($shop['title']);
			unset($shop['title']);
		}
		if ($shop['shop_score']) $shop['shop_score'] = serialize($shop['shop_score']);
	
		return $shop;
	}
}
?>
