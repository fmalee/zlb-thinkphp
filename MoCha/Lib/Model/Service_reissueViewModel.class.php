<?php
class Service_reissueViewModel extends ViewModel {
	
	// 关系视图
	public $viewFields = array(
			'Service_reissue'=>array('id','nick','tid','oid','status','reason','sid','company','memo','created','modified','invoice','_as'=>'Reissue','_type'=>'LEFT'),
			'Trade'=>array('status'=>'trade_status','buyer_nick','receiver_name', '_on'=>'Reissue.tid=Trade.tid','_type'=>'LEFT'),
			'Order'=>array('title','num_iid','num','outer_iid','sku_properties_name', '_on'=>'Reissue.oid=Orders.oid','_as'=>'Orders'),
	);
}