<?php
class RefundViewModel extends ViewModel {
	
	// 关系视图
	public $viewFields = array(
			'Refund'=>array('refund_id','tid','oid','seller_nick','status','cs_status','advance_status','refund_fee','total_fee','split_taobao_fee','split_seller_fee','reason','desc'=>'info','company_name','sid','has_good_return','good_return_time','refund_remind_timeout','created','modified','_type'=>'LEFT'),
			'Trade'=>array('created'=>'trade_created','pay_time','buyer_nick','post_fee','receiver_name', '_on'=>'Refund.tid=Trade.tid','_type'=>'LEFT'),
			'Order'=>array('title','num_iid','num','outer_iid','outer_sku_id', '_on'=>'Refund.oid=Orders.oid','_as'=>'Orders'),
	);
}