<?php
class PropsModel extends Model {
	protected $tablePrefix = 'tb_'; // 自动填充设置
	public $top;
	
	public function __construct() {
		parent::__construct();
		$this->top = new Top();
	}
	
	/**
	 * 获取宝贝属性
	 * @param $data 宝贝数据
	 *        	接受num_iid 或是$data数据
	 * @param $type 输出类型0=全部属性
	 *        	1=销售属性 2=关键属性 3=非关键属性 4=属性名称 5=属性值
	 * @param $props 直接传入需要格式化的数值
	 *        	输出：array ('颜色' => array ('1627207:28332' =>
	 *        	'灰色','1627207:28320' => '白色','1627207:28341' => '黑色'))
	 *        	输出：name pivdname:vidnam;pivdname:vidname
	 *        	get_props($data); 以 array 输出全部的props
	 *        	get_props($data, 1); 以 array 输出带销售属性的props
	 *        	get_props($id, 0, $props) 以 array 输出传入的$props
	 *        	get_props($id, 3, $props) 以 name 输出传入的$props
	 */
	public function get_props($data, $type = 0, $props = '') {
		if (!array_key_exists('cid', $data)) { // 判断数组
			$data = M('Item')->where("num_iid=$data")->field('cid,input_pids,input_str,property_alias,props')->find();
			if (!$data) return false;
		}
		$cid = $data['cid'];
		$input_pids = explode(',', $data['input_pids']);
		$input_str = explode(',', $data['input_str']);
		foreach ( $input_pids as $k => $v ) {
			$input [$v] = $input_str [$k];
		}
		$property_alias = explode(';', $data['property_alias']);
		$alias = array();
		foreach ( $property_alias as $v ) {
			$v = explode ( ':', $v );
			$key = $v [0] . ":" . $v [1]; // 组成pid:vid为key的数组
			$alias[$key] = $v [2];
		}
		
		if (! $props) $props = $data ['props']; // 是否接受传入的props
		$props = explode ( ';', $props );
		$props_arr = array ();
		foreach ($props as $prop) {
			$v = explode(':', $prop);
			$pid = $v[0];
			$vid = $v[1];
			$r = $this->where(array('pid' => $pid, 'cid' => $cid))->field('name, is_sale_prop, is_key_prop, prop_values')->find();
			$pname = $r['name'];
			if (array_key_exists($pid, $input)) {
				$name = $input[$pid];
			} elseif (array_key_exists($prop, $alias)) {
				$name = $alias[$prop];
			} else {
				$prop_values = unserialize($r['prop_values']);
				$name = $prop_values[$vid];
			}
			
			if ($type == 1) {
				if ($r ['is_sale_prop']) $props_arr[$pname][$prop] = $name;
			} elseif ($type == 2) {
				if ($r ['is_key_prop']) $props_arr[$pname][$prop] = $name;
			} elseif ($type == 3) {
				if ($r ['is_sale_prop'] || $r ['is_key_prop']) continue;
				$props_arr[$pname][$prop] = $name;
			} elseif ($type == 4) {
				$props_arr[$pname] = $pname . ":" . $name;
			} elseif ($type == 5) {
				$props_arr[$pname] = $name;
			} else {
				$props_arr[$pname][$prop] = $name;
			}
		}
		if ($type == 4)	$props_arr = implode ( ';', $props_arr );
		if ($type == 5)	$props_arr = implode ( ':', $props_arr );
		return $props_arr;
	}
	
	/**
	 * 更新单个系统类目的属性
	 */
	public function syn_Prop($cid) {
		$fields = "is_input_prop, pid, parent_pid, parent_vid, name, is_key_prop, is_sale_prop, is_color_prop, is_enum_prop, is_item_prop, must, multi, prop_values, status, sort_order, child_template, is_allow_alias";
		$params['ql'] = "select $fields from itemprops where cid=$cid";
		$props = $this->top->tql($params);
		$props = $props['item_props']['item_prop'];
		
		if ($props) $this->where(array('cid'=>$cid))->delete(); //清空对应类目表数据
		$modified = NOW_TIME;
		foreach ($props as $prop ) {
			$prop = $this->format($prop);
			$prop['cid'] = $cid;
			$prop['modified'] = $modified;
			$this->add($prop);
		}
	}
	
	/**
	 * 数据检查
	 * @param $prop 类目属性
	 */
	public function format($prop) {
		if (isset($prop['is_allow_alias'])) $prop['is_allow_alias'] = str2bool($prop['is_allow_alias']);
		if (isset($prop['is_color_prop'])) $prop['is_color_prop'] = str2bool($prop['is_color_prop']);
		if (isset($prop['is_enum_prop'])) $prop['is_enum_prop'] = str2bool($prop['is_enum_prop']);
		if (isset($prop['is_input_prop'])) $prop['is_input_prop'] = str2bool($prop['is_input_prop']);
		if (isset($prop['is_item_prop'])) $prop['is_item_prop'] = str2bool($prop['is_item_prop']);
		if (isset($prop['is_key_prop'])) $prop['is_key_prop'] = str2bool($prop['is_key_prop']);
		if (isset($prop['is_sale_prop'])) $prop['is_sale_prop'] = str2bool($prop['is_sale_prop']);
		if (isset($prop['multi'])) $prop['multi'] = str2bool($prop['multi']);
		if (isset($prop['must'])) $prop['must'] = str2bool($prop['must']);
		if ($prop['prop_values']) {
			$prop['prop_values'] = $this->foramt_value($prop['prop_values']);
			$prop['prop_values'] = serialize($prop['prop_values']);
		}
		
		return $prop;
	}
	
	/**
	 * 转换类目属性值格式
	 * @param $values 类目属性值
	 */
	public function foramt_value($values) {
		$values = $values['prop_value'];
		$values_arr = array();
		foreach ($values as $value) {
			$vid = $value['vid'];
			$values_arr[$vid] = $value['name'];
		}
		return $values_arr;
	}
}