<?php

/**
 * 淘宝用户
 */
class UserModel extends Model {
	protected $tablePrefix = 'tb_'; // 数据库前缀
	public $top;
	
	public function __construct() {
		parent::__construct();
		$this->top = new Top();
	}
	
	/**
	 * 下载多个用户数据
	 * @param $nicks 用户昵称。可传入数组或是字符串， 最多40个
	 * 所有字段：user_id, nick, sex, buyer_credit, seller_credit, location, created, last_visit, type, consumer_protection
	 */
	public function syn_users($nicks, $fields = "") {
		if (is_array($nicks)) $nicks = implode(',', $nicks);  //将数组转换成字符串
		if (!$fields) {
			$fields = "user_id, nick, sex, buyer_credit, seller_credit, location, created, last_visit, type, consumer_protection";
		} else {
			$fields .=", user_id, nick";
		}
		$params['ql'] = "select ".$fields." from users where nicks=$nicks";
		$users = $this->top->tql($params);
		$users = $users['users']['user'];
		return $users;
	}
	
	/**
	 * 下载单个用户数据
	 * @param $nick 用户昵称
	 * 所有字段：user_id, uid, nick, sex, buyer_credit, seller_credit, location, created, last_visit, birthday, type, has_more_pic, item_img_num, item_img_size, prop_img_num, prop_img_size, auto_repost, promoted_type, status, alipay_bind, consumer_protection, alipay_account, alipay_no, avatar, liangpin, sign_food_seller_promise, has_shop, is_lightning_consignment, has_sub_stock, is_golden_seller, vip_info, email, magazine_subscribe, vertical_market, online_gaming
	 */
	public function syn_user($nick, $fields = "", $session = SESSIONKEY) {
		if (!$fields) {
			$fields = "user_id, uid, nick, vip_info, promoted_type, location, birthday, sex, type, email, status, auto_repost, alipay_bind, alipay_account, alipay_no, avatar, has_shop, consumer_protection, is_lightning_consignment, liangpin, sign_food_seller_promise, has_sub_stock, is_golden_seller, magazine_subscribe, vertical_market, buyer_credit, seller_credit, last_visit, created";
		} else {
			$fields .=", user_id, nick";
		}
		$params['ql'] = "select ".$fields." from user where nick=$nick";
		$user = $this->top->tql($params, $session);
		
		$this->update_user($user['user']);  //更新用户表
		
		return $user;
	}
	
	/**
	 * 更新用户表
	 * @param $user 用户数据
	 */
	private function update_user($user) {
		$user = $this->check($user);  //内容转换
		$nick = $this->getFieldByUser_id($user['user_id'], 'nick');
		if (!$nick) {
			$this->add($user);
		} else {
			$this->where(array('nick'=>$nick))->save($user);
		}
	}
	
	/**
	 * 根据user_id取出nick
	 * @param $user_id 用户ID
	 */
	public function userid2nick($user_id) {
		return $this->getFieldByUser_id($user_id, 'nick');
	}
	
	/**
	 * 根据nick取出user_id
	 * @param $nick 用户昵称
	 */
	public function nick2userid($nick) {
		return $this->getFieldByNick($nick, 'user_id');
	}
	
	/**
	 * 根据nick取出sessionkey
	 * @param $nick 用户昵称
	 */
	public function nick2session($nick) {
		return $this->getFieldByNick($nick, 'sessionkey');
	}
	
	/**
	 * 退款内容检查和转换
	 * @param $user 用户数据
	 */
	public function check($user) {
		if (isset($user['location'])) {
			$user['state'] = $user['location']['state'];
			$user['city'] = $user['location']['city'];
			unset($user['location']);
		}
		if ($user['created']) $user['created'] = strtotime($user['created']);
		if ($user['last_visit']) $user['last_visit'] = strtotime($user['last_visit']);
		if ($user['buyer_credit']) $user['buyer_credit'] = serialize($user['buyer_credit']);
		if ($user['seller_credit']) $user['seller_credit'] = serialize($user['seller_credit']);
	
		return $user;
	}
}
?>
