<?php
class RefundModel extends Model {
	protected $tablePrefix = 'tb_'; // 数据库前缀
	public $Taoapi;
	
	public function __construct() {
		parent::__construct();
		$this->Taoapi = new Taoapi();
	}

	/**
	 * 下载退款数据
	 * @param $start_modified 修改时间开始
	 * @param $end_modified 修改时间结束
	 * @param $page_no 页码
	 * @param $page_size 每页条数
	 */
	public function synRefunds($start_modified = "", $end_modified = "", $page_no = 1, $page_size=100) {
		$this->Taoapi->method = 'refunds.receive';
		$this->Taoapi->session = SESSIONKEY;
		$this->Taoapi->fields = "refund_id, tid, oid, seller_nick, status, good_status, refund_fee, payment, total_fee, desc, reason, company_name, sid, has_good_return, created, modified";
		$where = "page_no=$page_no and page_size=$page_size";
		if ($start_modified && $end_modified) {
			if ($end_modified <= $start_modified) return false;
			$where .= " and start_modified=$start_modified and end_modified=$end_modified"; 
		}
		$this->Taoapi->where = $where;
		$result = $this->Taoapi->select()->get();
		$total = $result['total_results'];

		foreach ($result['refunds_receive_get_response']['refunds']['refund']  as $refund) {
			$this->saveRefund($refund); //入库
		}
		
		return $total;
	}
	
	/**
	 * 下载单笔退款数据
	 * @param $refund_id 退款ID
	 * @param $fields 需要获取的字段
	 * @param $refund 退款数据
	 */
	public function synRefund($refund_id, $fields = "", $nick = "") {
		$this->Taoapi->session = $nick ? D('User')->nick2session($nick) : SESSIONKEY;
		$this->Taoapi->method = 'refund';
		if (!$fields) {  //生成需要获取的字段
			$this->Taoapi->fields = "refund_id,tid,oid,seller_nick,status,good_status,cs_status,advance_status,split_taobao_fee,split_seller_fee,address,refund_fee,payment,total_fee,desc,reason,company_name,sid,has_good_return,good_return_time,created,modified,refund_remind_timeout";
		} else {
			$this->Taoapi->fields = $fields . ",refund_id,modified";
		}
		$this->Taoapi->where = "refund_id=$refund_id";

		$refund = $this->Taoapi->select()->get();

		$this->saveRefund($refund['refund_get_response']['refund']); //入库
		
		return $refund;
	}
	
	/**
	 * 保存退款
	 * @param $refund 退款数据
	 */
	private function saveRefund($refund) {
		if (!isset($refund['refund_id']) || empty($refund['refund_id'])) return false;
		$refund = $this->format($refund); //格式化

		$refund_id = $this->getFieldByRefund_id($refund['refund_id'], 'refund_id');
		if ($refund_id) {
			$this->save($refund);  //更新数据
		} else {
			$this->add($refund);  //新增数据
		}

		return $refund_id;
	}
	
	/**
	 * 下载退款留言/凭证列表查询
	 * @param $refund_id 退款ID
	 * @param $page_no 页数
	 * @param $page_size 页码
	 */
	public function getMsg($refund_id, $page_no = 1, $page_size=100) {
		$this->Taoapi->session = SESSIONKEY;
		$this->Taoapi->method = 'refund.messages';
		$this->Taoapi->fields = "id,owner_id,owner_role,owner_nick,message_type,pic_urls,content,created";
		$this->Taoapi->where = "refund_id=$refund_id";

		$results = $this->Taoapi->select()->get();

		$messages['total'] = $results['refund_messages_get_response']['total_results'];
		$messages['messages'] = $results['refund_messages_get_response']['refund_messages']['refund_message'];
		
		return $messages;
	}
	
	/**
	 * 创建退款留言/凭证
	 * @param $refund_id 退款编号
	 * @param $content 留言内容。最大长度: 400个字节
	 * @param $image 图片（凭证）。类型: JPG,GIF,PNG;最大为: 500K
	 */
	public function addMsg($refund_id, $content, $image="") {
		$this->Taoapi->session = SESSIONKEY;
		$this->Taoapi->method = 'refund.messages';
		$this->Taoapi->where = "refund_id=$refund_id";
		
		$results = $this->Taoapi->update($content)->get();

		$id = $results['refund_messages_update_response']['refund_message']['id'];
	
		return $id;
	}
	
	/**
	 * 退款内容检查和转换
	 * @param $refund 退款数据
	 */
	public function format($refund) {
		//if ($refund['refund_id']) $refund['refund_id'] = number_format($refund['refund_id'],0,',','');
		//if ($refund['tid']) $refund['tid'] = number_format($refund['tid'],0,',','');
		//if ($refund['oid']) $refund['oid'] = number_format($refund['oid'],0,',','');
		if ($refund['created']) $refund['created'] = strtotime($refund['created']);
		if ($refund['good_return_time']) $refund['good_return_time'] = strtotime($refund['good_return_time']);
		if ($refund['modified']) $refund['modified'] = strtotime($refund['modified']);
		if (isset($refund['has_good_return'])) $refund['has_good_return'] = str2bool($refund['has_good_return']);
		if ($refund['refund_remind_timeout']) $refund['refund_remind_timeout'] = serialize($refund['refund_remind_timeout']);
		if ($refund['company_name']) $refund['company_name'] = trim($refund['company_name']);
		if ($refund['desc']) $refund['desc'] = new_addslashes($refund['desc']);
		
		return $refund;
	}
}