<?php
class SkuModel extends Model {
	protected $tablePrefix = 'tb_'; // 自动填充设置
	public $top;
	
	public function __construct() {
		parent::__construct();
		$this->top = new Top();
	}

	/**
	 * 添加SKUS
	 * @param $num_iid 宝贝ID
	 * @param $item 商品数据
	 */
	public function add_skus($num_iid, $item) {
		$this->where(array('num_iid'=>$num_iid))->save(array('status'=>88));
		
		$skus = $item['skus']['sku'];
		if (!$skus) return false;
		$Porps = D('Props');
		foreach ($skus as $sku) {
			$sku = $this->format($sku);
			$sku['num_iid'] = $num_iid;
			$sku['properties_name'] = $Porps->get_props($item, 4, $sku['properties']);
			
			$has = $this->getFieldBySku_id($sku['sku_id'], 'num_iid'); //是否存在记录
			if (!$has) {
				$this->add($sku);
			} else {
				$this->save($sku);
			}
			$this->where(array('num_iid'=>$num_iid, 'status'=>88, 'stock'=>0, 'failed'=>0))->delete(); //删除多余的无库存的SKUS
		}
		return true;
	}
	
	/**
	 * 转换SKUS数据格式
	 * @param $sku 传入商品数据
	 */
	public function format($sku) {
		if ($sku['properties_name']) $sku['properties_name'] = new_addslashes($$sku['properties_name']);
		if ($sku['created']) $sku['created'] = strtotime($sku['created']);
		if ($sku['modified']) $sku['modified'] = strtotime($sku['modified']);
		$sku['status'] = 99;
	
		return $sku;
	}
}