<?php
class InvoiceModel extends Model {
	protected $tablePrefix = 'tb_'; // 自动填充设置
	
    // 自动验证设置
    protected $_validate = array(
        array('title', 'require', '请填写配货单名称！', 1),//1为必须验证
        array('title', '', '配货单名称已经存在！', 0, 'unique', self::MODEL_INSERT),
    );	
	
    // 自动填充设置
    protected $_auto = array(
        array('nick', NICK, 1),
		array('created', 'time', 1, 'function'),
		array('modified', 'time', 3, 'function'),
    );
	
    /*//定义关联
    protected $_link = array(
    	'Trade'=> array(
    			'mapping_type' => HAS_MANY,
    			'class_name'=>'Invoice_trade',
    			'foreign_key'=>'did',
		),
    	'Order'=> array(
    				'mapping_type' => MANY_TO_MANY,
    				'class_name'=>'Invoice_order2',
    				'foreign_key'=>'did',
    			'parent_key'=>'tid',
    			'relation_foreign_key'=>'id',
    			'relation_table'=>'tb_invoice_trade'
		),
    );
    
    /*
    // 关系视图
    public $viewFields = array(
    	'Invoice'=>array('id','nick','title','memo','price','created','modified','_type'=>'LEFT'),
    	'Invoice_trade'=>array('tid','source','_on'=>'Invoice.id=Trade.did','_as'=>'Trade','_type'=>'RIGHT'),
    	'Invoice_order'=>array('oid','num_iid','outer_id','pic_path','sku_id','properties','properties_name','num','created'=>'order_created', '_on'=>'Invoice.id=Orders.did AND Trade.tid=Orders.tid','_as'=>'Orders'),
    );*/
}