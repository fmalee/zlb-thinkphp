<?php
class CatsModel extends Model {
	protected $tablePrefix = 'tb_'; // 自动填充设置
	public $top;
	
	public function __construct() {
		parent::__construct();
		$this->top = new Top();
	}
	
	/**
	 * 同步店铺自有类目
	 * @param $nick 昵称
	 */
	public function syn_sellercats($nick) {
		$fields = "type, cid, parent_cid, name, pic_url, sort_order";
		$params['ql'] = "select $fields from sellercats.list where nick=$nick";
		$cats= $this->top->tql($params);
		$cats = $cats['seller_cats']['seller_cat'];
		if ($cats)  $this->where(array('model'=>'seller', 'nick'=>$nick))->delete(); //清空对应类目表数据
		$modified = NOW_TIME;
		foreach ($cats as $cat) {
			$cat['modified'] = $modified;
			$cat['model'] = 'seller';
			$cat['nick'] = $nick;
			$this->add($cat);
		}
		
		return true;
	}
	
	/**
	 * 更新宝贝类目
	 * @param $cid 类目
	 */
	public function syn_itemcats($cid) {
		$fields = "cid,parent_cid,name,is_parent,status,sort_order";
		$params['ql'] = "select $fields from itemcats where parent_cid=$cid";
		$cats= $this->top->tql($params);
		$cats = $cats['item_cats']['item_cat'];
		
		if ($cid == 0) $this->where(array('model'=>'item'))->delete(); //清空对应类目表数据
		$modified = NOW_TIME;
		foreach ($cats as $cat) {
			$cat['is_parent'] = str2bool($cat['is_parent']); // 进行布尔值转换
			$cat['modified'] = $modified;
			$cat['model'] = 'item';
			unset($cat['status']);
			$this->add($cat);
			if ($cat['is_parent'] && $cid > 0) $this->syn_itemcats($cat['cid']);
		}
		
		return true;
	}
	
	/**
	 * 修复栏目层级关系
	 * @param array $cats 栏目集合
	 */
	public function repair($cats) {
		if (!is_array($cats)) return false;
		foreach ($cats as $cid=>$cat) {
			$cache['arrparent_cid'] = $this->get_arrparent_cid($cid);
			$cache['arrchild_cid'] = $this->get_arrchild_cid($cid);
			
			//检查所有父id 子栏目id 等相关数据是否正确，不正确更新
			if ($cats[$cid]['arrparent_cid'] != $cache['arrparent_cid'] || $cats[$cid]['arrchild_cid'] != $cache['arrchild_cid']) {
				$this->where("cid=$cid")->save($cache);
			}
		}
		return true;
	}
	
	/**
	 * 获取父类目ID列表
	 * @param integer $catid 栏目ID
	 * @param array $arrparentid 父目录ID
	 * @param integer $n 查找的层次
	 */
	private function get_arrparent_cid($cid, $arrparent_cid = '', $n = 1) {
		if ($n > 5) return false;
		$parent_cid = $this->getFieldByCid($cid ,'parent_cid');
		
		$arrparent_cid = $arrparent_cid ? $parent_cid . ',' . $arrparent_cid : $parent_cid;
		if ($parent_cid) {
			$arrparent_cid = $this->get_arrparent_cid($parent_cid, $arrparent_cid, ++ $n);
		}
		return $arrparent_cid;
	}
	
	/**
	 * 获取叶子类目ID列表
	 * @param $cid 栏目ID
	 */
	private function get_arrchild_cid($cid) {
		$arrchild_cid = $cid;
		$arrchilds = $this->where("parent_cid=$cid")->field('cid,parent_cid')->order('sort_order ASC')->select();
		if (is_array($arrchilds)) {
			foreach ($arrchilds as $cat) {
				if ($cat['parent_cid'] && $cat['cid'] != $cid) {
					$arrchild_cid .= ',' . $this->get_arrchild_cid($cat['cid']);
				}
			}
		}
		return $arrchild_cid;
	}
	
	/**
	 * 当前路径
	 * 返回指定类目路径层级
	 * 当用CAHCE储存时应用
	 *
	 * @param $cid 类目id
	 * @param $symbol 栏目间隔符
	 */
	function catpos($cid, $symbol = ' > ') {
		$cat = $this->where("cid=$cid")->getField('name,arrparent_cid');
		if (!$cat) return '';
		$name = $cat['name'];
		$cats = array();
		$pos = "";
		$cats = array_filter( explode ( ',', $cat ['arrparent_cid'] ) );
		foreach ( $cats as $cid ) {
			$cat = $this->getFieldByCid($cid, 'name');
			$pos .= $cat.$symbol;
		}
		return $pos.$name;
	}
	
	/**
	 * 当前位置
	 * 来自admin的写法
	 * 会多出$symbol后缀
	 * final public static
	 *
	 * @param $id 栏目id
	 */
	function catpos1($cid, $symbol = ' > ') {
		$cat = $this->where("cid=$cid")->getField('name,arrparent_cid');
		$parent_cid = $cat ['parent_cid'];
		$pos = '';
		if ($parent_cid)
			$pos = self::catpos ( $parent_cid, $symbol ); // 递归类目
		return $pos . $cat ['name'] . $symbol;
	}
	
	/**
	 * 当前路径
	 * 返回指定类目路径层级
	 * 当用CAHCE储存时应用
	 *
	 * @param $cid 类目id
	 * @param $symbol 栏目间隔符
	 */
	function catpos2($cid, $symbol = ' > ') {
		$cats = array ();
		$cats = cache('cats');
		if (! isset ( $cats [$cid] )) return '';
		$pos = '';
		$arrparentid = array_filter ( explode ( ',', $cats [$cid] ['arrparentid'] . ',' . $cid ) );
		foreach ( $arrparentid as $cid ) {
			$pos .= $cats [$cid] ['name'] . $symbol;
		}
		return $pos;
	}
}