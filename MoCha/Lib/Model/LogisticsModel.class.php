<?php
class LogisticsModel extends Model {
	protected $tablePrefix = 'tb_'; // 自动填充设置
	public $Top, $Taoapi;
	
	public function __construct() {
		parent::__construct();
		$this->Top = new Top();
		$this->Taoapi = new Taoapi();

		defined('SESSIONKEY') 	or define('SESSIONKEY', null);
	}
	
	/**
	 * 获取物流公司信息
	 *
	 * @param $api 物流查询接口.
	 *  0:淘宝；1：金蝶kuaidi001
	 */
	public function getCompanies($key = 'name') {
		$field = 'name';
		if ($key <> 'name') $field = $key.',name';
		
		$logistics = $this->field($field)->limit(0,200)->order('listorder DESC, id DESC')->select();
		foreach ($logistics as $v) {
			$logistic[$v[$key]] = $v['name'];
		}
		return $logistic;
	}
	
	/**
	 * 查询卖家地址库
	 * @param $where 默认地址库
	 * no_def:查询非默认地址
	 * get_def:查询默认取货地址
	 * cancel_def:查询默认退货地址
	 * @param $area_id 地址库ID
	 */
	public function synAddress($where = "", $area_id = 0) {
		if ($where) $where = " where rdef=$where";
		$params['ql'] = "select area_id from taobao.logistics.address.search $where";
		$addresses = $this->top->tql($params, SESSIONKEY);
		$addresses = $addresses['addresses']['address_result'];
	
		if (!$area_id) return $addresses;
	
		foreach ($addresses as $address) {  //编辑卖家地址库
			if (in_array ($area_id, $address)) return $address;
		}
	}
	
	/**
	 * 新增卖家地址库
	 * @param $address 地址数据集合
	 */
	public function addAddress($address) {
		if ($address['memo']) {
			$params['memo'] = $address['memo'];
			$address['memo'] = "#memo#";
		}
		$params['addr'] = $address['addr'];
		$address['addr'] = "#addr#";
		$field = implode(",", array_keys($address));  //拼装字段列表
		$value = implode(",", array_values($address)); //拼装值列表
	
		$params['ql'] = "insert into logistics.address ($field) values ($value)";
		$result = $this->top->tql($params, SESSIONKEY);
	
		if ($result['address_result']) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 修改卖家地址库
	 * @param $contact_id 地址库ID
	 * @param $address 地址数据集合
	 */
	public function modifyAddress($contact_id, $address) {
		if ($address['memo']) {
			$params['memo'] = $address['memo'];
			$address['memo'] = "#memo#";
		}
		$params['addr'] = $address['addr'];
		$address['addr'] = "#addr#";
	
		$update = $this->top->sqls($address, ',');
		$params['ql'] = "update taobao.logistics.address.modify set $update where contact_id=".$contact_id;
		$result = $this->top->tql($params, SESSIONKEY);
	
		if ($result['address_result']) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 删除卖家地址库
	 * @param $contact_id 地址库ID
	 */
	public function removeAddress($contact_id) {
		$params['ql'] = "delete from taobao.logistics.address.remove where contact_id=".$contact_id;
		$result = $this->top->tql($params, SESSIONKEY);
	
		if ($result['address_result']) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 获取用户下所有运费模板
	 * template_name返回name fee_list
	 */
	public function synTemplates($fields = "") {
		$this->Taoapi->method = 'delivery.templates';
		$this->Taoapi->session = SESSIONKEY;
		if (!$fields) {
			$this->Taoapi->fields = "template_id, template_name, assumer ,valuation, query_express, query_ems, query_cod, query_post, supports, created, modified";
		} else {
			$this->Taoapi->fields = $fields;
		}
		$resp = $this->Taoapi->select()->get();

		return $resp['delivery_templates_response']['delivery_templates']['delivery_template'];
	}
	
	/**
	 * 获取物流公司信息
	 * 表中有kuaidi100的信息，慎用
	 */
	public function synCompany() {
		$fields = "id,code,name,reg_mail_no";
		$params['ql'] = "select $fields from logistics.companies";
		$companies = $this->top->tql($params);
	
		return $companies['logistics_companies']['logistics_company'];
	}
	
	/**
	 * 物流流转信息查询
	 * 表中有kuaidi100的信息，慎用
	 */
	public function synTrace($tid, $seller_nick) {
		$where = "tid=$tid and seller_nick=$seller_nick";
		$params['ql'] = "select out_sid from taobao.logistics.trace.search where $where";
		$traces = $this->top->tql($params);
		$traces['trace_list'] = $traces['trace_list']['transit_step_info'];
	
		return $traces;
	}
	
	/**
	 * 查询物流信息
	 * 金蝶接口
	 */
	public function getDelivery($com, $nu, $type = 0) {
		$apikey = '15db32338f3207ee';
		$info['no'] = $nu;
		if ($type) {
			$info['company'] = $com;
			$com = $this->getFieldByName($com, 'kingdee');
		} else {
			$info['company'] = $this->getFieldByKingdee($com, 'name');
		}
		$url = "http://api.kuaidi100.com/api?id=" . $apikey . "&com=" . $com . "&nu=" . $nu . "&show=1&muti=1&order=desc";
		$info['link'] = "http://www.kuaidi100.com/chaxun?com=" . $com . "&nu=" . $nu;
		$resp = file_get_contents($url);
		$resp = @simplexml_load_string($resp);
		$resp = (array)$resp;
		if ($resp['status']) {
			$state = $resp ['state'];
			if ($state == 2) {
				$info['state'] = "疑难件";
			} elseif ($state == 3) {
				$info['state'] = "已签收";
			} elseif ($state == 4) {
				$info['state'] = "已退货";
			} else {
				$info['state'] = "送货中";
			}
			$i = 0;
			foreach($resp['data'] as $v) {
				$v = (array) $v;
				$info['message'][$i]['time'] = $v['time'];
				$info['message'][$i]['context'] = $v['context'];
				$i ++;
			}
		} else {
			$info['message'] = $resp['message'];
			$info['state'] = "无查询结果";
		}
		return $info;
	}
}