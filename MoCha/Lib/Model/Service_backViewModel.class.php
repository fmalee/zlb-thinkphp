<?php
class Service_backViewModel extends ViewModel {
	
	// 关系视图
	public $viewFields = array(
			'Service_back'=>array('id','nick','tid','oid','status','reason','sid','company','memo','created','modified','_as'=>'Back','_type'=>'LEFT'),
			'Trade'=>array('status'=>'trade_status','buyer_nick','receiver_name', '_on'=>'Back.tid=Trade.tid','_type'=>'LEFT'),
			'Order'=>array('title','num_iid','num','outer_iid','sku_properties_name', '_on'=>'Back.oid=Orders.oid','_as'=>'Orders'),
	);
}