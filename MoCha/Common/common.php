<?php
/*
 * 项目扩展函数库
 */

/**
 * 加密解密
 * @param type $string 明文 或 密文  
 * @param type $operation DECODE表示解密,其它表示加密  
 * @param type $key 密匙  
 * @param type $expiry 密文有效期  
 * @return string 
 */
function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0) {
	// 动态密匙长度，相同的明文会生成不同密文就是依靠动态密匙
	$ckey_length = 4;
	// 密匙
	$key = md5(($key ? $key : C("AUTHCODE")));
	// 密匙a会参与加解密
	$keya = md5(substr($key, 0, 16));
	// 密匙b会用来做数据完整性验证
	$keyb = md5(substr($key, 16, 16));
	// 密匙c用于变化生成的密文
	$keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), -$ckey_length)) : '';
	// 参与运算的密匙
	$cryptkey = $keya . md5($keya . $keyc);
	$key_length = strlen($cryptkey);
	// 明文，前10位用来保存时间戳，解密时验证数据有效性，10到26位用来保存$keyb(密匙b)，解密时会通过这个密匙验证数据完整性
	// 如果是解码的话，会从第$ckey_length位开始，因为密文前$ckey_length位保存 动态密匙，以保证解密正确  
	$string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
	$string_length = strlen($string);
	$result = '';
	$box = range(0, 255);
	$rndkey = array();
	// 产生密匙簿
	for ($i = 0; $i <= 255; $i++) {
		$rndkey[$i] = ord($cryptkey[$i % $key_length]);
	}
	// 用固定的算法，打乱密匙簿，增加随机性，好像很复杂，实际上对并不会增加密文的强度
	for ($j = $i = 0; $i < 256; $i++) {
		$j = ($j + $box[$i] + $rndkey[$i]) % 256;
		$tmp = $box[$i];
		$box[$i] = $box[$j];
		$box[$j] = $tmp;
	}
	// 核心加解密部分
	for ($a = $j = $i = 0; $i < $string_length; $i++) {
		$a = ($a + 1) % 256;
		$j = ($j + $box[$a]) % 256;
		$tmp = $box[$a];
		$box[$a] = $box[$j];
		$box[$j] = $tmp;
		// 从密匙簿得出密匙进行异或，再转成字符
		$result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
	}
	if ($operation == 'DECODE') {
		// substr($result, 0, 10) == 0 验证数据有效性
		// substr($result, 0, 10) - time() > 0 验证数据有效性  
		// substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16) 验证数据完整性  
		// 验证数据有效性，请看未加密明文的格式  
		if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
			return substr($result, 26);
		} else {
			return '';
		}
	} else {
		// 把动态密匙保存在密文里，这也是为什么同样的明文，生产不同密文后能解密的原因
		// 因为加密后的密文可能是一些特殊字符，复制过程可能会丢失，所以用base64编码  
		return $keyc . str_replace('=', '', base64_encode($result));
	}
}

/**
 * 生成上传附件验证
 * @param $args   参数
 */
function upload_key($args) {
	$auth_key = md5(C("AUTHCODE") . $_SERVER['HTTP_USER_AGENT']);
	$authkey = md5($args . $auth_key);
	return $authkey;
}

/*
 * 产生随机字符串 
 * 产生一个指定长度的随机字符串,并返回给用户 
 * @access public 
 * @param int $len 产生字符串的位数 
 * @return string 
 */

function genRandomString($len = 6) {
	$chars = array(
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
		"l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
		"w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G",
		"H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
		"S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2",
		"3", "4", "5", "6", "7", "8", "9"
	);
	$charsLen = count($chars) - 1;
	shuffle($chars);    // 将数组打乱 
	$output = "";
	for ($i = 0; $i < $len; $i++) {
		$output .= $chars[mt_rand(0, $charsLen)];
	}
	return $output;
}

/**
 * 去一个二维数组中的每个数组的固定的键知道的值来形成一个新的一维数组
 * @param $pArray 一个二维数组
 * @param $pKey 数组的键的名称
 * @return 返回新的一维数组
 */
function getSubByKey($pArray, $pKey = "", $pCondition = "") {
	$result = array();
	foreach ($pArray as $temp_array) {
		if (is_object($temp_array)) {
			$temp_array = (array) $temp_array;
		}
		if (("" != $pCondition && $temp_array[$pCondition[0]] == $pCondition[1]) || "" == $pCondition) {
			$result[] = ("" == $pKey) ? $temp_array : isset($temp_array[$pKey]) ? $temp_array[$pKey] : "";
		}
	}
	return $result;
}

/**
 * 字符截取 支持UTF8/GBK
 * @param $string
 * @param $length
 * @param $dot
 */
function str_cut($string, $length, $dot = '...') {
	$strlen = strlen($string);
	if ($strlen <= $length)
		return $string;
	$string = str_replace(array(' ', '&nbsp;', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;'), array('∵', ' ', '&', '"', "'", '“', '”', '—', '<', '>', '·', '…'), $string);
	$strcut = '';
	if (strtolower(C("DEFAULT_CHARSET")) == 'utf-8') {
		$length = intval($length - strlen($dot) - $length / 3);
		$n = $tn = $noc = 0;
		while ($n < strlen($string)) {
			$t = ord($string[$n]);
			if ($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
				$tn = 1;
				$n++;
				$noc++;
			} elseif (194 <= $t && $t <= 223) {
				$tn = 2;
				$n += 2;
				$noc += 2;
			} elseif (224 <= $t && $t <= 239) {
				$tn = 3;
				$n += 3;
				$noc += 2;
			} elseif (240 <= $t && $t <= 247) {
				$tn = 4;
				$n += 4;
				$noc += 2;
			} elseif (248 <= $t && $t <= 251) {
				$tn = 5;
				$n += 5;
				$noc += 2;
			} elseif ($t == 252 || $t == 253) {
				$tn = 6;
				$n += 6;
				$noc += 2;
			} else {
				$n++;
			}
			if ($noc >= $length) {
				break;
			}
		}
		if ($noc > $length) {
			$n -= $tn;
		}
		$strcut = substr($string, 0, $n);
		$strcut = str_replace(array('∵', '&', '"', "'", '“', '”', '—', '<', '>', '·', '…'), array(' ', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;'), $strcut);
	} else {
		$dotlen = strlen($dot);
		$maxi = $length - $dotlen - 1;
		$current_str = '';
		$search_arr = array('&', ' ', '"', "'", '“', '”', '—', '<', '>', '·', '…', '∵');
		$replace_arr = array('&amp;', '&nbsp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;', ' ');
		$search_flip = array_flip($search_arr);
		for ($i = 0; $i < $maxi; $i++) {
			$current_str = ord($string[$i]) > 127 ? $string[$i] . $string[++$i] : $string[$i];
			if (in_array($current_str, $search_arr)) {
				$key = $search_flip[$current_str];
				$current_str = str_replace($search_arr[$key], $replace_arr[$key], $current_str);
			}
			$strcut .= $current_str;
		}
	}
	return $strcut . $dot;
}

/**
 * 取得文件扩展
 * 
 * @param $filename 文件名
 * @return 扩展名
 */
function fileext($filename) {
	return strtolower(trim(substr(strrchr($filename, '.'), 1, 10)));
}

/**
 * 返回附件类型图标
 * @param $file 附件名称
 * @param $type png为大图标，gif为小图标
 */
function file_icon($file, $type = 'png') {
	$ext_arr = array('doc', 'docx', 'ppt', 'xls', 'txt', 'pdf', 'mdb', 'jpg', 'gif', 'png', 'bmp', 'jpeg', 'rar', 'zip', 'swf', 'flv');
	$ext = fileext($file);
	if ($type == 'png') {
		if ($ext == 'zip' || $ext == 'rar')
			$ext = 'rar';
		elseif ($ext == 'doc' || $ext == 'docx')
			$ext = 'doc';
		elseif ($ext == 'xls' || $ext == 'xlsx')
			$ext = 'xls';
		elseif ($ext == 'ppt' || $ext == 'pptx')
			$ext = 'ppt';
		elseif ($ext == 'flv' || $ext == 'swf' || $ext == 'rm' || $ext == 'rmvb')
			$ext = 'flv';
		else
			$ext = 'do';
	}

	if (in_array($ext, $ext_arr)) {
		return CONFIG_SITEURL . 'statics/images/ext/' . $ext . '.' . $type;
	} else {
		return CONFIG_SITEURL . 'statics/images/ext/blank.' . $type;
	}
}

/**
 * 判断是否为图片
 */
function is_image($file) {
	$ext_arr = array('jpg', 'gif', 'png', 'bmp', 'jpeg', 'tiff');
	$ext = fileext($file);
	return in_array($ext, $ext_arr) ? $ext_arr : false;
}

/**
 * 调试，用于保存数组到txt文件 正式生产删除 array2file($info, 'post.txt');
 */
function array2file($array, $filename) {
	file_exists($filename) or touch($filename);
	file_put_contents($filename, var_export($array, TRUE));
}

/**
 * 安全过滤函数
 *
 * @param $string
 * @return string
 */
function safe_replace($string) {
	$string = str_replace('%20', '', $string);
	$string = str_replace('%27', '', $string);
	$string = str_replace('%2527', '', $string);
	$string = str_replace('*', '', $string);
	$string = str_replace('"', '&quot;', $string);
	$string = str_replace("'", '', $string);
	$string = str_replace('"', '', $string);
	$string = str_replace(';', '', $string);
	$string = str_replace('<', '&lt;', $string);
	$string = str_replace('>', '&gt;', $string);
	$string = str_replace("{", '', $string);
	$string = str_replace('}', '', $string);
	return $string;
}

/**
 * 返回经stripslashes处理过的字符串或数组
 * @param $string 需要处理的字符串或数组
 * @return mixed
 */
function new_stripslashes($string) {
	if (!is_array($string))
		return stripslashes($string);
	foreach ($string as $key => $val)
		$string[$key] = new_stripslashes($val);
	return $string;
}

/**
 * 返回经addslashes处理过的字符串或数组
 * @param $string 需要处理的字符串或数组
 * @return mixed
 */
function new_addslashes($string) {
	if (!is_array($string))
		return addslashes($string);
	foreach ($string as $key => $val)
		$string[$key] = new_addslashes($val);
	return $string;
}

/**
 * 分页输出
 * @param type $Total_Size 总记录数
 * @param type $Page_Size 分页大小
 * @param type $Current_Page 当前页
 * @param type $listRows 显示页数
 * @param type $PageParam 分页参数
 * @param type $PageLink 分页链接
 * @param type $Static 是否(伪)静态
 * @return \Page 
 * 其他说明 ：当开启静态的时候 $PageLink 传入的是数组，数组格式
 * array(
	"index"=>"http://www.abc3210.com/192.html",//这种是表示当前是首页，无需加分页1
	"list"=>"http://www.abc3210.com/192-{page}.html",//这种表示分页非首页时启用
	)
 */
function page($Total_Size = 1, $Page_Size = 0, $Current_Page = 1, $listRows = 6, $PageParam = '', $PageLink = '', $Static = FALSE, $TP = "") {
	static $_pageCache = array();
	$cacheIterateId = md5($Total_Size . $Page_Size . $Current_Page . $listRows . $PageParam);
	if (isset($_pageCache[$cacheIterateId]))
		return $_pageCache[$cacheIterateId];

	import('ORG.Util.Page',APP_PATH.'Lib');
	if ($Page_Size == 0) {
		$Page_Size = C("PAGE_LISTROWS");
	}
	if (empty($PageParam)) {
		$PageParam = C("VAR_PAGE");
	}
	//生成静态，需要传递一个常量URLRULE，来生成对应规则
	if (empty($PageLink) && $Static == true) {
		$P = explode("~", URLRULE);
		$PageLink = array();
		$PageLink['index'] = $P[0];
		$PageLink['list'] = $P[1];
	}
	if (empty($TP)) {
		$TP = '共有{recordcount}条信息&nbsp;{pageindex}/{pagecount}&nbsp;{first}{prev}&nbsp;{liststart}{list}{listend}&nbsp;{next}{last}';
	}
	$Page = new Page($Total_Size, $Page_Size, $Current_Page, $listRows, $PageParam, $PageLink, $Static);
	$Page->SetPager('default', $TP, array("listlong" => "6", "first" => "首页", "last" => "尾页", "prev" => "上一页", "next" => "下一页", "list" => "*", "disabledclass" => ""));

	$_pageCache[$cacheIterateId];
	return $Page;
}

/**
 * 取得URL地址中域名部分
 * @param type $url 
 * @return \url 返回域名
 */
function urlDomain($url) {
	if (preg_match_all('#https?://(.*?)($|/)#m', $url, $Domain)) {
		return $Domain[0][0];
	}
	return false;
}

/**
 * 获取当前页面完整URL地址
 */
function get_url() {
	$sys_protocal = isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://';
	$php_self = $_SERVER['PHP_SELF'] ? safe_replace($_SERVER['PHP_SELF']) : safe_replace($_SERVER['SCRIPT_NAME']);
	$path_info = isset($_SERVER['PATH_INFO']) ? safe_replace($_SERVER['PATH_INFO']) : '';
	$relate_url = isset($_SERVER['REQUEST_URI']) ? safe_replace($_SERVER['REQUEST_URI']) : $php_self . (isset($_SERVER['QUERY_STRING']) ? '?' . safe_replace($_SERVER['QUERY_STRING']) : $path_info);
	return $sys_protocal . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '') . $relate_url;
}

// 判断配货单状态
function invoice_status($str) {
	switch ($str) {
	case 'trade' :
		return "订单";
		break;
	case 'niffer' :
		return "换货";
		break;
	case 'reissue' :
		return "补发";
		break;
	case 'add' :
		return "手动";
		break;
	case 'edit' :
		return "修改";
		break;
	case 'mix' :
		return "综合";
		break;
	default :
		return "订单";
	}
}

// 判断上下架状态
function approve_status($str) {
	if ($str == 'onsale') {
	return '下架';
	} else {
	return '上架';
	}
}

// 判断评价状态
function getRate($str1, $str2) {
	if ($str1 == 'true' && $str2 == 'true')
	return "双方已评";
	else if ($str1 == 'false' && $str2 == 'true')
	return "我已评价";
	else if ($str1 == 'true' && $str2 == 'false')
	return "对方已评价";
	else
	return "双方未评价";
}

// 判断交易类型
function getTypes($str) {
switch ($str) {
	case 'fixed' :
		return "一口价";
		break;
	case 'auction' :
		return "拍卖";
		break;
	case 'guarantee_trade' :
		return "一口价、拍卖";
		break;
	case 'auto_delivery' :
		return "自动发货";
		break;
	case 'independent_simple_trade' :
		return "旺店入门版交易";
		break;
	case 'independent_shop_trade' :
		return "旺店标准版交易";
		break;
	case 'ec' :
		return "直冲";
		break;
	case 'cod' :
		return "货到付款";
		break;
	case 'fenxiao' :
		return "分销";
		break;
	case 'game_equipment' :
		return "游戏装备";
		break;
	case 'shopex_trade' :
		return "ShopEX交易";
		break;
	case 'netcn_trade' :
		return "万网交易";
		break;
	case 'external_trade' :
		return "统一外部交易";
		break;
	default :
		return $str;
	}
}

// 判断货到付款状态
function CodStatus($str) {
	switch ($str) {
	case 'ACCEPTED_BY_COMPANY' :
		return '接单成功';
	case 'REJECTED_BY_COMPANY' :
		return '接单失败';
	case 'RECIEVE_TIMEOUT' :
		return '接单超时';
	case 'TAKEN_IN_SUCCESS' :
		return '揽收成功 ';
	case 'TAKEN_IN_FAILED' :
		return '揽收失败';
	case 'RECIEVE_TIMEOUT' :
		return '揽收超时';
	case 'SIGN_IN' :
		return '签收成功';
	case 'REJECTED_BY_OTHER_SIDE' :
		return '签收失败';
	case 'WAITING_TO_BE_SENT' :
		return '订单等待发送给物流公司';
	case 'CANCELED' :
		return '用户取消物流订单';
	default :
		return '初始状态';
	}
}
// 判断交易状态
function TradeStatus($str) {
	switch ($str) {
	case 'WAIT_BUYER_PAY' :
		return "等待付款";
	case 'WAIT_SELLER_SEND_GOODS' :
		return "等待发货";
	case 'WAIT_BUYER_CONFIRM_GOODS' :
		return "已发货";
	case 'TRADE_BUYER_SIGNED' :
		return "买家签收";
	case 'TRADE_FINISHED' :
		return "交易成功";
	case 'TRADE_CLOSED' :
		return "交易关闭";
	case 'TRADE_CLOSED_BY_TAOBAO':
		return "交易关闭";
	case 'TRADE_NO_CREATE_PAY' :
		return "没有创建支付宝交易";
	default :
		return $str;
	}
}

// 返回对应交易动作。
function TradeAction($str) {
	$action = array ();
	switch ($str) {
	case 'WAIT_BUYER_PAY' :
		$action ['title'] = '关闭交易';
		$action ['action'] = 'close';
		$action ['price'] ['title'] = '修改价格';
		$action ['price'] ['action'] = 'price';
		break;
	case 'WAIT_SELLER_SEND_GOODS' :
		$action ['title'] = '立即发货';
		$action ['action'] = 'logistics';
		$action ['address'] ['title'] = '修改收货地址';
		$action ['address'] ['action'] = 'address';
		$action ['sku'] ['title'] = '修改订单属性';
		$action ['sku'] ['action'] = 'sku';
		break;
	case 'WAIT_BUYER_CONFIRM_GOODS' :
		$action ['title'] = '延长收货时间';
		$action ['action'] = 'delay';
		break;
	default :
		return '';
	}
	return $action;
}

// 物流方式
function getpost($shipping_type) {
	switch ($shipping_type) {
	case 'free' :
		return "卖家包邮";
	case 'ems' :
		return "EMS";
	case 'ORDINARY' :
		return "平邮";
	case 'post' :
		return "平邮";
	case 'FAST' :
		return "快递";
	case 'express' :
		return "快递";
	case 'virtual' :
		return "虚拟物品";
	default :
		return $shipping_type;
		;
	}
}

// 物流状态
function getstatus($status) {
	switch ($status) {
	case 'CREATED' :
		return "订单已创建";
	case 'RECREATED' :
		return "订单重新创建";
	case 'CANCELLED' :
		return "订单已取消";
	case 'CLOSED' :
		return "订单关闭";
	case 'SENDING' :
		return "等候发送";
	case 'ACCEPTING' :
		return "等待接单";
	case 'ACCEPTED' :
		return "已接单";
	case 'REJECTED' :
		return "不接单";
	case 'PICK_UP' :
		return "揽收成功";
	case 'PICK_UP_FAILED' :
		return "揽收失败";
	case 'LOST' :
		return "丢单";
	case 'REJECTED_BY_RECEIVER' :
		return "对方拒签";
	case 'ACCEPTED_BY_RECEIVER' :
		return "对方已签收";
	default :
		return "没有物流信息";
	}
}

// 判断退款状态
function RefundStatus($str) {
	switch ($str) {
	case 'WAIT_SELLER_AGREE' :
		return '等待卖家同意退款申请';
		break;
	case 'WAIT_BUYER_RETURN_GOODS' :
		return '等待买家退货';
		break;
	case 'WAIT_SELLER_CONFIRM_GOODS' :
		return '等待卖家确认收货';
		break;
	case 'SELLER_REFUSE_BUYER' :
		return '卖家拒绝退款 ';
		break;
	case 'CLOSED' :
		return '退款关闭';
		break;
	default :
		return '退款成功';
		break;
	}
}

// 判断客服介入状态
function CsStatus($str) {
	switch ($str) {
		case 1 :
			return '无需客服介入';
			break;
		case 2 :
			return '已申请客服介入';
			break;
		case 3 :
			return '客服已介入';
			break;
		case 4 :
			return '客服初审完成 ';
			break;
		case 5 :
			return '客服主管复审失败';
			break;
		case 6 :
			return '客服处理完成';
			break;
		default :
			return '无需客服介入';
			break;
	}
}

// 判断客服介入状态
function AdvanceStatus($str) {
	switch ($str) {
		case 0 :
			return '无需垫付';
			break;
		case 1 :
			return '已申请垫付 ';
			break;
		case 2 :
			return '垫付完成';
			break;
		case 3 :
			return '已垫付，卖家拒绝 ';
			break;
		case 4 :
			return '垫付关闭';
			break;
		case 5 :
			return '垫付分账成功';
			break;
		default :
			return '无需垫付';
			break;
	}
}
/**
 * 判断字段长度
 * @param str 输入字符
 * @param $len 基准长度
 */
function str_length($str, $len = "") {
	$i = 0;
	if (empty ( $str ))
	return 0;
	$detect = mb_detect_encoding ( $str );
	$iconv = iconv ( $detect, "GB2312//IGNORE", $str );
	$lenth = strlen ( $iconv );
	while ( $i < $lenth ) {
	if (preg_match ( "/^[" . chr ( 0xa1 ) . "-" . chr ( 0xff ) . "]+$/", $iconv [$i] )) {
		$i += 2;
	} else {
		$i += 1;
	}
	}
	if ($len)
	$i = intval ( ($len - $i) / 2 );
	return $i;
}

// 旺旺状态
function msg_path($nick, $type = 0) {
	$s = $type ? 2 : 1;
	$msg = '<a target="_blank" href="http://amos.im.alisoft.com/msg.aw?v=2&uid=' . $nick . '&site=cntaobao&s=1&charset=utf-8" ><img border="0" src="http://amos.im.alisoft.com/online.aw?v=2&uid=' . urlencode ( $nick ) . '&site=cntaobao&s=' . $s . '&charset=utf-8" alt="联系' . $nick . '" /></a>';
	return $msg;
}


// 简化PROPS
function split_prop($properties, $symbol = " ") {
	$properties = explode ( ";", $properties );
	$propertie = array ();
	foreach ( $properties as $v ) {
	$v = explode ( ":", $v );
	$propertie [] = $v [1];
	}
	return implode ( $symbol, $propertie );
}

/**
 * 字符转换成布尔值
 */
function str2bool($str) {
	$str = (string)$str;
	if (($str == '1') || ($str == 'true')) {
	return 1;
	} elseif (!$str || ($str == 'false')) {
	return 0;
	}
	return $str;
}


/**
 * 布尔值转换成字符
 */
function bool2str($bool) {
	$str = intval(trim($bool));
	if ($bool == 1) return 'true';
	return 'false';
}

/**
 * URl相关信息
 * 'scheme' => 'http',
 * 'host' => '127.0.0.1',
 * 'path' => '/index.php',
 * 'query' => 'm=taobao&c=stream&a=asyn',
 * 'port' => '80',
 * 'ip' => '127.0.0.1',
 * 'request' => '/index.php?m=taobao&c=stream&a=asyn',
 */
function urlinfo($url) {
	if (!url) return false;
	$url_arr  = parse_url($url);
	$url_arr["path"]    = empty($url_arr["path"]) ? "/"  : $url_arr["path"];
	$url_arr["port"]    = empty($url_arr["port"]) ? "80" : $url_arr["port"];
	$url_arr["ip"]      = gethostbyname($url_arr["host"]);
	$url_arr["request"] = $url_arr["path"]
	. (empty($url_arr["query"])    ? "" : "?" . $url_arr["query"])
	. (empty($url_arr["fragment"]) ? "" : "#" . $url_arr["fragment"]);
	
	return $url_arr;
}

//发包函数
function httpPost($url, $data, $timeout = '10') {
	$url = urlinfo($url); //获取URL信息
	$query = $url["request"];
	
	if(is_array($data)) { //数组转成拼接
		foreach($data as $k =>$v) {
			$data_arr .=(empty($data_arr) ? "" : "&").urlencode($k)."=".urlencode($v);
		}
	}
	
	$header = "POST ".$query." HTTP/1.1\r\n";
	$header .= "Host:".$url["host"]."\r\n";
	$header .= "Content-type: application/x-www-form-urlencoded\r\n";
	$header .= "Content-Length:".strlen($data_arr)."\r\n";
	$header .= "Connection: Close\r\n\r\n";
	if ($data_arr) $header.= "$data_arr\r\n\r\n";

	$fp = fsockopen($url["host"], $url["port"], $errno, $errstr, $timeout);
	if (!$fp) {
		Log::write("fsockopen打开失败：$errstr ($errno)", Log::ERR);
	}

	fwrite($fp,$header);
	
	fclose($fp);
	unset($header);

	return true;
}

//错误日志
function msg($str, $file='stream') {
	$ip = get_client_ip ();
	$time = date ('Y-m-d H-i-s');
	$content = $time.' | '.$ip.' | '.$str.' | '."\r\n";
	//echo $content;
	error_log($content, 3, LOG_PATH.$file.'_log.php');
}

/**
 * 消息提示
 * 源自DZ
 */
function showmessage($message, $url_forward = '', $values = array(), $extraparam = array(), $custom = 0) {
	load("@.dz_message");
	return dshowmessage($message, $url_forward, $values, $extraparam, $custom);
}

/*
 * 用处 ：此函数用来逆转javascript的escape函数编码后的字符。
* 关键的正则查找我不知道有没有问题.
* 参数：javascript编码过的字符串。
* 如：unicodeToUtf8("%u5927")= 大
* 2005-12-10
*
*/
function unescape($escstr){
	preg_match_all("/%u[0-9A-Za-z]{4}|%.{2}|[0-9a-zA-Z.+-_]+/",$escstr,$matches); //prt($matches);
	$ar = &$matches[0];
	$c = "";
	foreach($ar as $val){
	if (substr($val,0,1)!="%") { //如果是字母数字+-_.的ascii码
		$c .=$val;
	}
	elseif (substr($val,1,1)!="u") { //如果是非字母数字+-_.的ascii码
		$x = hexdec(substr($val,1,2));
		$c .=chr($x);
	}
	else { //如果是大于0xFF的码
		$val = intval(substr($val,2),16);
		if($val < 0x7F){        // 0000-007F
		$c .= chr($val);
		}elseif($val < 0x800) { // 0080-0800
		$c .= chr(0xC0 | ($val / 64));
		$c .= chr(0x80 | ($val % 64));
		}else{                // 0800-FFFF
		$c .= chr(0xE0 | (($val / 64) / 64));
		$c .= chr(0x80 | (($val / 64) % 64));
		$c .= chr(0x80 | ($val % 64));
		}
	}
	}
	return $c;
}

//javascript unescape解密函数
//备用
function unescape2($str) {
	$ret = '';
	$len = strlen($str);
	for ($i = 0; $i < $len; $i++) {
	if ($str[$i] == '%' && $str[$i+1] == 'u') {
		$val = hexdec(substr($str, $i+2, 4));
		if ($val < 0x7f) {
		$ret .= chr($val);
		} else {
		if($val < 0x800) {
			$ret .= chr(0xc0|($val>>6)).chr(0x80|($val&0x3f));
		} else {
			$ret .= chr(0xe0|($val>>12)).chr(0x80|(($val>>6)&0x3f)).chr(0x80|($val&0x3f));
		}
		}
		$i += 5;
	}else {
		if ($str[$i] == '%') {
		$ret .= urldecode(substr($str, $i, 3));
		$i += 2;
		} else {
		$ret .= $str[$i];
		}
	}
	}
	return $ret;
}

/** 
 * js escape php 实现 
 * @param $string           the sting want to be escaped 
 * @param $in_encoding       
 * @param $out_encoding      
 */ 
function escape($string, $in_encoding = 'UTF-8',$out_encoding = 'UCS-2') { 
	$return = ''; 
	if (function_exists('mb_get_info')) { 
	for($x = 0; $x < mb_strlen ( $string, $in_encoding ); $x ++) { 
		$str = mb_substr ( $string, $x, 1, $in_encoding ); 
		if (strlen ( $str ) > 1) { // 多字节字符 
		$return .= '%u' . strtoupper ( bin2hex ( mb_convert_encoding ( $str, $out_encoding, $in_encoding ) ) ); 
		} else { 
		$return .= '%' . strtoupper ( bin2hex ( $str ) ); 
		} 
	} 
	} 
	return $return; 
}

/**
 * AJAX消息返回
 * 格式化特定格式的返回消息
 */
function ajaxmsg($message, $status = 1, $type = 'json') {
	$info = array('IsSuccess'=>$status, 'Msg'=>$message);
	return $this->ajaxReturn($info, $type);
}

function format_date($timestamp, $showtime = 1) {
	$times = intval($timestamp);
	if (!$times) return true;
	$str = $showtime ? date('Y-m-d H:i:s', $times) : date('Y-m-d', $times);
	return $str;
}

/**
 * 设置config文件
 * @param $config 配属信息
 * @param $filename 要配置的文件名称
 */
function set_config($config, $filename = "config") {
	$configfile = CONF_PATH . $filename . '.php';
	if (!is_writable($configfile)) $this->error("文件不可写");
	$pattern = $replacement = array ();
	foreach ($config as $k => $v) {
		if (in_array($k, array(
				'SITE_NAME',
				'SITE_DOMAIN',
				'SITE_STATUS',
				'SITE_IPBAN',
				'IMG_PATH',
				'JS_PATH',
				'CSS_PATH',
				'UPLOAD_PATH',
				'UPLOAD_URL',
				'UPLOAD_FILE_RULE',
				'UPLOAD_MAX_SIZE',
				'UPLOAD_ALLOW_EXT',
				'AUTHCODE',
				'HTML_PATH',
		))) {
			$v = trim($v);
			$configs[$k] = $v;
			$pattern[$k] = "/'" . $k . "'\s*=>\s*([']?)[^']*([']?)(\s*),/is";
			$replacement[$k] = "'" . $k . "' => \${1}" . $v . "\${2}\${3},";
		}
	}
	$str = file_get_contents($configfile);
	$str = preg_replace($pattern, $replacement, $str);
	return file_put_contents($configfile, $str);
}

/**
 * 将字符串转换为数组
 *
 * @param string $data        	
 * @return array
 *
 */
function string2array($data) {
	if ($data == '') return array();
	eval("\$array = \"$data\";");
	return $array;
}

/**
 * 将数组转换为字符串
 *
 * @param array $data        	
 * @param bool $isformdata        	
 * @return string
 *
 */
function array2string($data, $isformdata = 1) {
	if ($data == '') return '';
	if ($isformdata) $data = new_stripslashes($data);
	return addslashes(var_export($data, TRUE));
}
?>