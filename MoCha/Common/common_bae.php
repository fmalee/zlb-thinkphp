<?php
/*
 * 项目扩展函数库_BAE专用
 */

/**
 * 远程图片下载
 * @param $value 传入下载内容
 * @param $code 转化格式
 * @param $watermark 是否加入水印
 */
function remote_pic($value, $code = '', $watermark = '') {
	if ($code) {
		require_once('BaeImage.class.php');
		$imageui = new BaeImage($value);
		require_once "BaeLog.class.php";
		$logger=BaeLog::getInstance();

		$ret = $imageui->setTranscoding($code, 40);
		if (!$ret) {
		    $err = 'set transcoding failed, error:' . $imageui->errmsg() . "\n";
		    $logger ->logFatal($err);
		}

		$arrRes = $imageui->process();
		if ($arrRes !== false && isset($arrRes['response_params']) && isset($arrRes['response_params']['image_data'])) {
		    $newfile = sys_get_temp_dir() . '/' . date('his') . rand(100, 999) . '.' . $code;
		    file_put_contents($newfile, base64_decode($arrRes['response_params']['image_data']));
		} else {
		    $err = 'process failed, error:' . $imageui->errmsg() . "\n";
		    $logger ->logFatal($err);
		}
	} else {
		import("ORG.Net.Http");
		$file = new_stripslashes($value);
		if (strpos($file, '://') === false) continue;
		$filename = date('his') . rand(100, 999) . '.' . fileext($file);
		$newfile = sys_get_temp_dir() . '/' . $filename;
		Http::curlDownload($file, $newfile);
	}
	
	return $newfile;
}
?>