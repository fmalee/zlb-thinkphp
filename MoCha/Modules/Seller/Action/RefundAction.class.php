<?php
/**
 * 订单评价
 */
class RefundAction extends TbbaseAction {
	
	/**
	 * 退款列表
	 */
	public function index() {
		$where = array();
		$where['seller_nick'] = $this->nick;
		$status = $this->_get('status', 'trim', '');
		$status && $where['status']= $status;
		$start_date = $this->_get('start_date', 'strtotime', '');
		$end_date = $this->_get('end_date', 'strtotime', '');
		if ($start_date) {
			if (!$end_date) $end_date = NOW_TIME;
			if ($start_date > $end_date) $this->error('结束日期需要大于开始日期');
			$where['created']= array(array('gt', $start_date), array('lt', $end_date));
		}
		$keyword = $this->_get('keyword', 'trim', '');
		$keyword && $where['Refund.tid|Refund.refund_id|Refund.sid'] = array('like', "%$keyword%");
		
		$Refund = D('RefundView');
		$count = $Refund->where($where)->count();
		$Page = new Page($count, 20);
		$page = $Page->show();
		
		$refunds = $Refund->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('created DESC,refund_id DESC')->select();
		
		$this->assign("status", $status);
		$this->assign("start_date", $_GET['start_date']);
		$this->assign("end_date", $_GET['end_date']);
		$this->assign("keyword", $keyword);
		$this->assign("refunds", $refunds);
		$this->assign("page", $page);
		$this->parent_name = "退款管理";
		$this->seo = "退款管理 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 退款请情页
	 */
	public function read() {
		$where = array();
		$where['seller_nick'] = $this->nick;
		$refund_id = $this->_get('refund_id', 'trim', '');
		!$refund_id && $this->error('参数错误，请重试');
		$where['refund_id'] = $refund_id;
		$refund = D('RefundView')->where($where)->find();
		$messages = D('Refund')->getMsg($refund_id);
		
		if ($_GET['ajax']) $tpl = "read_ajax"; //切换模版
		$this->assign("refund_id", $refund_id);
		$this->assign("refund", $refund);
		$this->assign("messages", $messages);
		trace($messages, 'messages');
		$this->seo = "退款详情".$refund_id. " - 退款管理 - 卖家中心";
		$this->display($tpl);
	}
	
	/**
	 * 上传留言
	 * TODO 上传留言
	 */
	public function message() {
		
	}
	
	/**
	 * 同步退款准备
	 */
	public function collect() {
		$cache['start'] = date('Y-m-d H:i:s',  NOW_TIME - $this->_get('date', 'trim', ''));
		$cache['end'] = date('Y-m-d H:i:s', NOW_TIME);
		$cache['pagesize'] = 100;
		//把采集信息写入缓存
		cookie('refund_syn', serialize($cache));
		$this->success('开始同步退款记录');
	}
	
	/**
	 * 同步退款开始
	 */
	public function syn() {
		$page = $this->_get('page', 'intval', 1);
		$cache = unserialize(cookie('refund_syn'));
		!$cache && $this->error('参数错误，请重试');
		$total = D('Refund')->synRefunds($cache['start'], $cache['end'], $page, $cache['pagesize']);  //更新退款信息
		
		if (!$cache['pagenum']) {
			$total_page = ceil($total / $cache['pagesize']); //计算总数
			$cache['pagenum'] = $total_page;
			$cache['total'] = $total;
			cookie('refund_syn', serialize($cache));
		}
		if ($page > $cache['pagenum']) {
			cookie('refund_syn', null); //删除缓存
			$this->error('同步完成，将刷新页面');
		}
		
		$this->assign('pagenum', $cache['pagenum']);
		$this->assign('total', $total);
		$this->assign('page', $page);
		$resp = $this->fetch('Public:syn');
		$this->ajaxReturn($resp, '同步退款记录', 1);
	}
	
	/**
	 * AJAX同步单笔退款
	 */
	public function asyn() {
		$refund_id = $this->_get('refund_id', 'trim', '');
		!$refund_id && $this->ajaxReturn(0);
		D('Refund')->synRefund($refund_id);
		$refund = D('RefundView')->where(array('refund_id' => $refund_id))->find();
		
		$this->assign("refund", $refund);
		$this->display();
	}
}
?>
