<?php
/**
 * 订单评价
 */
class CreditAction extends TbbaseAction {
	
	/**
	 * 同步用户
	 */
	public function index() {
		$where = array();
		$where['nick'] = $this->nick;
		$status = $this->_get('status', 'trim', '');
		$status && $where['status']= $status;
		$type = $this->_get('type', 'trim', '');
		$type && $where['type']= $type;
		$start_date = $this->_get('start_date', 'strtotime', '');
		$end_date = $this->_get('end_date', 'strtotime', '');
		if ($start_date) {
			if (!$end_date) $end_date = NOW_TIME;
			if ($start_date > $end_date) $this->error('结束日期需要大于开始日期');
			$where['created']= array(array('gt', $start_date), array('lt', $end_date));
		}
		$keyword = $this->_get('keyword', 'trim', '');
		$keyword && $where['Credit.tid|Credit.contact|Credit.user|Credit.buer_nick'] = array('like', "%$keyword%");
		
		$Credit = D('CreditView');
		$count = $Credit->where($where)->count();
		$Page = new Page($count, 20);
		$page = $Page->show();
		
		$credits = $Credit->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('created DESC,tid DESC')->select();
		
		$this->assign("status", $status);
		$this->assign("start_date", $_GET['start_date']);
		$this->assign("end_date", $_GET['end_date']);
		$this->assign("keyword", $keyword);
		$this->assign("credits", $credits);
		$this->assign("page", $page);
		$this->seo = "刷单管理 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 更多订单信息
	 * AJAX返回
	 */
	public function trade() {
		$tid = $this->_request('tid', 'trim', '');
		!$tid && $this->error("请选择要操作的订单");
		
		$trade = D('CreditView')->where(array('tid'=>$tid))->find();
		$this->assign('trade', $trade);
		$this->display();
	}
	
	/**
	 * 添加刷单记录
	 */
	public function add() {
		if (IS_POST) {
			$Credit = D('Credit');
			if ($Credit->create()) {
				$_POST['nick'] = $this->nick;
				$Credit->add($_POST);
				D('Trade')->where(array('tid'=>$_POST['tid']))->save(array('credit'=>1));

				$this->success('成功建立刷单记录！');
			} else {
				$this->error($Credit->getError());
			}
		}
		$tid = $this->_get('tid', 'trim', '');
		!$tid && $this->error("错误的参数！");
		
		$info = M('Trade')->where(array('tid'=>$tid, 'seller_nick'=>$this->nick))->field('tid,created,buyer_nick,receiver_name')->find();
		$info['contact'] = $info['receiver_name'];
		unset($info['receiver_name']);
		$this->assign("info", $info);
		$this->display();
	}
	
	/**
	 * 编辑刷单记录
	 * @param intval $id
	 */
	public function edit() {
		if (IS_POST) {
			$Credit = D('Credit');
			if ($Credit->create()) {
				$Credit->save();
				$this->success('成功更新刷单记录！');
			} else {
				$this->error($Credit->getError());
			}
		}
		$tid = $this->_get('tid', 'trim', '');
		!$tid && $this->error("错误的参数！");
	
		$info = M('Credit')->where(array('tid'=>$tid, 'nick'=>$this->nick))->find();
		$info['tid'] = $tid;
		$this->assign("info", $info);
		$this->display('add');
	}
	
	/**
	 * 删除记录
	 * @param intval $delid
	 */
	public function del() {
		$delid = $this->_request('delid', 'trim', '');
		!$delid && $this->error("请选择要删除的记录");
	
		$ids = explode(',', trim($delid, ','));
		foreach ($ids as $tid) {
			M('Credit')->where(array('tid'=>$tid))->delete(); //删除记录
			D('Trade')->where(array('tid'=>$tid))->save(array('credit'=>0));
		}
		$this->success('记录删除成功！');
	}
}
?>
