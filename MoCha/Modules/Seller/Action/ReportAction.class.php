<?php
/**
 * 订单评价
 */
class ReportAction extends TbbaseAction {
	
	/**
	 * 同步用户
	 */
	public function syn_users() {
		$nicks = array('英摩卡','coromachi','没有远方','克洛达服饰','学就哥','a285209225');
		foreach ($nicks as $nick) {
			$user = $this->db->syn_user($nick);
		}
	}
	
	/**
	 * 30天零销量宝贝
	 */
	public function zero() {
		$Item = M('Item');
		$Serial = M('Serial');
		
		$where['nick'] = $this->nick;
		$where['approve_status'] = 'onsale';
		$where['volume'] = 0;
		$count = $Item->where($where)->count();
		$Page = new Page($count, '50');
		$page = $Page->show();
		
		$items = $Item->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('created DESC,num_iid DESC')->select();
		foreach ($items as $k => $v) {
			$cost = $Serial->getFieldByNum_iid($v['num_iid'], 'price');
			$items[$k]['cost'] = $cost;
		}
		
		$this->assign("infos", $items);
		$this->assign("page", $page);
		$this->parent_name = '货源管理';
		$this->seo = '货源管理 - 卖家中心';
		$this->display();
	}
}
?>
