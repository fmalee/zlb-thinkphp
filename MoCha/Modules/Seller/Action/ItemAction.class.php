<?php
/**
 * 商品操作类
 */
class ItemAction extends TbbaseAction {
	
	/**
	 * 订单列表
	 * 包含自动更新
	 */
	public function index() {
		$Item = M('Item');
		$Serial = M('Serial');
		$Shop = M('Shop');
		$where = array();
		$where['nick'] = $this->nick;
		$where['approve_status'] = 'onsale';
		
		$pagesize = $this->_get('pz', 'intval', '60'); //设置页码
		$outerid = $this->_get('outerid', 'trim', ''); //商家编码
		$outerid && $where['outerid'] = $outerid;
		
		$keyword = $this->_get('keyword', 'trim', '');  //标题
		if ($keyword) {
			$this->assign("keyword", $keyword);
			$keywords = explode(' ', $keyword);  //关键字分词
			if (count($keywords) > 1) {
				foreach ($keywords as $keyword) {
					$likes[] = "%$keyword%";
				}
				$where['title'] = array('like', $likes, 'AND');
			} else {
				$where['title'] = array('like', "%$keywords[0]%");
			}
		}
		$location = $this->_get('location', 'trim', ''); //位置
		if ($location) {
			$nicks = $Shop->where(array('location'=>$location))->field('nick')->select();
			$nicks && $where['nick'] = array('in', implode(',', $nicks));
		}
		$cid = $this->_get('cid', 'intval', ''); //类目
		if ($cid) {
			$arrchild_cid = M('Cats')->where(array('cid'=>$cid))->getFieldByCid($cid, 'arrchild_cid');
			if ($cid == $arrchild_cid) {
				$where['cid'] = $cid;
			} else {
				$where['cid'] = array('in', $arrchild_cid);
			}
		}
		
		$start_price = $this->_get('start_price', 'intval', '');
		$end_price = $this->_get('end_price', 'intval', '');
		if ($start_price) {
			if (!$end_price) $this->error('请输入最高价格');
			if ($start_price > $end_price) $this->error('起始金额不能大于结束金额');
			$where['price']= array(array('gt', $start_price), array('lt', $end_price));
		}
		
		$count = $Item->where($where)->count();
		$Page = new Page($count, $pagesize);
		$page = $Page->show();
		
		$infos = $Item->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('modified DESC,num_iid DESC')->select();
		foreach($infos as $k => $info) {
			$r = $Serial->where(array('nick' => $this->nick, 'num_iid'=>$info['num_iid']))->field('iid,price,model,barcode,stock')->find();
			$infos[$k]['serial'] = $r;
		}
		
		$this->assign("infos", $infos);
		$this->assign("page", $page);
		$this->assign("pz", $pagesize);
		$this->parent_name = '宝贝管理';
		$this->seo = '出售中的宝贝 - 卖家中心';
		$this->display('piclist');
	}
	
	/**
	 * 仓库中的宝贝
	 */
	public function instock() {
		$Item = M('Item');
		$Serial = M('Serial');
		$Shop = M('Shop');
		$where = array();
		$where['nick'] = $this->nick;
		$where['approve_status'] = 'instock';
		
		$pagesize = $this->_get('pz', 'intval', '60'); //设置页码
		$outerid = $this->_get('outerid', 'trim', ''); //商家编码
		$outerid && $where['outerid'] = $outerid;
		
		$keywords = $this->_get('keyword', 'trim', '');  //标题
		if ($keywords) {
			$keywords = explode(' ', $keywords);  //关键字分词
			if (is_array($keywords)) {
				foreach ($keywords as $keword) {
					$likes[] = "%$keywords%";
				}
				$where['title'] = array('like', $likes, 'AND');
			} else {
				$where['title'] = "%$keywords%";
			}
		}
		$location = $this->_get('location', 'trim', ''); //位置
		if ($location) {
			$nicks = $Shop->where(array('location'=>$location))->field('nick')->select();
			$nicks && $where['nick'] = array('in', implode(',', $nicks));
		}
		$cid = $this->_get('cid', 'intval', ''); //类目
		if ($cid) {
			$arrchild_cid = M('Cats')->where(array('cid'=>$cid))->getFieldByCid($cid, 'arrchild_cid');
			if ($cid == $arrchild_cid) {
				$where['cid'] = $cid;
			} else {
				$where['cid'] = array('in', $arrchild_cid);
			}
		}
		
		$start_price = $this->_get('start_price', 'intval', '');
		$end_price = $this->_get('end_price', 'intval', '');
		if ($start_price) {
			if (!$end_price) $this->error('请输入最高价格');
			if ($start_price > $end_price) $this->error('起始金额不能大于结束金额');
			$where['price']= array(array('gt', $start_price), array('lt', $end_price));
		}
		
		$count = $Item->where($where)->count();
		$Page = new Page($count, $pagesize);
		$page = $Page->show();
		
		$infos = $Item->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('modified DESC,num_iid DESC')->select();
		foreach($infos as $k => $info) {
			$r = $Serial->where(array('nick' => $this->nick, 'num_iid'=>$info['num_iid']))->field('iid,price,model,barcode,stock')->find();
			$infos[$k]['serial'] = $r;
		}
		
		$this->assign("infos", $infos);
		$this->assign("page", $page);
		$this->assign("pz", $pagesize);
		$this->parent_name = '宝贝管理';
		$this->seo = '仓库中的宝贝 - 卖家中心';
		$this->display('piclist');
	}
	
	public function read() {
		$id = $_GET ['id'];
		$id = $this->_get('id', 'trime', '');
		!$id && $this->error('参数错误');
		$fields = "cid,created,delist_time,ems_fee,express_fee,freight_payer,input_pids,input_str,modified,outer_id,pic_url,price,property_alias,props,seller_cids,title,item_img,prop_img,desc";
		$item = D('Item')->synItem($id, 0, $fields); // 更新宝贝信息
		$item['item_imgs'] = $item['item_imgs']['item_img'];
		
		/* $item_imgs = $prop_imgs = array ();
		foreach ( $item ['item_imgs']->item_img as $v ) {
			$item_imgs [] = ( array ) $v;
		}
		foreach ( $item ['prop_imgs']->prop_img as $v ) {
			$prop_imgs [] = ( array ) $v;
		}
		$item ['item_imgs'] = $item_imgs;
		$item ['prop_imgs'] = $prop_imgs; */
		
		$sale_props = D('Props')->get_props($item, 1);
		$normal_props = D('Props')->get_props($item);
		
		$serial = M('Serial')->where(array('num_iid' => $id))->field('iid, price, barcode, stock')->find();
		$serial && $item = array_merge($item, $serial);
		$nick = M('Goods')->getFieldById($item['iid'], 'nick');
		$shop = M('Shop')->where(array('nick' => $nick))->field('pic_path,title, sid, phone, location, shop_score, business, qq')->find();
		$shop['url'] = "http://shop".$shop['sid'] . ".taobao.com";
		
		$this->assign("sale_props", $sale_props);
		$this->assign("normal_props", $normal_props);
		$this->assign("item", $item);
		$this->assign("shop", $shop);
		$this->parent_name = '宝贝管理';
		$this->seo = $item['title'].'宝贝管理 - 卖家中心';
		$this->display('Goods:read');
	}
	
	/**
	 * 更新商家编码
	 */
	public function join() {
		$num_iid = $this->_get('num_iid', 'trim', '');
		!$num_iid && $this->error("请选择要更新的宝贝");
		$Item = D('Item');
		
		$item = $Item->where(array('num_iid'=>$num_iid, 'nick'=>$this->nick))->Field('title,outer_id,input_pids,input_str')->find();
		!$item && $this->error("请选择要更新的宝贝");
		
		if (IS_POST) {
			$info = $_POST['info'];
			$org = $_POST['org'];
			$serials = $skus = array();
			
			if (($info['title'] != $item['title']) || ($info['outer_id'] != $item['outer_id'])) {
				if ($info['title'] != $item['title']) $title = $info['title'];
				if ($info['outer_id'] != $item['outer_id']) $serial = $info['outer_id'];
				$serials[$serial] = 1;
				if ($this->memberinfo['type'] == "C") {
					$input_pids = $item['input_pids'];
					if (strstr($input_pids, ',')) {
						$input_strs = explode(',', $item ['input_str']);
						$input_strs[1] = $serial;
						$input_str = implode(',', $input_strs);
					} else {
						$input_str = $serial;
					}
				}
				$update = array();
				$update['title'] = $title;
				$update['outer_id'] = $serial;
				$update['input_pids'] = $input_pids;
				$update['input_str'] = $input_str;
				$result = $Item->updateItem($num_iid, $update);
				
				!$result && $this->error('更新标题、商家编码失败');
			}
			
			$sku = array();
			foreach($info['skus'] as $properties => $serial) {  //更新商品SKU信息
				if ($serial != $org[$properties]) {
					$sku['outer_id'] = $serial;
					$Item->updateSku($num_iid, $properties, $sku);
				}
				$serials[$serial] = 1;
			}
			
			$Serial = D('Serial');
			$Alias = M('Serial_alias');
			foreach ($serials as $serial=>$v) {  //更新本地编码库
				$iid = $Alias->where(array('serial'=>$serial, 'nick'=>$this->nick))->Field('iid')->find();
				$iid && $Serial->set_major($iid['iid'], $num_iid);
			}
			
			$this->success('编码更新成功');
		}
		
		$skus = M('Sku')->where(array('num_iid'=>$num_iid))->Field('outer_id, properties, properties_name')->select();
		$item['lenth'] = str_length($item['title'], '60');
		
		$this->assign("item", $item);
		$this->assign("skus", $skus);
		$this->display();
	}
	
	/**
	 * 同步所有宝贝
	 * 在售 下架 违规
	 */
	public function syn2($type) {
		if (IS_POST) {
			$time = $this->_post('time', 'trim', '');
			if ($time) {
				$cache['start'] = date('Y-m-d H:i:s',  NOW_TIME) - $time;
				$cache['end'] = date('Y-m-d H:i:s', NOW_TIME);
			}
			$cache['total'] = 0;
			$cache['pagenum'] = 0;
			$cache['page'] = 1;
			$cache['type'] = 0;
			$cache['pagesize'] = 49;
			//把采集信息写入缓存
			cookie('item_syn', serialize($cache));
			$this->success('开始同步订单记录');
		}
		$cache = unserialize(cookie('item_syn'));
		!$cache && $this->error('参数错误，请重试');

		$Item = D('Item');
		$type = $cache['type'];
		$Taoapi = new Taoapi();
		$Taoapi->session = SESSIONKEY;

		$where['page_no'] = $cache['page'];
		$where['page_size'] = $cache['pagesize'];
		
		$banners = array('onsale', 'for_shelved', 'violation_off_shelf');
		$banners_str = array('出售中的宝贝', '等待上架的宝贝', '违规的宝贝');
		$map = array('nick'=>$this->nick);
		if ($type == 0) {
			$map['approve_status'] = 'onsale';
		} else {
			$where['banner'] = $banners[$type];
			$map['approve_status'] = 'instock';
		}
		
		if ($page == 1 && $type < 2) {
			if ($type == 0) {
				$cache['total'] = $Item->synOnsale($where, 'num_iid');
			} elseif ($type < 2) {
				$cache['total'] = $Item->syninstock($where, 'num_iid');
			}
			$cache['pagenum'] = ceil($cache['total'] / $cache['pagesize']); //计算总数
			cookie('item_syn', serialize(array($Taoapi->method=>$total)));

			$Item->where($map)->save(array('approve_status'=>'recycle')); //改变宝贝状态
		}

		$Taoapi->where = $where;
		$Taoapi->fields = 'num_iid';
		$Taoapi->select();

		$params = array();
		$params['ql'] = "select num_iid from $method where $where";
		$items = $Top->tql($params, SESSIONKEY);
		$total = $items['total_results'];
		$items = $items['items']['item'];
		$total_page = ceil($total / $pageSize);
		if ($total > 0) {
			$count = count($items);
			$num = ceil($count / 20);

			for ($i = 0; $i < $num; $i++) {
				$iids = array_slice($items, $i*20 , 20);
				$num_iids = array();
				foreach ($iids as $iid) {
					$num_iids[] = $iid['num_iid'];
				}
				
				$Item->synItems(implode(',', $num_iids));  //更新宝贝数据
			}
				
			if ($total_page > $page) {
				$data['info'] = "正同步$banners_str[$type]";
				$data['status'] = 1;
				$this->ajaxReturn($data);
			}
		}
		if ($type > 2) $this->error('同步完成，将刷新页面');
		
		$data['info'] = "同步$banners_str[$type]完成";
		$data['status'] = 2;
		$this->ajaxReturn($data);
	}

	/**
	 * 同步所有宝贝
	 * 在售 下架 违规
	 */
	public function syn($type) {
		$page = $this->_get('page', 'intval', 1);
		$type = $this->_get('type', 'trim', 0);
		$Item = D('Item');
		$Top = new Top();
		$pageSize = 80;
		$where = " page_no=$page and page_size=$pageSize";
		
		$banners = array('onsale', 'for_shelved', 'violation_off_shelf');
		$banners_str = array('出售中的宝贝', '等待上架的宝贝', '违规的宝贝');
		$map = array('nick'=>$this->nick);
		if ($type == 0) {
			$method = 'items.onsale';
			$map['approve_status'] = 'onsale';
		} else {
			$method = 'items.inventory';
			$where .= " and banner=$banners[$type]";
			$map['approve_status'] = 'instock';
		}
		
		if ($page == 1 && $type < 2) $Item->where($map)->save(array('approve_status'=>'recycle')); //改变宝贝状态
		
		$params = array();
		$params['ql'] = "select num_iid from $method where $where";
		$items = $Top->tql($params, SESSIONKEY);
		$total = $items['total_results'];
		$items = $items['items']['item'];
		$total_page = ceil($total / $pageSize);
		if ($total > 0) {
			$count = count($items);
			$num = ceil($count / 20);
				
			for ($i = 0; $i < $num; $i++) {
				$iids = array_slice($items, $i*20 , 20);
				$num_iids = array();
				foreach ($iids as $iid) {
					$num_iids[] = $iid['num_iid'];
				}
				
				$Item->synItems(implode(',', $num_iids));  //更新宝贝数据
			}
				
			if ($total_page > $page) {
				$data['info'] = "正同步$banners_str[$type]";
				$data['status'] = 1;
				$this->ajaxReturn($data);
			}
		}
		if ($type > 2) $this->error('同步完成，将刷新页面');
		
		$data['info'] = "同步$banners_str[$type]完成";
		$data['status'] = 2;
		$this->ajaxReturn($data);
	}
	
	/**
	 * 同步单笔交易
	 * AJAX
	 */
	public function asyn() {
		$iid = $this->_get('iid', 'trim', '');
		!$iid && $this->error('参数错误');
		
		$item = D('Item')->synItem($iid);
		$item = D('Item')->format($item);
		
		$this->assign("info", $item);
		$this->display('item');
	}
	
	/**
	 * 获取BRACODE
	 */
	public function barcode($num_iid) {
		return M('Serial')->where(array('num_iid'=>$num_iid, 'nick'=>$this->nick))->getField('barcode');
	}
	
	/**
	 * 删除在线宝贝
	 */
	public function delete() {
		$num_iid = $this->_post('num_iid', 'trim', '');
		!$num_iid && $this->error("请选择要删除的宝贝");
	
		$ids = explode(',', trim($num_iid, ','));
		$err = '';
		$Item = D('Item');
		foreach ($ids as $id) {
			$result = D('Item')->deleteItem($id);
		}
		$this->success('宝贝已经放入回收站');
	}
	
	/**
	 * 删除回收站宝贝
	 */
	public function crash() {
		$num_iid = $this->_post('num_iid', 'trim', '');
		!$num_iid && $this->error("请选择要删除的宝贝");
		
		$ids = explode(',', trim($num_iid, ','));
		$err = '';
		$Item = M('Item');
		$Sku = M('Sku');
		foreach ($ids as $id) {
			$item = $Item->where(array('nick'=>$this->nick, 'num_iid'=>$id))->delete(); //删除宝贝
			if ($item) $Sku->where(array('num_iid'=>$id))->delete(); //删除宝贝SKU
		}
		$this->success('删除宝贝成功');
	}
	
	/**
	 * 商品上下架
	 * 读取数据库状态判断上下架
	 */
	public function listing() {
		$num_iid = $this->_post('num_iid', 'trim', '');
		!$num_iid && $this->error("请选择要操作的宝贝");
		
		$ids = explode(',', trim($num_iid, ','));
		$Item = D('Item');
		$Shop = D('Shop');
		foreach ($ids as $id) {
			$item = $Item->where(array('num_iid'=>$id, 'nick'=>$this->nick))->Field('approve_status,num')->find();
			if (!$item) $this->error('宝贝不存在，请重试');
			
			if ($item['approve_status'] == 'onsale') {
				$Item->delistingItem($id);
			} else {
				$Item->listingItem($id, $item['num']);
			}
		}
		$this->success('宝贝操作成功');
	}
	
	/**
	 * 商品橱窗推荐
	 * 读取数据库状态判断橱窗状态
	 */
	public function recommend() {
		$num_iid = $this->_post('num_iid', 'trim', '');
		!$num_iid && $this->error("请选择要推荐的宝贝");
		
		$ids = explode(',', trim($num_iid, ','));
		$err = '';
		$Item = D('Item');
		$Shop = D('Shop');
		$info = '推荐';
		foreach ($ids as $id) {
			$has_showcase = $Item->where(array('num_iid'=>$id, 'nick'=>$this->nick))->getField('has_showcase');
			if ($has_showcase == NULL) $this->error('宝贝不存在，请重试');
			
			if (!$has_showcase) {
				$showcase = $Shop->get_showcase();
				if (!$showcase['remain_count']) $this->error('已经没有剩余橱窗');
				
				$Item->recommend($id);
			} else {
				$Item->recommend($id, 'delete');
				$info = '未推荐';
			}
		}
		$data['info'] = "宝贝操作成功";
		$data['data'] = $info;
		$data['status'] = 1;
		$this->ajaxReturn($data);;
	}
}
?>
