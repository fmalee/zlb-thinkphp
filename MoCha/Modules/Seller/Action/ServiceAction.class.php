<?php
/**
 * 售后服务
 */
class ServiceAction extends TbbaseAction {
	
	/**
	 * 售后服务
	 */
	public function index() {
		$this->parent_name = '售后管理';
		$this->seo = '换货记录 - 卖家中心';
		$this->display();
	}
	/**
	 * 换货记录
	 */
	public function niffer() {
		if ($_GET['ajax']) {
			$Niffer = M('Service_niffer');
			$where = array();
			$where['nick'] = $this->nick;
			$page = $this->_post('page', 'intval', 1); //设置页数
			$pagesize = $this->_post('rows', 'intval', 40); //设置页码
			$status = $this->_post('status', 'trim', ''); //记录状态
			$status && $where['status'] = $status;
			$tid = $this->_post('tid', 'trim', ''); //订单编号
			$tid && $where['tid'] = array('like', "%$tid%");
			$sid = $this->_post('sid', 'trim', ''); //物流编号
			$sid && $where['sid'] = array('like', "%$sid%");
			$begindate = $this->_post('begindate', 'trim', ''); //记录时间
			$enddate = $this->_post('enddate', 'trim', '');
			if ($enddate) {
				if (!$enddate) $this->error('请输入最晚时间');
				if ($begindate > $enddate) $this->error('起始时间不能大于结束时间');
				$where['created']= array(array('gt', strtotime($begindate)), array('lt', strtotime($enddate)));
			}
			$keywords = $this->_post('keyword', 'trim', '');  //关键字
			if ($keywords) {
				$where['tid|outer_iid|receiver_sid|send_sid'] = array('like', "%$keywords%");
			}
			$sort = $this->_post('sort', 'trim', '');  //排序字段
			$desc = $this->_post('order', 'trim', '');  //排序方式
			if($sort && $desc) {
				$order[$sort] = $desc;
			} else {
				$order['created'] = 'DESC';
			}
			$total  = $Niffer->where($where)->count(); //计算总数
			$limt = ($page-1)*$pagesize. ',' . $pagesize;  //当前记录
	
			$infos = $Niffer->where($where)->limit($limt)->order($order)->select();
			foreach($infos as $k => $info) {
				$trade = M('Trade')->where(array('tid'=>$info['tid']))->field('created as trade_created,pay_time,buyer_nick,receiver_name')->find();
				$trade['trade_created'] = format_date($trade['trade_created']);
				$order = M('Order')->where(array('tid'=>$info['tid']))->field('outer_iid,sku_properties_name,num')->select();
				$infos[$k] = array_merge($info,$trade);
				$infos[$k]['order'] = $order;
				$infos[$k]['created'] = format_date($info['created']);
				$infos[$k]['invoice'] = $info['invoice'] ? format_date($info['invoice']) : '';
				$infos[$k]['modified'] = format_date($info['modified']);
			}
			$data = array('total'=>$total, 'rows'=>$infos ? $infos : array()); //无记录输出空数组
			$this->ajaxReturn($data);
		}
		$this->parent_name = '售后管理';
		$this->seo = '换货记录 - 卖家中心';
		$this->display();
	}
	
	/**
	 * 退货记录
	 */
	public function back() {
		if ($_GET['ajax']) {
			$where = array();
			$where['nick'] = $this->nick;
			$page = $this->_post('page', 'intval', 1); //设置页数
			$pagesize = $this->_post('rows', 'intval', 40); //设置页码
			$status = $this->_post('status', 'trim', ''); //记录状态
			$status && $where['status'] = $status;
			$tid = $this->_post('tid', 'trim', ''); //订单编号
			$tid && $where['tid'] = array('like', "%$tid%");
			$sid = $this->_post('sid', 'trim', ''); //物流编号
			$sid && $where['sid'] = array('like', "%$sid%");
			$begindate = $this->_post('begindate', 'trim', ''); //记录时间
			$enddate = $this->_post('enddate', 'trim', '');
			if ($enddate) {
				if (!$enddate) $this->error('请输入最晚时间');
				if ($begindate > $enddate) $this->error('起始时间不能大于结束时间');
				$where['created']= array(array('gt', strtotime($begindate)), array('lt', strtotime($enddate)));
			}
			$keywords = $this->_post('keyword', 'trim', '');  //关键字
			if ($keywords) {
				$where['Back.tid|Back.sid|Back.memo'] = array('like', "%$keywords%");
			}
			$sort = $this->_post('sort', 'trim', '');  //排序字段
			$desc = $this->_post('order', 'trim', '');  //排序方式
			if($sort && $desc) {
				$order[$sort] = $desc;
			} else {
				$order['created'] = 'DESC';
			}
			$total  = M('Service_back')->where($where)->count(); //计算总数
			$limt = ($page-1)*$pagesize. ',' . $pagesize;  //当前记录
			
			$infos = D('Service_backView')->where($where)->limit($limt)->order($order)->select();
			foreach($infos as $k => $info) {
				$infos[$k]['created'] = format_date($info['created']);
			}
			
			$data = array('total'=>$total, 'rows'=>$infos ? $infos : array()); //无记录输出空数组
			$this->ajaxReturn($data);
		}
		$this->parent_name = '售后管理';
		$this->seo = '退货记录 - 卖家中心';
		$this->display();
	}
	
	/**
	 * 补发记录
	 */
	public function reissue() {
		if ($_GET['ajax']) {
			$where = array();
			$where['nick'] = $this->nick;
			$page = $this->_post('page', 'intval', 1); //设置页数
			$pagesize = $this->_post('rows', 'intval', 40); //设置页码
			$status = $this->_post('status', 'trim', ''); //记录状态
			$status && $where['status'] = $status;
			$tid = $this->_post('tid', 'trim', ''); //订单编号
			$tid && $where['tid'] = array('like', "%$tid%");
			$sid = $this->_post('sid', 'trim', ''); //物流编号
			$sid && $where['sid'] = array('like', "%$sid%");
			$begindate = $this->_post('begindate', 'trim', ''); //记录时间
			$enddate = $this->_post('enddate', 'trim', '');
			if ($enddate) {
				if (!$enddate) $this->error('请输入最晚时间');
				if ($begindate > $enddate) $this->error('起始时间不能大于结束时间');
				$where['created']= array(array('gt', strtotime($begindate)), array('lt', strtotime($enddate)));
			}
			$keywords = $this->_post('keyword', 'trim', '');  //关键字
			if ($keywords) {
				$where['Reissue.tid|Reissue.sid|Reissue.memo'] = array('like', "%$keywords%");
			}
			$sort = $this->_post('sort', 'trim', '');  //排序字段
			$desc = $this->_post('order', 'trim', '');  //排序方式
			if($sort && $desc) {
				$order[$sort] = $desc;
			} else {
				$order['created'] = 'DESC';
			}
			$total  = M('Service_reissue')->where($where)->count(); //计算总数
			$limt = ($page-1)*$pagesize. ',' . $pagesize;  //当前记录
				
			$infos = D('Service_reissueView')->where($where)->limit($limt)->order($order)->select();
			foreach($infos as $k => $info) {
				$infos[$k]['created'] = format_date($info['created']);
				$infos[$k]['invoice'] = $info['invoice'] ? format_date($info['invoice']) : '';
			}
				
			$data = array('total'=>$total, 'rows'=>$infos ? $infos : array()); //无记录输出空数组
			$this->ajaxReturn($data);
		}
		$this->parent_name = '售后管理';
		$this->seo = '补发记录 - 卖家中心';
		$this->display();
	}
	
	/**
	 * 沟通记录
	 */
	public function talk() {
		if ($_GET['ajax']) {
			$Talk = M('Service_talk');
			$where = $order = $map = array();
			$where['nick'] = $this->nick;
			$page = $this->_post('page', 'intval', 1); //设置页数
			$pagesize = $this->_post('rows', 'intval', 40); //设置页码
			$status = $this->_post('status', 'trim', ''); //记录状态
			$status && $where['status'] = $status;
			$tid = $this->_post('tid', 'trim', ''); //订单编号
			$tid && $where['tid'] = array('like', "%$tid%");
			$memo= $this->_post('memo', 'trim', ''); //记录备注
			$memo && $where['memo'] = array('like', "%$memo%");
			$begindate = $this->_post('begindate', 'trim', ''); //记录时间
			$enddate = $this->_post('enddate', 'trim', '');
			if ($enddate) {
				if (!$enddate) $this->error('请输入最晚时间');
				if ($begindate > $enddate) $this->error('起始时间不能大于结束时间');
				$where['created']= array(array('gt', strtotime($begindate)), array('lt', strtotime($enddate)));
			}
			$keywords = $this->_post('keyword', 'trim', '');  //关键字
			if ($keywords) {
				$where['tid|memo'] = array('like', "%$keywords%");
			}
			$sort = $this->_post('sort', 'trim', '');  //排序字段
			$desc = $this->_post('order', 'trim', '');  //排序方式
			if($sort && $desc) {
				$order[$sort] = $desc;
			} else {
				$order['created'] = 'DESC';
			}
			$total  = $Talk->where($where)->count(); //计算总数
			$limt = ($page-1)*$pagesize. ',' . $pagesize;  //当前记录
				
			$infos = $Talk->where($where)->limit($limt)->order($order)->select();
			foreach($infos as $k => $info) {
				$trade = M('Trade')->where(array('tid'=>$info['tid']))->field('created as trade_created,pay_time,buyer_nick,receiver_name')->find();
				$trade['trade_created'] = format_date($trade['trade_created']);
				$order = M('Order')->where(array('tid'=>$info['tid']))->field('outer_iid,sku_properties_name,num')->select();
				$infos[$k] = array_merge($info,$trade);
				$infos[$k]['order'] = $order;
				$infos[$k]['created'] = format_date($info['created']);
				$infos[$k]['modified'] = format_date($info['modified']);
			}
			$data = array('total'=>$total, 'rows'=>$infos ? $infos : array()); //无记录输出空数组
			$this->ajaxReturn($data);
		}
		$this->parent_name = '售后管理';
		$this->seo = '沟通记录 - 卖家中心';
		$this->display();
	}
		
	/**
	 * 差价记录
	 */
	public function fund() {
		if ($_GET['ajax']) {
			$Fund = M('Service_fund');
			$where = $order = $map = array();
			$where['nick'] = $this->nick;
			$page = $this->_post('page', 'intval', 1); //设置页数
			$pagesize = $this->_post('rows', 'intval', 40); //设置页码
			$status = $this->_post('status', 'trim', ''); //记录状态
			$status && $where['status'] = $status;
			$tid = $this->_post('tid', 'trim', ''); //订单编号
			$tid && $where['tid'] = array('like', "%$tid%");
			$memo= $this->_post('memo', 'trim', ''); //记录备注
			$memo && $where['memo'] = array('like', "%$memo%");
			$begindate = $this->_post('begindate', 'trim', ''); //记录时间
			$enddate = $this->_post('enddate', 'trim', '');
			if ($enddate) {
				if (!$enddate) $this->error('请输入最晚时间');
				if ($begindate > $enddate) $this->error('起始时间不能大于结束时间');
				$where['created']= array(array('gt', strtotime($begindate)), array('lt', strtotime($enddate)));
			}
			$keywords = $this->_post('keyword', 'trim', '');  //关键字
			if ($keywords) {
				$where['tid|memo'] = array('like', "%$keywords%");
			}
			$sort = $this->_post('sort', 'trim', '');  //排序字段
			$desc = $this->_post('order', 'trim', '');  //排序方式
			if($sort && $desc) {
				$order[$sort] = $desc;
			} else {
				$order['created'] = 'DESC';
			}
			$total  = $Fund->where($where)->count(); //计算总数
			$limt = ($page-1)*$pagesize. ',' . $pagesize;  //当前记录
	
			$infos = $Fund->where($where)->limit($limt)->order($order)->select();
			foreach($infos as $k => $info) {
				$trade = M('Trade')->where(array('tid'=>$info['tid']))->field('created as trade_created,pay_time,buyer_nick,receiver_name')->find();
				$trade['trade_created'] = format_date($trade['trade_created']);
				$infos[$k] = array_merge($info,$trade);
				$infos[$k]['order'] = $order;
				$infos[$k]['created'] = format_date($info['created']);
				$infos[$k]['modified'] = format_date($info['modified']);
				$infos[$k]['type'] = $info['type'] ? '收入' : '支出';
			}
			$data = array('total'=>$total, 'rows'=>$infos ? $infos : array()); //无记录输出空数组
			$this->ajaxReturn($data);
		}
		$this->parent_name = '售后管理';
		$this->seo = '差价记录 - 卖家中心';
		$this->display();
	}
	
	/**
	 * 售后列表
	 */
	public function search() {
		$tid = $this->_get('tid', 'trim', ''); //订单编号
		!$tid && $this->error('参数错误');
		$oid = $this->_get('oid', 'trim', ''); //子订单编号
		$where = array('nick'=>$this->nick);
		$where['tid'] = $tid;
		$niffer = M('Service_niffer')->where($where)->field('id,oid,reason,status,outer_iid,properties_name,num,created,invoice')->select(); //换货记录
		$back = M('Service_back')->where($where)->field('id,oid,status,reason,company,sid,created')->select(); //退货记录
		$reissue = M('Service_reissue')->where($where)->field('id,oid,status,reason,company,sid,created,invoice')->select(); //补发记录
		$talk = M('Service_talk')->where($where)->field('id,reason,status,memo,created')->select(); //沟通记录
		$fund = M('Service_fund')->where($where)->field('id,reason,status,money,type,created')->select(); //差价记录
		
		$this->assign("niffer", $niffer);
		$this->assign("back", $back);
		$this->assign("reissue", $reissue);
		$this->assign("talk", $talk);
		$this->assign("fund", $fund);
		$this->assign("tid", $tid);
		$this->assign("oid", $oid);
		$this->display();
	}
	
	/**
	 * 查看记录
	 */
	public function view() {
		$id = $this->_get('id', 'trim', ''); //记录编号
		!$id && $this->error('参数错误');
		$model = $this->_request('model', 'trim', '');
		!$model && $this->error("请选择要操作的类型");
		$where = array('nick'=>$this->nick, 'id'=>$id);
		$table = 'Service_'.$model;
		$info = M($table)->where($where)->find();
		$trade = M('Trade')->where(array('tid'=>$info['tid']))->field('created,pay_time,buyer_nick,receiver_name')->find();
		$this->assign("info", $info);
		$this->assign("trade", $trade);
		$this->display($model.'view');
	}
	
	/**
	 * 添加记录
	 */
	public function add() {
		$tid = $this->_get('tid', 'trim', ''); //子订单编号
		$oid = $this->_get('oid', 'trim', ''); //子订单编号
		if (!$tid && !$oid) $this->error('参数错误');
		$model = $this->_get('model', 'trim', '');
		!$model && $this->error("请选择要操作的类型");
		$table = 'Service_'.$model;
		if (IS_POST) {
			if ($model == 'niffer') { //换货
				if (!$_POST['receiver_sid']) unset($_POST['receiver_company'],$_POST['receiver_sid']); //清除无效快递
				if (!$_POST['send_sid']) unset($_POST['send_company'],$_POST['send_sid']); //清除无效快递
				if ($_POST['props']) {
					$properties = implode(';', $_POST['props']);
					$_POST['sku_id'] = M('Sku')->getFieldByProperties($properties,'sku_id'); //获取新属性的sku_id
					$_POST['properties_name'] = D('Props')->get_props($_POST['num_iid'], 4, $properties); //获取新属性的properties_name
					unset($_POST['props']);
				}
			}
			$Serivce = D($table);
			if ($Serivce->create()) {
				$Serivce->nick = $this->nick;
				$Serivce->created = time();
				$Serivce->modified = time();
				$Serivce->add();
				$this->success('成功添加记录！');
			} else {
				$this->error($Serivce->getError());
			}
		}
		$where['nick'] = $this->nick;
		
		if (in_array($model, array('back','reissue','niffer'))) { //子订单信息
			$info = M('Order')->where(array('oid'=>$oid))->field('tid,oid,num_iid,num,sku_id,outer_iid,sku_properties_name')->find();
		} else {
			$info['tid'] = $tid;
		}
		if ($model == 'niffer') { //换货的销售属性
			$properties = M('Sku')->getFieldBySku_id($info['sku_id'],'properties');
			$info['properties'] = explode(';', $properties);
			$info['old_properties_name'] = $info['sku_properties_name']; //订单属性名称
			$info['props'] = D('Props')->get_props($info['num_iid'], 1); //宝贝销售属性
		}		
		$this->assign("info", $info);
		$this->display($model.'edit');
	}
	
	/**
	 * 编辑记录
	 */
	public function edit() {
		$id = $this->_get('id', 'trim', ''); //记录编号
		!$id && $this->error('参数错误');
		$model = $this->_request('model', 'trim', '');
		!$model && $this->error("请选择要操作的类型");
		$table = 'Service_'.$model;
		if (IS_POST) {
			if (!$_POST['id']) $this->error('参数错误');
			if ($model == 'niffer') { //换货
				if (!$_POST['receiver_sid']) unset($_POST['receiver_company'],$_POST['receiver_sid']); //清除无效快递
				if (!$_POST['send_sid']) unset($_POST['send_company'],$_POST['send_sid']); //清除无效快递
				if ($_POST['props']) {
					$properties = implode(';', $_POST['props']);
					$_POST['sku_id'] = M('Sku')->getFieldByProperties($properties,'sku_id'); //获取新属性的sku_id
					$_POST['properties_name'] = D('Props')->get_props($_POST['num_iid'], 4, $properties); //获取新属性的properties_name
					unset($_POST['props']);
				}
			}
			$Serivce = D($table);
			if ($Serivce->create()) {
				$Serivce->modified = time();
				$Serivce->save();
				$this->success('成功更新记录！');
			} else {
				$this->error($Serivce->getError());
			}
		}
		$where['nick'] = $this->nick;
		$info = M($table)->getById($id);
		if ($info['nick'] <> $this->nick) $this->error('不要乱来哦');
		if ($model == 'niffer') { //换货的销售属性
			$order = M('Order')->where(array('oid'=>$info['oid']))->field('sku_id,sku_properties_name')->find();
			$properties = M('Sku')->getFieldBySku_id($order['sku_id'],'properties');
			$info['properties'] = explode(';', $properties);
			$info['tr'] = count($info['properties']);
			$info['old_properties_name'] = $order['sku_properties_name']; //订单属性名称
			$info['props'] = D('Props')->get_props($info['num_iid'], 1); //宝贝销售属性
		}
		$this->assign("info", $info);
		$this->display($model.'edit');
	}
	
	/**
	 * 操作记录
	 * @param intval $delid
	 */
	public function act() {
		$id = $this->_request('id', 'trim', '');
		!$id && $this->error("请选择要操作的记录");
		$act = $this->_request('act', 'trim', '');
		!$act && $this->error("请选择需要的操作");
		$model = $this->_request('model', 'trim', '');
		!$model && $this->error("请选择要操作的类型");
		$table = 'Service_'.$model;
		$where = array('nick'=>$this->nick, 'id'=>$id); //条件
		$data = array('modified'=>time()); //数据
		switch ($act) {
			case 'receiver':
				$data['status'] = '已收货';
				break;
			case 'send':
				$data['status'] = '已发货';
				break;
			case 'failing':
				$data['status'] = '失败';
				break;
			case 'finish':
				$data['status'] = '完成';
				break;
		}
		M($table)->where($where)->save($data);
		$this->success('记录操作成功！');
	}
	
	/**
	 * 删除记录
	 * @param intval $delid 记录ID
	 * @param str $model 模型
	 */
	public function del() {
		$delid = $this->_request('delid', 'trim', '');
		!$delid && $this->error("请选择要删除的记录");
		$model = $this->_request('model', 'trim', '');
		!$model && $this->error("请选择要操作的类型");
		$table = 'Service_'.$model;
		$ids = explode(',', trim($delid, ','));
		foreach ($ids as $id) {
			M($table)->where(array('id'=>$id))->delete(); //删除记录
		}
		$this->success('记录删除成功！');
	}
}
?>
