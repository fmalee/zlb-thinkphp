<?php
/**
 * 商家编码
 */
class SerialAction extends TbbaseAction {
	
	/**
	 * 使用中的商家编码列表
	 */
	public function index() {
		$where['nick'] = $this->nick;
		$start_price = $this->_get('start_price', 'intval', '');
		$end_price = $this->_get('end_price', 'intval', '');
		if ($start_price) {
			if (!$end_price) $this->error('请输入最高价格');
			if ($start_price > $end_price) $this->error('起始金额不能大于结束金额');
			$where['price']= array(array('gt', $start_price), array('lt', $end_price));
		}
		$keyword = $this->_get('keyword', 'trim', '');
		$keyword && $where['serial|num_iid|iid|model'] = array('like', "%$keyword%");
		
		$Serial = M('Serial');
		$count = $Serial->where($where)->count();
		$Page = new Page($count, 30);
		$page = $Page->show();
		
		$infos = $Serial->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('modified DESC,id DESC')->select();
		foreach($infos as $k => $info) {
			$item = M('Item')->where(array('num_iid' => $info['num_iid']))->field('title,pic_url,discount,volume')->find();
			$alias = M('Serial_alias')->where(array('serial' =>$info['serial'],'nick' => $this->nick))->count();
			if ($item) $infos[$k] = array_merge ($infos[$k], $item);
			$infos[$k]['alias'] = $alias;
		}
		
		$this->assign("start_date", $_GET['start_price']);
		$this->assign("end_date", $_GET['end_price']);
		$this->assign("keyword", $keyword);
		$this->assign("infos", $infos);
		$this->assign("page", $page);
		$this->parent_name = "商家编码";
		$this->seo = "店铺货源 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 供货商列表
	 * AJAX
	 */
	public function alias() {
		$serial = $this->_get('serial', 'trim', '');
		!$serial && $this->error('参数错误');
		
		$Goods = D('Goods');
		$alias = M('Serial_alias')->where(array('nick'=>$this->nick, 'serial'=>$serial))->order('status DESC,id DESC')->select();
		foreach ($alias as $k => $v) {
			$alias[$k]['shop'] = $Goods->iid2shop($v['iid']);
		}
		$this->assign("infos", $alias);
		$this->display();
	}
	
	/**
	 * 备用货源列表
	 */
	public function spare() {
		$where['nick'] = $this->nick;
		$where['status'] = 77;
		$start_price = $this->_get('start_price', 'intval', '');
		$end_price = $this->_get('end_price', 'intval', '');
		if ($start_price) {
			if (!$end_price) $this->error('请输入最高价格');
			if ($start_price > $end_price) $this->error('起始金额不能大于结束金额');
			$where['price']= array(array('gt', $start_price), array('lt', $end_price));
		}
		$keyword = $this->_get('keyword', 'trim', '');
		$keyword && $where['Serial.serial|Serial.num_iid|Serial.iid|Serial.model'] = array('like', "%$keyword%");
		
		$Serial = M('Serial_alias');
		$count = $Serial->where($where)->count();
		$Page = new Page($count, 30);
		$page = $Page->show();
		
		$Goods = D('Goods');
		$infos = $Serial->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('modified DESC,id DESC')->select();
		foreach($infos as $k => $info) {
			$infos[$k]['shop'] = $Goods->iid2shop($info['iid']);
			$item = M('Goods')->where(array('id' => $info['iid']))->field('title,pic_url')->find();
			if ($item) $infos[$k] = array_merge ($infos[$k], $item);
		}
		
		$this->assign("start_date", $_GET['start_price']);
		$this->assign("end_date", $_GET['end_price']);
		$this->assign("keyword", $keyword);
		$this->assign("infos", $infos);
		$this->assign("page", $page);
		$this->parent_name = "商家编码";
		$this->seo = "备用货源 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 设置默认编码
	 */
	public function major() {
		$iid = $this->_request('iid', 'trim', '');
		!$iid && $this->error("参数错误");
		
		$alais = M('Serial_alias')->where(array('iid'=>$iid, 'nick'=>NICK))->getField('iid');
		if ($alais) {
			$result = D('Serial')->set_major($iid);
			if ($result) {
				$this->success('成功更新首选供应商');
			} else {
				$this->error('更新首选供应商失败');
			}
			
		} else {
			$this->error('参数错误');
		}
	}
	
	/**
	 * 删除商家编码
	 * @param intval $delid
	 */
	public function del() {
		$delid = $this->_request('delid', 'trim', '');
		!$delid && $this->error("请选择要删除的商家编码");
		
		$ids = explode(',', trim($delid, ','));
		$err = '';
		foreach ($ids as $id) {
			$info = M('Serial')->where(array('nick'=>$this->nick, 'id'=>$id))->field('serial,num_iid')->find();
			$has = M('Item')->getFieldByNum_iid($info['num_iid'], 'num_iid');
			if ($has) {
				$err .=$info['serial'].'&nbsp;'; //添加记数
				continue;
			}
			$where = array('serial'=>$info['serial'], 'nick'=>$this->nick);
			M('Serial')->where($where)->delete();
			M('Serial_alias')->where($where)->delete();
		}
		if ($err) {
			$msg = "部分编码未删除：$err";
		} else {
			$msg = '商家编码删除成功！';
		}
		$this->success($msg);
	}
	
	/**
	 * 编辑供货商编码
	 */
	public function aedit() {
		$iid = $this->_request('iid', 'trim', '');
		!$iid && $this->error("请选择要编辑的供货商编码");
		if (IS_POST) {
			$result = D('Serial')->edit($_POST['item']);
			if ($result) {
				$this->success('成功更新商家编码');
			} else {
				$this->error('更新商家编码失败');
			}
		}
		$item = D('Serial')->get_item($iid);  //获取商品及编码信息
	
		$this->assign("item", $item);
		$this->display();
	}
	
	/**
	 * 删除供货商编码
	 * @param intval $delid
	 */
	public function adel() {
		$delid = $this->_post('delid', 'trim', '');
		!$delid && $this->error("请选择要删除的供货商编码");
		
		$ids = explode(',', trim($delid, ','));
		$err = '';
		foreach ($ids as $id) {
			$where = array('nick'=>$this->nick, 'iid'=>$id);
			$status = M('Serial_alias')->where($where)->getField('status');
			if ($status == 99) {
				$err .=$id.'&nbsp;'; //添加记数
				continue;
			}
			M('Serial_alias')->where($where)->delete();
		}
		if ($err) {
			$msg = "部分编码未删除：$err";
		} else {
			$msg = '备用编码删除成功！';
		}
		$this->success($msg);
	}
	
	/**
	 * 随机获取编码
	 */
	public function random_serial() {
		$serial = D('Serial')->create_serial();
		$this->success($serial);
	}
	
	/**
	 * 检查编码是否存在
	 */
	public function check_serial() {
		$serial = $this->_get('str','trim','');
		!$serial && $this->error('请输入编码!');
		$has = M('Serial_alias')->where(array('serial'=>$serial,'nick' =>NICK))->getField('serial');
		if ($has) {
			$this->error('编码已存在！');
		} else {
			$this->success('有效编码');
		}
	}
}
?>
