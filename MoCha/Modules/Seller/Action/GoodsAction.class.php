<?php
/**
 * 货源
 */
class GoodsAction extends TbbaseAction {
	
	public function index() {
		$Goods = M('Goods');
		$Shop = M('Shop');
		$where['status'] = array('lt', 100); //状态
		$pagesize = $this->_get('pz', 'intval', 60); //设置页码
		$this->assign("pz", $pagesize);
		$nick = $this->_get('nick', 'trim', ''); //昵称
		$name = $this->_get('name', 'trim', ''); //供应商名称
		$name && $nick = $Shop->getFieldByTitle($name, 'nick');
		if ($nick) {
			$where['nick'] = $nick;
			$this->assign("nick", $nick);
		} else {
			$this->assign("name", $name);
		}
		$keyword = $this->_get('keyword', 'trim', '');  //标题
		if ($keyword) {
			$this->assign("keyword", $keyword);
			$keywords = explode(' ', $keyword);  //关键字分词
			if (count($keywords) > 1) {
				foreach ($keywords as $keyword) {
					$likes[] = "%$keyword%";
				}
				$where['title'] = array('like', $likes, 'AND');
			} else {
				$where['title'] = array('like', "%$keywords[0]%");
			}
		}
		$location = $this->_get('location', 'trim', ''); //位置
		if ($location) {
			$nicks = $Shop->where(array('location'=>$location))->field('nick')->select();
			if ($nicks) {
				foreach ($nicks as $v) {
					$narr[] = $v['nick'];
				}
				$where['nick'] = array('in', $narr);
			}
		}
		$this->assign("location", $location);
		$cid = $this->_get('cid', 'intval', 0); //类目
		if ($cid) {
			$arrchild_cid = M('Cats')->where(array('cid'=>$cid))->getFieldByCid($cid, 'arrchild_cid');
			if ($cid === $arrchild_cid) {
				$where['cid'] = $cid;
				
			} else {
				$where['cid'] = array('in', $arrchild_cid);
			}
		}
		$this->assign("cid", $cid);
		$begindate = $this->_get('begindate', 'trim', ''); //记录时间
		$enddate = $this->_get('enddate', 'trim', '');
		if ($begindate) {
			if (!$enddate) $enddate = date('Y-m-d');
			$this->assign("begindate", $begindate);
			$this->assign("enddate", $enddate);
			if ($begindate > $enddate) $this->error('起始时间不能大于结束时间');
			$where['created']= array(array('gt', strtotime($begindate)), array('lt', strtotime($enddate)));
		}
		
		$count = $Goods->where($where)->count();
		$Page = new Page($count, $pagesize);
		$page = $Page->show();
		
		//$infos = $Goods->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('modified DESC,id DESC')->select();
		$infos = $Goods->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->select();
		foreach($infos as $k => $info) {
			$shop = M('Shop')->where(array('nick' => $info['nick']))->field('title,sid')->find();
			$infos[$k]['shop_name'] = $shop['title'];
			$infos[$k]['shop_url'] = "shop".$shop['url_path'].'.taobao.com';
			$infos[$k]['model'] = $info ['model'] ? $info ['model'] : $info ['id'];
		}
		
		$this->assign("infos", $infos);
		$this->assign("page", $page);
		$this->parent_name = '货源管理';
		$this->seo = '货源管理 - 卖家中心';
		$this->display();
	}
	
	/**
	 * 同步货源准备
	 */
	public function collect() {
		$cache = array();
		$cache['page'] = 1;
		$nick = $this->_get('nick', 'trim', '');
		$nick && $cache['nick'] = $nick;
		$cache['type'] = $this->_get('type', 'trim', 0);
		//把采集信息写入缓存
		cookie('goods_syn', serialize($cache));
		$this->success('开始同步货源记录');
	}
	
	/**
	 * 同步货源开始
	 */
	public function syn() {
		$cache = unserialize(cookie('goods_syn'));
		!$cache && $this->error('参数错误，请重试');
		$cache['pagesize'] = 200;
		$Model = D('Item');
		$Goods = M('Goods');
		$Taoapi = new Taoapi();
		$ones = 2; //每次处理的会员数
		$where = array();

		if ($cache['nick']) {
			$map['nicks'] = $cache['nick'];
			$where['nick'] = $cache['nick'];
			$cache['total'] = 1;
			$cache['pagenum'] = 1;
		} else {
			switch ($cache['type']) {
				case 1 : // 更新代理
					$where = 'status=99';
					break;
				case 2 : // 更新网销
					$where = 'source=1';
					break;
				case 3 : // 更新其他
					$where = 'sou+rce>10 AND source<50';
					break;
				case 4 : // 更新分店
					$where = 'source=50';
					break;
				default : // 更新全部
					$where['source'] = array('lt', 50);
			}
			if (!$cache['total']) {
				$cache['total'] = M('Shop')->where($where)->count();
				$cache['pagenum'] = ceil($cache['total'] / $ones); //计算总数
				cookie('goods_syn', serialize($cache));
			}
			$offset = ($cache['page'] - 1) * $ones;
			$data = M('Shop')->where($where)->field('nick')->order('id DESC')->limit($offset,$ones)->select();
			$nick = array();
			foreach ($data as $v) $nick[] = $v['nick'];
			$map['nicks'] = implode(',', $nick);
		}
		if ($page == 1) {
			$Goods->where($where)->save(array('syn'=>0));
		}

		$map['page_no'] = 1;
		$map['page_size'] = $cache['pagesize'];
		$result = $Model->searchItems($map, 'num_iid');
		$total_results = $result['items_get_response']['total_results'];
		if ($cache['pagenum'] >= $cache['page']) {
			if ($total_results > 0) {
				$total_page = ceil($total_results / $cache['pagesize']);
				$items = $result['items_get_response']['items']['item'];
				
				for ($i = 1; $i <= $total_page; $i++) {
					if ($i > 1) {
						$map['page_no'] = $i;
						$result = $Model->searchItems($map, 'num_iid');
						$items = $result['items_get_response']['items']['item'];
					}
					$count = count($items);
					$count_page = ceil($count / 20);

					$Taoapi->session = SESSIONKEY;
			        $Taoapi->method = 'items.list';
			        $Taoapi->fields = 'num_iid,title,nick,pic_url,cid,price,type,created,modified';

					for ($j = 0; $j < $count_page; $j++) {
						$offset = $j * 20;
						
						$left = $count - $offset;
						if ($left < 20) {
							$length = $count - $offset;
						} else {
							$length = 20;
						}
						
						$num_iids = array_slice($items, $offset, $length);
						$iid_arr = array();
						foreach ($num_iids as $value) {
							$iid_arr[] = $value['num_iid'];
						}
						$num_iid = implode(',', $iid_arr);
						$Taoapi->select(array('num_iids'=>$num_iid));
					}

					$result = $Taoapi->get();
					if (isset($result['items_list_get_response'])) {
						$result = array($result);
					}

					foreach ($result as $items) {
						$items = $items['items_list_get_response']['items']['item'];
						foreach ($items as $item) {
							$item['id'] = $item['num_iid'];
							unset($item['num_iid']);

							$item['title'] = new_addslashes($item['title']);
							$item['created'] = strtotime($item['created']);
							$item['modified'] = strtotime($item['modified']);
							$item['syn'] = 1;
							$item['status'] = 66;
							
							$id = $Goods->getFieldById($item['id'], 'id');
							if ($id) {
								$Goods->save($item);  //更新数据
							} else {
								$Goods->add($item);  //新增数据
							}
						}
						
					}
				}
			}

			if ($cache['pagenum'] > $cache['page']) {
				$cache['page'] = $cache['page'] + 1; //计算页数
				cookie('goods_syn', serialize($cache));

				$this->assign('pagenum', $cache['pagenum']);
				$this->assign('total', $cache['total']);
				$this->assign('page', $cache['page']);
				$resp = $this->fetch('Public:syn');
				$this->ajaxReturn($resp, '同步货源记录', 1);
			}
		}
		$del['syn'] = 0;
		$del['status'] = array('lt', 99);
		$Goods->where($del)->delete();

		cookie('goods_syn', null); //删除缓存
		
		//$this->get_loser(); //获取遗失的goods
		
		$this->error('同步完成，将刷新页面');
	}

	/**
	 * 获取一些没有办法正常获取的GOODS
	 * 如JJ杰1986,您好哈喽
	 * 有用
	 */
	public function get_loser() {
		$id = "16888628160,14096914221"; //T99,K25
		$ids = explode(",", $id);
		$Item = D('Item');
		$fields = "num_iid,approve_status,cid,created,input_pids,input_str,modified,nick,num,outer_id,pic_url,price,seller_cids,title,type";
		foreach ($ids as $num_iid) {
			$Item->synItem($num_iid, 2, $fields);
		}
	}
	
	/**
	 * 添加单独的宝贝到数据库
	 */
	public function add() {
		$url = urldecode($this->_post('url', 'trim', ''));
		preg_match('/id=([0-9]{9,20})?/i', $url, $match_out); // TODO fma.正则改进
		
		if ($match_out[1]) {
			$info['id'] = $match_out[1];
			$item = D('Item')->synItem($info['id'], 0, 'title');  //获取商品及编码信息
			$info['title'] = $item['title'];
			$this->success($info);
		} else {
			$this->error('链接不正确，请输入正确的链接');
		}
	}
	
	/**
	 * 一键上传
	 * 将商品加入淘宝
	 */
	public function upload() {
		set_time_limit(0);  //防止中断
		$id = $this->_get('id', 'trim', '');
		!$id && $this->error('参数错误');
		$Serial = D('Serial');
		
		if (IS_POST) {
			$info = $_POST['item'];
			$Item = D('Item');
			
			$info['iid'] = $id;
			$result = $Serial->edit($info); // 货源入编码库
			!$result && $this->error('建立商家编码失败！');
			
			$setting = M('User')->getFieldByNick($this->nick,'setting');
			$setting = unserialize($setting); //获取卖家默认上传设置
			$fields = "num_iid,approve_status,auction_point,cid,created,delist_time,ems_fee,express_fee,freight_payer,has_discount,has_invoice,has_showcase,has_warranty,increment,input_pids,input_str,is_3D,is_ex,is_lightning_consignment,is_taobao,is_timing,is_virtual,is_xinpin,list_time,modified,nick,num,one_station,outer_id,pic_url,post_fee,postage_id,price,product_id,property_alias,props,sell_promise,seller_cids,sku,stuff_status,sub_stock,template_id,title,type,valid_thru,violation,item_img,prop_img,desc";
			$item = $Item->synItem($id, 2, $fields); // 获取在线最新宝贝信息
			!$item && $this->error('获取宝贝数据失败！');
			$item['outer_id'] = $info['serial'];  //合并 数组
			$item['title'] = $info['title'];
			
			$item['tmall'] = ($this->memberinfo['type']=="B") ? 1 : 0;
			
			$item['approve_status'] = $info['type'] ? 'onsale' : 'instock';
			$item['list_time'] = date('Y-m-d H:i:s', NOW_TIME + $setting['list_time'] * 60);
			$item['watermark'] = $setting['watermark'];
			$item['seller_cids'] = $setting['seller_cids'];
			$item['sub_stock'] = $setting['sub_stock'];
			$item['sell_promise'] = $setting['sell_promise'] ? "true" : "false";
			$item['postage_id'] = $setting['postage_id'];
			$item['template_id'] = $setting['template_id'];
			//$item['valid_thru'] = $setting['valid_thru'];
			
			$item['has_discount'] = "true";
			$item['has_showcase'] = "false";
			$item['has_warranty'] = "false";
			$item['is_3D'] = "false";
			$item['is_ex'] = "false";
			$item['is_lightning_consignment'] = "false";
			$item['is_taobao'] = "true";
			$item['is_xinpin'] = "false";
			$item['item_imgs'] = $item['item_imgs']['item_img'];
			
			!$item['skus'] && $this->error('缺少参数SKUS，宝贝无法上传！');
			foreach ($item['skus']['sku'] as $sku) {
				$sku_properties[] = $sku['properties'];
				$sku_prices[] = $sku['price'];
				$sku_quantities[] = $sku['quantity'];
				$sku_outer_ids[] = $item['outer_id'];
			}
			
			$item['sku_properties'] = implode(",", $sku_properties);
			$item['sku_prices'] = implode(",", $sku_prices);
			$item['sku_quantities'] = implode(",", $sku_quantities);
			$item['sku_outer_ids'] = implode(",", $sku_outer_ids);
			
			$item = $Item->checkProps($item);  //添加必要的类目属性
			
			if ($item['tmall']) {
				$item['brand'] = $setting['brand'];
				$item['input_pids'] = '20000,1632501'; //只适合服装类目
				$item['input_str'] = $item['brand'].','.$item['outer_id'];
				$item['auction_point'] = $setting['auction_point'] ? $setting['auction_point'] : '5';
				$item['has_invoice'] = "true";
			} else {
				if ($item['input_pids']) { // 用户自定义属性
					if (strstr($item['input_pids'], ',')) {
						$input_strs = explode(',', $item['input_str']);
						$input_strs[1] = $item['outer_id'];
						$item['input_str'] = implode(',', $input_strs);
					} else {
						$item['input_str'] = $item['outer_id'];
					}
				}
				$item['has_invoice'] = $setting['has_invoice'] ? "true" : "false";
			}
			
			$item['id'] = $id;
			$num_iid = $Item->addItem($item);  //上传商品
			if (is_numeric($num_iid)) {
				$Serial->set_major($id, $num_iid); // 生成主编码
				$this->success('宝贝上传成功！');
			} else {
				$this->error($num_iid);
			}
			
		}
		$item = $Serial->get_item($id);  //获取商品及编码信息
		$item['lenth'] = str_length($item['title'], '60');  //计算标题长度
		
		if ($item['status'] == '99') {
			echo '没有编码信息或是允许重复铺货';  //没有编码信息或是允许重复铺货
			exit();
		} else {
			$template = "upload";  //有编码信息，直接上传  编码为77
		}
		
		$this->assign("item", $item);
		$this->display($template);
	}
	
	/**
	 * 多进程替换详情图片
	 */
	public function public_desc() {
		if (!isset($_POST['num_iid']) || !isset($_POST['desc'])) exit(0);

		$Item = D('Item');
		$Item->setNick($_POST['nick']);
		$Item->replaceDesc($_POST['num_iid'], $_POST['desc']); //替换详情图片
	
		exit('Success');
	}

	/**
	 * 商品详情页
	 */
	public function read() {
		$id = $this->_get('id', 'trim', '');
		!$id && $this->error('参数错误');
		$fields = "nick,cid,created,delist_time,ems_fee,express_fee,freight_payer,input_pids,input_str,outer_id,pic_url,price,property_alias,props,seller_cids,title,item_img,prop_img,desc";
		$item = D('Item')->synItem($id, 0, $fields); // 更新宝贝信息
		$item['item_imgs'] = $item['item_imgs']['item_img'];
		
		$sale_props = D('Props')->get_props($item, 1);
		$normal_props = D('Props')->get_props($item);
		
		$shop = M('Shop')->where(array('nick' => $item['nick']))->field('pic_path,title, sid, phone, location, shop_score, business, qq')->find();
		$shop['url'] = "http://shop".$shop['sid'] . ".taobao.com";
		
		$this->assign("sale_props", $sale_props);
		$this->assign("normal_props", $normal_props);
		$this->assign("item", $item);
		$this->assign("shop", $shop);
		$this->parent_name = '货源管理';
		$this->seo = $item['title'].'货源管理 - 卖家中心';
		$this->display();
	}
}
?>
