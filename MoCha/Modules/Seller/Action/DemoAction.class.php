<?php
/**
 * 订单评价
 */
class IndexAction extends TbbaseAction {
	/**
     * 首页 
     */
    public function index() {
        $this->display();
    }
    
    /**
     * 判断字符长度
     * AJAX读取
     */
    public function strlen() {
    	$str = $this->_get('str', 'trim', '');
    	!$str && $this->error('参数错误');
    	$len = $this->_get('len', 'intval', 60);
    	$num = str_length($str, $len);
    	$this->ajaxReturn($num);
    }

    public function demo1() {
        $Taoapi = new Taoapi();
        $Item = D('Item');
        $Taoapi->method = 'trade.fullinfo';
        $Taoapi->session = SESSIONKEY;
        $Taoapi->fields = "pay_time, end_time,,tid,modified";
        $Taoapi->where = "tid=321980883441951";
        $Taoapi->select();
        $Taoapi->where = "tid=321797564513907";
$Taoapi->select();
        $Taoapi->where = "tid=243233636411938";
$Taoapi->select();
        $Taoapi->where = "tid=207316540230670";
$Taoapi->select();
        $Taoapi->where = "tid=243116833109881";
$Taoapi->select();
        $trade = $Taoapi->get();
        trace($trade,'a');
        
        $this->display('demo');
    }
    
    public function demo3() {
        $Tql = new Taoapi();
        $resp = D('Item')->get_Items(array('nicks'=>'kamozi'));
        trace($resp, 'a');
    }

    public function demo2() {
        $num_iids = '17408126569,17634941358,23215104607,19315559036,23087040170,17518666647,23086872068,19308003070,19431599828,19431039612,19430755477,17623241632,19430263958,23392336381,22995880615,19173735312,17417946872,19296271157,17410826187,22974288716';
        $num_iids2 = '17455725640,23438568512,17632246814,17632850828,17643597540,23434660638,17642869749,17642221962,22996132797,19164139069,17408126569,17634941358,23215104607,19315559036,23087040170,17518666647,23086872068,19308003070,19431599828,19431039612';

        $Tql = new Taoapi();
        $Tql->session = SESSIONKEY;
        $Tql->method = 'items.list';
        $Tql->fields = 'num_iid,title,nick,pic_url,cid,price,type,created,modified';

        $Tql->select(array('num_iids'=>$num_iids));
        $Tql->select(array('num_iids'=>$num_iids2));
        $Tql->select(array('num_iids'=>$num_iids));
        $Tql->select(array('num_iids'=>$num_iids2));
        $Tql->select(array('num_iids'=>$num_iids));
        $Tql->select(array('num_iids'=>$num_iids));
        $Tql->select(array('num_iids'=>$num_iids));
        $Tql->select(array('num_iids'=>$num_iids));
        $Tql->select(array('num_iids'=>$num_iids));
        $Tql->select(array('num_iids'=>$num_iids));
        $resp = $Tql->get();
        trace($resp, 'a');
    }
    public function demo() {
    	$Tql = new Taoapi();
        $Tql->session = SESSIONKEY;

        $Tql->fields = "num_iid";
        $Tql->method = 'items.onsale';
        $Tql->where = array('page_size'=>2, 'page_no'=>1);
        $Tql->select();

        $Tql->fields = "num_iid,title";
        $Tql->method = 'item';
        $Tql->where = array('num_iid'=>'__SUBSELECT__');
        $Tql->select();

        $result = $Tql->sub()->get();

/*        $Tql->method = 'shop';
        $Tql->session = SESSIONKEY;
        $Tql->where = array('nick'=>'__SUBSELECT__');
        $Tql->update(array('data'=>1,'nick'=>'joey'));
        $result = $Tql->sub()->get();
        trace($result, 'result');
*/

/*
        $Tql->method = 'shop';
        $Tql->session = SESSIONKEY;
        $Tql->where = array('nick'=>'joey');
        $Tql->data = array('data'=>'$nick$','nick'=>'$seller_nick$','title'=>'ssss');
        $Tql->insert();
        $result = $Tql->copy()->get();
        trace($result, 'result');*/
/*


        $Tql->fields = "title,num_iid,modified";
        $Tql->method = 'item';
        $Tql->session = SESSIONKEY;
        $Tql->where = array('num_iid'=>'23466100638');
        $Tql->select();

        $result = $Tql->get();
        trace($result, 'result');*/

        trace($result, 'result');
    	$this->display();
    }
    
    //发包函数
    function httpPost($url, $data, $timeout = '3') {
    	$url = $this->urlinfo($url); //获取URL信息
    	$query = $url["request"];
    	if(is_array($data)) { //数组转成拼接
    		$long = array("oid", "tid");  //把长数字的计数方式1.55+14转成数字
    		foreach($data as $k =>$v) {
    			if (in_array($k, $long)) $v = number_format($v, 0, '', '');  //把长数字的计数方式1.55+14转成数字
    			$data_arr .=(empty($data_arr) ? "" : "&").urlencode($k)."=".urlencode($v);
    		}
    	}
    	
    	$header = "POST ".$query." HTTP/1.1\r\n";
    	$header .= "Host:".$url["host"]."\r\n";
    	$header .= "Content-type: application/x-www-form-urlencoded\r\n";
    	$header .= "Content-Length:".strlen($data_arr)."\r\n";
    	//$header .= "Connection: Keep-Alive\r\n\r\n";
    	$header .= "Connection: Close\r\n\r\n";
    	if ($data_arr) $header.= "$data_arr\r\n\r\n";
    	
    
    	$fp = fsockopen($url["host"],$url["port"],$errno, $errstr, $timeout);
    	if (!$fp) F('b',"fsockopen打开失败：$errstr($errno)");
    	fwrite($fp,$header);
    	fclose($fp);
    	unset($header);
    	return true;
    }
    
    /**
     * URl相关信息
     * 'scheme' => 'http',
     * 'host' => '127.0.0.1',
     * 'path' => '/index.php',
     * 'query' => 'm=taobao&c=stream&a=asyn',
     * 'port' => '80',
     * 'ip' => '127.0.0.1',
     * 'request' => '/index.php?m=taobao&c=stream&a=asyn',
     */
    public function urlinfo($url) {
    	if (!$url) return false;
    	$url_arr  = parse_url($url);
    	$url_arr["path"]    = empty($url_arr["path"]) ? "/"  : $url_arr["path"];
    	$url_arr["port"]    = empty($url_arr["port"]) ? "80" : $url_arr["port"];
    	$url_arr["ip"]      = gethostbyname($url_arr["host"]);
    	$url_arr["request"] = $url_arr["path"] . (empty($url_arr["query"])    ? "" : "?" . $url_arr["query"]) . (empty($url_arr["fragment"]) ? "" : "#" . $url_arr["fragment"]);
    
    	return $url_arr;
    }
}
?>