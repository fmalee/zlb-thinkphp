<?php
/**
 * 订单评价
 */
class CatsAction extends TbbaseAction {
	
	/**
	 * 退款列表
	 */
	public function index() {
		$this->assign("nick", $this->nick);
		$this->parent_name = "数据管理";
		$this->seo = "数据管理 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 获取紧接着的下一级分类ID
	 */
	public function ajax_cats() {
		$cid = $this->_post('cid', 'intval', 0);
		$model = $this->_post('model', 'intval', 'item');
		$where = array('parent_cid' => $cid);
		$where['model'] = $model;
		
		$cats = M('Cats')->field('cid,name')->where($where)->select();
		if ($cats) {
			$this->ajaxReturn($cats, '获取成功', 1);
		} else {
			$this->ajaxReturn(0, '获取失败');
		}
	}
	
	/**
	 * AJAX获取类目
	 */
	public function ajax_cats1() {
		$id = $this->_get('id', 'trim', '');
		$cats = M('Cats')->where(array('parent_cid'=>$id, 'model'=>'item'))->getField('name,cid');
		foreach ($cats as $cat) {
			$data[$cat['cid']] =array('name'=>$cat['name'], 'cid'=>$cat['cid']);
		}
		$this->success($data);
	}
	
	/**
	 * 更新淘宝后台类目
	 */
	public function synCats() {
		$page = $this->_post('page', 'intval', 1);
		$Cats= D('Cats');
		$where = " `model`='item' and parent_cid=0";
		
		if ($page == 1) { //初始化
			$Cats->syn_itemcats(0); //获取顶级栏目
			$cache['pagesize'] = 8; //设置页码
			$cache['total'] = $Cats->where($where)->count(); //统计整数
			$cache['pagenum'] = ceil($cache['total'] / $cache['pagesize']); //计算页数
			cookie('cats_syn', serialize($cache));
		} else {
			$cache = unserialize(cookie('cats_syn')); //获取缓存
		}
		if ($page > $cache['pagenum']) { //更新完成
			cookie('cats_syn', null); //删除缓存
			$this->error('淘宝系统类目同步完成!');
		}
		
		$data = $Cats->where($where)->field('cid')->page($page . ',' . $cache['pagesize'])->order('cid DESC')->select();
		foreach ($data as $cat) {
			$Cats->syn_itemcats($cat['cid']); //获取各级栏目
		}
		
		$this->assign('pagenum', $cache['pagenum']);
		$this->assign('total', $cache['total']);
		$this->assign('page', $page);
		$resp = $this->fetch('syn');
		$this->ajaxReturn($resp, '同步淘宝系统类目', 1);
	}
	
	/**
	 * 同步店铺自有类目
	 * TODO 不能和repair同时执行。
	 */
	public function synSeller() {
		$this->nick = '英摩卡';
		$page = $this->_post('page', 'intval', 1);
		D('Cats')->syn_sellercats($this->nick);
		//$this->repair($this->nick); //有执行，但没有入库
		$this->error('更新店铺分类完成');
	}
	
	/**
	 * 更新系统类目的属性
	 */
	public function synProps() {
		$page = $this->_post('page', 'intval', 1);
		$Cats= D('Cats');
		$where = " `model`='item' AND `is_parent`=0"; //底层类目
		
		if ($page == 1) { //初始化
			$cache['pagesize'] = 500; //设置页码
			$cache['total'] = $Cats->where($where)->count(); //统计整数
			$cache['pagenum'] = ceil($cache['total'] / $cache['pagesize']); //计算页数
			cookie('props_syn', serialize($cache));
		} else {
			$cache = unserialize(cookie('props_syn')); //获取缓存
			
		}
		if ($page > $cache['pagenum']) { //更新完成
			cookie('props_syn', null); //删除缓存
			$this->error('淘宝类目属性同步完成!');
		}
		
		$data = $Cats->where($where)->field('cid')->page($page . ',' . $cache['pagesize'])->order('cid DESC')->select();
		foreach ($data as $v) {
			D('Props')->syn_Prop($v['cid']);  //更新类目属性
		}
		
		$this->assign('pagenum', $cache['pagenum']);
		$this->assign('total', $cache['total']);
		$this->assign('page', $page);
		$resp = $this->fetch('syn');
		$this->ajaxReturn($resp, '同步淘宝类目属性', 1);
	}
	
	/**
	 * 修复栏目层级关系
	 * @param $nick 更新店铺类目缓存
	 */
	public function repair($nick = "") {
		$page = $this->_post('page', 'intval', 1);
		$where = " `model`='item'";
		$Cats= D('Cats');
	
		if ($page == 1) { //初始化
			if ($nick) {
				$cache['nick'] = $nick;
				$where = " `model`='seller' and `nick`='$nick'";
			}
			$cache['pagesize'] = 2000; //设置页码
			$cache['total'] = $Cats->where($where)->count(); //统计整数
			$cache['pagenum'] = ceil($cache['total'] / $cache['pagesize']); //计算页数
			cookie('repair_syn', serialize($cache));
		} else {
			$cache = unserialize(cookie('repair_syn')); //获取缓存
			if ($cache['nick']) $where = " `model`='seller' and `nick`='$nick'";
		}
		if ($page > $cache['pagenum']) { //更新完成
			cookie('repair_syn', null); //删除缓存
			$this->error('类目缓存更新完成!');
		}
		$categorys = $Cats->where($where)->page($page . ',' . $cache['pagesize'])->order('cid DESC')->getField('cid,arrparent_cid,arrchild_cid');
		D('Cats')->repair($categorys);  //执行修复
	
		$this->assign('pagenum', $cache['pagenum']);
		$this->assign('total', $cache['total']);
		$this->assign('page', $page);
		$resp = $this->fetch('syn');
		$this->ajaxReturn($resp, '同步类目缓存', 1);
	}
}
?>
