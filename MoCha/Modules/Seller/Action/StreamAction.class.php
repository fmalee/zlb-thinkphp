<?php
/**
 * 主动通知
 */
class StreamAction extends Action {
	
	public $Increment;
	function _initialize() {
		import('COM.Tb.Increment');
		$this->Increment = new Increment();
	}
	
	/**
	 *查询开通的增量消息服务
	 */
	public function customer() {
		$customers = $this->Increment->get_customers();
		$total = $customers['total_results'];
		$items = $customers['app_customers']['app_customer'];
		echo array2string($customers);
	}

	/**
	 *开通增量消息服务
	 */
	public function permit() {
		$resp = $this->Increment->permit_customer();
	}
	
	/**
	 *关闭用户的增量消息服务
	 *TODO
	 */
	public function stop() {
		$nick = "a285209225";
		$customers = $this->Increment->stop_customer($nick, "get");
	}

	/**
	 * 处理业务包
	 */
	public function asyn() {
		Log::write(array2string($_POST), Log::ERR);
		
		if (!isset($_POST['topic']) || empty($_POST['topic'])) exit(0);
		$topic = $_POST['topic'];
		unset($_POST['topic']);
		if ($topic == 'trade') {  //交易
			import('COM.Tb.CometTrade');
			$Trade = new CometTrade();
			$Trade->Import($_POST);
		} elseif ($topic == 'refund') {  //退款
			import('COM.Tb.CometTrade');
			$Refund = new CometRefund();
			$Refund->Import($_POST);
		} else {  //商品
			import('COM.Tb.CometItem');
			$Item = new CometItem();
			$Item->Import($_POST);
		}
	
		exit('Success');
	}
	
	/**
	 * 获取哪些用户丢弃了消息
	 */
	public function discardinfo($data = "") {
		$data = $_POST ? $_POST : $data;
		$max = 3600; //时间间隔
		$begin = $data['begin']/1000;
		$end =  $data['end']/1000;
		while (true) {
			$mi = $begin + $max;
			if ($mi > $end) {
				$mi = $end;
				$over = true;
			}
			$infos = $this->Increment->syn_discardinfo(date('Y-m-d H:i:s', $begin), date('Y-m-d H:i:s'));
			if (is_array($infos)) {
				Log::write('discard:' . array2string($infos), Log::ERR); //TODO
				foreach ($infos as $info) {
					$info['start'] = date('Y-m-d H:i:s', $info['start']/1000);  //格式化时间
					$info['end'] = date('Y-m-d H:i:s', $info['end']/1000);
					$type = $info['type'];
					$this->$type($info);
				}
			}
				
			$begin = $mi;
			if (($begin > $end) || $over) exit();
		}
	}
	
	/**
	 * 增量查询商品变更
	 */
	public function item($info) {
		import('COM.Tb.CometItem');
		$Item = new CometItem();
		
		$items = $this->Increment->syn_items($info['nick'], $info['start'], $info['end']);
		foreach ($items as $item) {
			$Item->Import($item);
		}
		
		return true;
	}
	
	/**
	 *增量查询订单变更
	 */
	public function trade($info) {
		import('COM.Tb.CometTrade');
		$Trade = new CometTrade();
		$trades = $this->Increment->syn_trades($info['nick'], $info['start'], $info['end']);
		foreach ($trades as $trade) {
			$Trade->Import($trade);
		}
		
		return true;
	}
	
	/**
	 *增量查询退款变更
	 */
	public function refund($info) {
		import('COM.Tb.CometRefund');
		$Refund = new CometRefund();
		$refunds = $this->Increment->syn_refunds($info['nick'], $info['start'], $info['end']);
		foreach ($refunds as $refund) {
			$Refund->Import($refund);
		}
		
		return true;
	}
	
	public function init() {
		$info['start'] = date('Y-m-d H:i:s', 1341376389993/1000);  //格式化时间
		$info['end'] = date('Y-m-d H:i:s', 1341377100000/1000);
		$info['nick'] = '克洛马奇旗舰店';
		$items = $this->Increment->syn_items($info['nick'], $info['start'], $info['end']);
		exit();
		$start = "2012-07-04 12:00:00";
		$end =  "2012-07-04 12:59:59";
		$info['begin'] = strtotime($start)*1000;
		$info['end'] = strtotime($end)*1000;
		$infos = $this->discardinfo($info);
	}
	public function demo() {
		$start = "2012-05-20 16:00:00";
		$end =  "2012-05-20 16:59:59";
		$begin = 1337790600000;
		$end = 1337791500000;
		$start = date('Y-m-d H:i:s', $begin/1000);
		$end = date('Y-m-d H:i:s', $end/1000);
		echo $start;
		echo $end;
		exit();
		$info['begin'] = strtotime($start);
		$info['end'] = strtotime($end);
		$infos = $this->discardinfo($info);
	}
	
	public function demo1() {
		$start = "2012-05-27 01:00:00";
		$end =  "2012-05-30 14:20:59";
		$info['begin'] = strtotime($start) * 1000;
		$info['end'] = strtotime($end) * 1000;
		$infos = $this->discardinfo($info);
		exit();
	}
	
}
?>
