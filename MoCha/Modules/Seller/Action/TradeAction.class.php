<?php
/**
 * 订单管理
 */
class TradeAction extends TbbaseAction {
	
	/**
	 * 订单列表
	 * 包含自动更新
	 */
	public function index() {
		$Trade = M('Trade');
		$Order = M('Order');
		$Serial = M('Serial');
		$where = array();
		
		$where['seller_nick'] = $this->nick;
		$pagesize = $this->_post('pz', 'intval', 40); //设置页码
		$outerid = $this->_post('outerid', 'trim', ''); //商家编码
		$outerid && $where['outerid'] = $outerid;
		$status = $this->_request('status', 'trim', ''); //订单状态
		$status && $where['status'] = $status;
		$flag = $this->_post('flag', 'intval', ''); //旗帜分类
		$flag && $where['seller_flag'] = $flag;
		$refund = $this->_post('refund', 'intval', ''); //TODO退款订单
		$refund && $where['refund'] = 1;
		$closed = $this->_post('closed', 'intval', ''); //关闭的订单
		if ($closed && !$status) $where['status'] = array('in', array('TRADE_CLOSED_BY_TAOBAO','TRADE_CLOSED'));
		$rate = $this->_post('rate', 'intval', ''); //需要评价
		if ($rate && !$status) {
			$where['buyer_rate'] = 'false';
			$where['status'] = array('in', array('TRADE_CLOSED_BY_TAOBAO','TRADE_CLOSED'));
		}
		$begindate = $this->_post('begindate', 'trim', ''); //订单时间
		$enddate = $this->_post('enddate', 'trim', '');
		if ($enddate) {
			if (!$enddate) $this->error('请输入最高价格');
			if ($begindate > $enddate) $this->error('起始金额不能大于结束金额');
			$where['created']= array(array('gt', strtotime($begindate)), array('lt', strtotime($enddate)));
		}
		$keywords = $this->_post('keyword', 'trim', '');  //关键字
		if ($keywords) {
			$type_array = array('buyer_nick','receiver_name','receiver_mobile','orders','tid','out_sid','receiver_city');
			$searchtype =  $this->_post('searchtype', 'intval', 0); //关键字类别
			$searchfield = $type_array[$searchtype];
			//$keyword = strip_tags($keywords);
			$where[$searchfield] = array('like',"%$keywords%");
		}
		
		$count = $Trade->where($where)->count();
		$Page = new Page($count, $pagesize);
		$page = $Page->show();
		
		$infos = $Trade->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('created DESC,tid DESC')->select();
		foreach($infos as $k => $info) {
			$orders = $Order->where(array('tid'=>$info['tid']))->order('oid DESC')->select();
			$discount = 0;
			foreach ($orders as $v => $order) {
				$item = $Serial->where(array('nick' => $this->nick, 'num_iid'=>$order['num_iid'], 'serial' => $order['outer_iid']))->field('iid,price as discount,model,barcode,stock')->find();
				$item && $orders[$v] = array_merge($item, $order);
				$discount = $discount + $item['discount'];
			}
			$infos[$k]['discount'] = $discount;
			$infos[$k]['orders'] = $orders;
			
		}
		
		$this->assign("infos", $infos);
		$this->assign("page", $page);
		$this->assign("pz", $pagesize);
		$this->parent_name = '交易管理';
		$this->seo = '已卖出的宝贝 - 卖家中心';
		$this->display();
	}
	
	/**
	 * 订单列表
	 * gridtable版
	 */
	public function table() {
		if ($_GET['ajax']) {
			$Trade = M('Trade');
			$Order = M('Order');
			$where = array();
			
			$where['seller_nick'] = $this->nick;
			$page = $this->_post('page', 'intval', 1); //设置页数
			$pagesize = $this->_post('rows', 'intval', 40); //设置页码
			$sort = $this->_post('sort', 'trim', '');  //排序字段
			$desc = $this->_post('order', 'trim', '');  //排序方式
			if($sort && $desc) {
				$order[$sort] = $desc;
			} else {
				$order['created'] = 'DESC';
			}
			$outerid = $this->_post('outerid', 'trim', ''); //商家编码
			$outerid && $where['outerid'] = $outerid;
			$status = $this->_post('status', 'trim', ''); //订单状态
			$status && $where['status'] = $status;
			$flag = $this->_post('flag', 'intval', ''); //旗帜分类
			$flag && $where['seller_flag'] = $flag;
			$refund = $this->_post('refund', 'intval', ''); //TODO退款订单
			$refund && $where['refund'] = 1;
			$closed = $this->_post('closed', 'intval', ''); //关闭的订单
			if ($closed && !$status) $where['status'] = array('in', array('TRADE_CLOSED_BY_TAOBAO','TRADE_CLOSED'));
			$rate = $this->_post('rate', 'intval', ''); //需要评价
			if ($rate && !$status) {
				$where['buyer_rate'] = 'false';
				$where['status'] = array('in', array('TRADE_CLOSED_BY_TAOBAO','TRADE_CLOSED'));
			}
			$begindate = $this->_post('begindate', 'trim', ''); //订单时间
			$enddate = $this->_post('enddate', 'trim', '');
			if ($enddate) {
				if (!$enddate) $this->error('请输入最高价格');
				if ($begindate > $enddate) $this->error('起始金额不能大于结束金额');
				$where['created']= array(array('gt', strtotime($begindate)), array('lt', strtotime($enddate)));
			}
			$keywords = $this->_post('keyword', 'trim', '');  //关键字
			if ($keywords) {
				$type_array = array('buyer_nick','receiver_name','receiver_mobile','orders','tid','out_sid','receiver_city');
				$searchtype =  $this->_post('searchtype', 'intval', 0); //关键字类别
				$searchfield = $type_array[$searchtype];
				//$keyword = strip_tags($keywords);
				$where[$searchfield] = array('like',"%$keywords%");
			}
			
			$total = $Trade->where($where)->count();
			$limt = ($page-1)*$pagesize. ',' . $pagesize;  //当前记录->field('tid,created,pay_time,buyer_nick,receiver_name')
			
			$infos = $Trade->where($where)->limit($limt)->order('created DESC,tid DESC')->select();
			foreach($infos as $k => $info) {
				$orders = $Order->where(array('tid'=>$info['tid']))->field('outer_iid,sku_properties_name,num')->select();
				$infos[$k]['orders'] = $orders;
				$infos[$k]['created'] = format_date($info['created']);
				$infos[$k]['invoice'] = $info['invoice'] ? format_date($info['invoice']) : '';
				$infos[$k]['modified'] = format_date($info['modified']);
				$infos[$k]['status'] = TradeStatus($info['status']);
			}
			$data = array('total'=>$total, 'rows'=>$infos ? $infos : array()); //无记录输出空数组
			$this->ajaxReturn($data);
		}
		$this->parent_name = '交易管理';
		$this->seo = '已卖出的宝贝 - 卖家中心';
		$this->display();
	}
	
	/**
	 * 增加备注
	 */
	public function memo() {
		$tid = $this->_get('tid', 'trim', '');
		!$tid && $this->error('参数错误');
		if (IS_POST) {
			$result = D('Trade')->synMemo($tid, $_POST['memo']);
			if ($result) {
				$this->success('成功修改备注');
			} else {
				$this->error('执行操作出错');
			}
		}
		$memo = M('Trade')->where(array('tid'=>$tid))->field('seller_memo,seller_flag')->find();
		$this->assign("memo", $memo);
		$this->display();
	}
	
	/**
	 * 延长收货时间
	 */
	public function delay() {
		$tid = $this->get('tid', 'trim', '');
		!$tid && $this->error('参数错误');
		if (IS_POST) {
			$delay = $this->_post('delay', 'intval', '3');
			$result = D('Trade')->synDelay($tid, $delay);
			if ($result) {
				$this->success('成功延长发货时间'.$delay.'天');
			} else {
				$this->error('执行操作出错');
			}
		}
		$this->display();
	}
	
	/**
	 * 关闭交易
	 */
	public function close() {
		$tid = $this->get('tid', 'trim', '');
		!$tid && $this->error('参数错误');
		if (IS_POST) {
			$close = $this->_post('close', 'trim', '');
			$result = D('Trade')->synClose($tid, $close);
			if ($result) {
				$this->success('成功关闭交易');
			} else {
				$this->error('执行操作出错');
			}
		}
		$this->display();
	}
	
	/**
	 * 更新销售属性
	 */
	public function sku() {
		$oid = $this->get('oid', 'trim', '');
		!$oid && $this->error('参数错误');
		if (IS_POST) {
			$order = $_POST['info'];
			$properties = implode(';', $order['skus']);
			$sku = M('Sku')->where(array('properties'=>$properties,	'num_iid'=>$order['num_iid']))->field('sku_id')->find();
			!$sku && $this->error('错误的SKU参数');
			$result = D('Trade')->synSku($oid, $sku['sku_id'], $properties);
				
			if ($result) {
				$this->db->update(array('sku_id'=>$sku['sku_id']), array('oid'=>$this->tid));
				$this->success('成功更改销售属性');
			} else {
				$this->error('执行操作出错');
			}
		}
		$order = M('Order')->where(array('oid'=>$oid))->field('tid,oid,title,pic_path,price,payment,num_iid,num,outer_iid,sku_properties_name')->find();
		$order['skus'] = D('Props')->get_props($order['num_iid'], 1);
		
		$this->display();
	}
	
	/**
	 * 修改价格
	 */
	public function price() {
		
	}
	
	/**
	 * 修改地址
	 */
	public function address() {
		
	}
	
	/**
	 * 发货
	 */
	public function logistics() {
		
	}
	
	/**
	 * 同步订单准备
	 */
	public function collect() {
		if (IS_POST) {
			$cache['status'] = $this->_post('status', 'trim', '');
			$cache['start'] = date('Y-m-d H:i:s',  NOW_TIME - $this->_post('time', 'trim', ''));
			$cache['end'] = date('Y-m-d H:i:s', NOW_TIME);
			$cache['pagesize'] = 49;
			//把采集信息写入缓存
			cookie('trade_syn', serialize($cache));
			$this->success('开始同步订单记录');
		}
		$this->display();
	}

	/**
	 * 同步订单开始
	 */
	public function syn() {
		$page = $this->_get('page', 'intval', 1);
		$cache = unserialize(cookie('trade_syn'));
		!$cache && $this->error('参数错误，请重试');
		
		$Taoapi = new Taoapi();
		$Trade = D('Trade');

		$where = array();
		$where['start_created'] = $cache['start'];
		$where['end_created'] = $cache['end'];
		$where['page_no'] = $page;
		$where['page_size'] = $cache['pagesize'];
		if ($cache['status']) $where['status'] = $cache['status'];

		if (!$cache['pagenum']) {
			$result = $Trade->synSold($where, 'tid'); //更新订单信息
	        
	        if (isset($result['trades_sold_get_response']['total_results'])) {
	            $total = $result['trades_sold_get_response']['total_results'];
	        }

			$total_page = ceil($total / $cache['pagesize']); //计算总数
			$cache['pagenum'] = $total_page;
			$cache['total'] = $total;
			cookie('trade_syn', serialize($cache));
		}
		if ($cache['total'] > 0) {
			$Trade->synTrades($where); //更新订单信息

			if ($cache['pagenum'] > $page) {
				$this->assign('pagenum', $cache['pagenum']);
				$this->assign('total', $cache['total']);
				$this->assign('page', $page);
				$resp = $this->fetch('Public:syn');
				$this->ajaxReturn($resp, '同步订单记录', 1);
			}
		}
		cookie('trade_syn', null); //删除缓存
		$this->error('同步完成，将刷新页面');
	}

	/**
	 * 同步单笔交易
	 * AJAX
	 */
	public function asyn() {
		$tid = $this->_get('tid', 'trim', '');
		!$tid && $this->error('参数错误');
		
		D('Trade')->synTrade($tid);
		$info = M('Trade')->getByTid($tid);
		$orders = M('Order')->where(array('tid'=>$tid))->select();
		$info['orders'] = $orders;
		
		$this->assign("info", $info);
		$this->display('trade');
	}
	
	/**
	 * 获取子订单
	 */
	public function getoid() {
		$tid = $this->_get('tid','trim','');
		!$tid && $this->error('请输入订单编号!');
		$orders = M('Order')->where(array('tid'=>$tid,'nick' =>NICK))->field('oid,outer_iid,sku_properties_name')->select();
		if ($orders) {
			$this->success($orders);
		} else {
			$this->error('不存在的订单编号');
		}
	}
}
?>
