<?php
/**
 * 卖家中心
 */
class IndexAction extends TbbaseAction {
	/**
     * 首页 
     */
    public function index() {
        $this->display();
    }
    
    /**
     * 判断字符长度
     * AJAX读取
     */
    public function strlen() {
        $str = $this->_get('str', 'trim', '');
        !$str && $this->error('参数错误');
        $len = $this->_get('len', 'intval', 60);
        $num = str_length($str, $len);
        $this->ajaxReturn($num);
    }
}
?>