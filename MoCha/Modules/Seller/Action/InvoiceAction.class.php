<?php
/**
 * 配货单
 */
class InvoiceAction extends TbbaseAction {
	
	/**
	 * 配货单首页
	 */
	public function index() {
		if ($_GET['ajax']) {
			$where = $order = $map = array();
			$where['nick'] = $this->nick;
			$keywords = $this->_post('keyword', 'trim', '');  //关键字
			if ($keywords) {
				$where['title|memo'] = array('like', "%$keywords%");
			}
			if (isset($_POST['sort']) && !empty($_POST['sort'])) {
				$order[$_POST['sort']] = $_POST['order'];
			} else {
				$order['created'] = 'DESC';
			}
			$page = isset($_POST['page']) && intval($_POST['page']) ? intval($_POST['page']) : 1;
			$pagesize = isset($_POST['rows']) && intval($_POST['rows']) ? intval($_POST['rows']) : 10;
			$Invoice   =   M('Invoice');
			$total  = $Invoice->where($where)->count();    //计算总数
			$limt = ($page-1)*$pagesize. ',' . $pagesize;  //当前记录
			
			$infos = $Invoice->where($where)->limit($limt)->order($order)->select();
			foreach ($infos as $key=>$info) {
				$trade =  M('Invoice_trade')->where(array('did'=>$info['id']))->count();  //统计订单数量
				$num =  M('Invoice_order')->where(array('did'=>$info['id']))->sum('num');  //统计宝贝数量
				$infos[$key]['trade'] = $trade;
				$infos[$key]['num'] = $num;
					
				$infos[$key]['created'] = format_date($info['created']); //格式化时间
			}
			
			$data = array('total'=>$total, 'rows'=>$infos ? $infos : array()); //无记录输出空数组
			$this->ajaxReturn($data);
		}
		$this->seo = "配货单 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 编辑配货单
	 * @param intval $id
	 */
	public function edit() {
		if (IS_POST) {
			$id = $this->_post('id', 'intval', '');
			$Invoice = D('Invoice');
			if ($Invoice->where(array('nick'=>NICK))->create()) {
				if ($id) {  //更新数据
					unset($_POST['id']);
					$Invoice->where(array('id'=>$id))->save();
					$this->success('成功更新配货单！');
				} else {  //新增数据
					$did = $Invoice->add();
					if ($_POST['import']) $this->autoipt($did, $_POST['import']); //TODO自动 导入
					$this->success('成功新建配货单！');
				}
			} else {
				$this->error($Invoice->getError());
			}
		}
		$id = $this->_get('id', 'intval', '');
		if (!$id) {
			$info['title'] = "配货单" . date('Ymd');
		} else {
			$info = M('Invoice')->where(array('id'=>$id, 'nick'=>$this->nick))->field('id,title,memo')->find();
		}
		$this->assign("info", $info);
		$this->display('edit');
	}
	
	/**
	 * 删除配货单
	 * @param intval $delid
	 */
	public function del() {
		$delid = $this->_request('delid', 'trim', '');
		!$delid && $this->error("请选择要删除的配货单");
		
		$ids = explode(',', trim($delid, ','));
		foreach ($ids as $id) {
			M('Invoice')->where(array('id'=>$id))->delete();
			M('Invoice_order')->where(array('did'=>$id))->delete();
			M('Invoice_trade')->where(array('did'=>$id))->delete();
		}
		$this->success('配货单删除成功！');
	}
	
	/**
	 * 订单列表
	 */
	public function trade() {
		$id = $this->_get('id', 'intval', '');
		!$id && $this->error('错误的参数！');
		
		$infos = array();
		$invoice = M('Invoice')->where(array('id'=>$id))->find();
		$num = M('Invoice_trade')->where(array('did'=>$id))->count();
		$trades = M('Invoice_trade')->where(array('did'=>$id))->field('tid,source')->select();
		foreach ($trades as $v) {
			$tid = $v['tid'];
			$field = 'tid, status, buyer_message, created, modified, end_time, shipping_type, seller_memo, pay_time, seller_flag, cod_status, buyer_nick, type, payment, receiver_name, consign_time, trade_from';
			$trade = M('Trade')->where(array('tid'=>$tid))->field($field)->find();
			$trade ['status'] = TradeStatus($trade['status']);
			if (!$trade['pay_time']) {
				$pay_time = "未付款";
				if ($trade['type'] == "cod") format_date($pay_time = $trade['created']);
			} else {
				$pay_time = format_date($trade['pay_time']);
			}
			$trade['pay_time'] = $pay_time;
			$trade['isorder'] = $v['source'];
			$infos[$tid] = $trade;
			$orders = M('Invoice_order')->where(array('tid'=>$tid,'did'=>$id))->field('oid,num_iid,outer_iid,sku_id,properties_name,num')->select();
			$infos[$tid]['orders'] = $orders;
		}
		
		$this->assign('invoice', $invoice);
		$this->assign('infos', $infos);
		$this->parent_name =  '配货单';
		$this->seo =  $invoice['title']." - 配货单 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 订单列表扩展
	 * AJAX获取
	 */
	public function more() {
		$tid = $this->_request('id', 'trim', '');
		!$tid && $this->error("请选择要操作的配货单");
		$infos = array();
		
		$trade = M('Trade')->where(array('tid'=>$tid))->find();
		$trade['created'] = format_date($trade['created']);
		$trade['modified'] = format_date($trade['modified']);
		$trade['pay_time'] = format_date($trade['pay_time']);
		//$serial =  M('Serial')->>where(array('tid'=>$tid))->find();
		$this->assign('trade', $trade);
		$this->display();
	}
	
	/**
	 * 配货单打印
	 */
	public function read() {
		$id = $this->_get('id', 'intval', '');
		!$id && $this->error("请选择要操作的配货单");
		
		$info = M('Invoice')->where(array('id'=>$id))->find();
		$orders = M('Invoice_order')->where(array('did'=>$id))->field('num_iid,outer_iid,sku_id,properties_name,num')->order('properties_name asc')->select();
		
		$items = array ();
		$sum_discount = $sum_num = 0;
		foreach($orders as $order) {
			extract($order); // 分拆数组成变量
			$outer_idss = explode('-', $outer_iid);
			$outer_id = $outer_idss [0];
			if (! array_key_exists($outer_id, $outer_ids)) {
				$item = M('Serial')->where(array('num_iid'=>$num_iid, 'serial'=>$outer_id, 'nick'=>$this->nick))->field('model,price,iid')->find();
				$v = M('goods')->where(array('id' =>$item['iid']))->field('nick')->find();
				$nick = $v['nick'];
				$shop = M('shop')->where(array('nick'=>$nick))->field('title,location')->find();
				extract($shop);
				
				$items[$title]['title'] = $title;
				$items[$title]['location'] = $location;
				$items[$title]['outer_id'][$outer_id]['model'] = $item['model'];
				$items[$title]['outer_id'][$outer_id]['discount'] = $item['price'];
				$outer_ids[$outer_id]['title'] = $title;
				$outer_ids[$outer_id]['price'] = $item['price'];
				$price = $item ['price'];
			} else {
				$title = $outer_ids[$outer_id]['title'];
				$price = $outer_ids[$outer_id]['price'];
			}
			$items[$title]['outer_id'][$outer_id]['sku_id'][$sku_id]['properties'] = $properties_name;
			$items[$title]['outer_id'][$outer_id]['sku_id'][$sku_id]['num'] += $num;
			$items[$title]['outer_id'][$outer_id]['num'] += $num;
			$items[$title]['outer_id'][$outer_id]['price'] += $price * $num;
			$sum_discount = $sum_discount + $price * $num;
			$sum_num = $sum_num + $num;
		}
		krsort ($items);
		$info['price'] = $sum_discount;
		$info['num'] = $sum_num;
		M('Invoice')->where(array('id'=>$id))->save(array('num'=>$sum_num, 'price'=>$sum_discount)); //更新配货单统计数据
		$this->assign('info', $info);
		$this->assign('items', $items);
		$this->seo =  $info['title']." - 配货单 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 编辑配货单中的订单
	 */
	public function tedit() {
		$did = $this->_get('did', 'intval', '');
		$tid = $this->_get('tid', 'trim', '');
		(!$did || !$tid) && $this->error("请选择要删除的订单");
		if (IS_POST) {
			$orders = $_POST['orders'];
			M('Invoice_order')->where(array('tid'=>$tid, 'did'=>$did))->delete(); //删除子订单
			foreach($orders as $k => $order) {
				foreach($order as $prop) {
					$data = array();
					$num_iid = $prop['num_iid'];
					$properties = implode(";", $prop['props']);
					$sku = M('Sku')->where(array('properties'=>$properties, 'num_iid'=>$num_iid))->field('sku_id,outer_id')->find();
					if (!$sku) continue;
					$id = M('Invoice_order')->where(array('sku_id'=>$sku['sku_id'], 'oid'=>$k, 'did'=>$did))->getField('id');
					
					if ($id) {
						M('Invoice_order')->where(array('id'=>$id))->setInc('num');
					} else {
						$data['tid'] = $tid;
						$data['did'] = $did;
						$data['oid'] = $k;
						$data['num_iid'] = $num_iid;
						$data['outer_iid'] = $sku['outer_id'];
						$data['sku_id'] = $sku['sku_id'];
						$data['properties_name'] = D('Props')->get_props($num_iid, 4, $properties);
						$data['num'] = 1;
						$data['created'] = NOW_TIME;
						M('Invoice_order')->add($data);
					}
				}
			}
			M('Invoice_trade')->where("tid=$tid")->save(array('source'=>'edit'));
			if ($tid) M('Trade')->where("tid=$tid")->save(array('invoice'=>NOW_TIME));
			$this->success('成功修改订单记录');
		}
		$trade = M('Trade')->where("tid=$tid")->field('status, buyer_message, seller_memo')->find();
		$info =  M('Invoice_order')->where(array('tid'=>$tid, 'did'=>$did))->field('oid, num_iid, outer_iid, sku_id, properties_name, num')->select();
		if (!$info) $this->error('参数错误！');
		$orders = array();
		$k = 1;
		foreach($info as $v) {
			$i = 1;
			$properties = M('Sku')->getFieldBySku_id($v['sku_id'], 'properties');
			$properties = explode(';', $properties);
			for($j = 1; $j <= $v['num']; $j ++) {
				$key = array_key_exists($i, $orders[$v['oid']]) ? $k : $i;  //TODO
				$orders[$v['oid']][$key]['properties'] = $properties;
				$orders[$v['oid']][$key]['properties_name'] = $v['properties_name']; //配货单属性名称
				$orders[$v['oid']][$key]['props'] = D('Props')->get_props($v['num_iid'], 1);
				$orders[$v['oid']][$key]['num_iid'] = $v['num_iid'];
				$orders[$v['oid']][$key]['outer_iid'] = $v['outer_iid'];
				$i ++;
				$k ++;
			}
		}
		$this->assign('tid', $tid);
		$this->assign('trade', $trade);
		$this->assign('orders', $orders);
		$this->display();
	}
	
	/**
	 * 删除配货单的订单记录
	 * @param intval $sid
	 */
	public function tdel() {
		$did = $this->_request('id', 'intval', '');
		$delid = $this->_request('delid', 'trim', '');
		(!$did || !$delid) && $this->error("请选择要删除的订单");
	
		$ids = explode(',', trim($delid, ','));
		$where = array('tid' => array('in',$ids), 'did' => $did);
		M('Invoice_trade')->where($where)->delete();
		M('Invoice_order')->where($where)->delete();
		/*
			foreach ($ids as $id) {
		M('Invoice_trade')->where(array('did'=>$did, 'tid'=>$id))->delete();
		M('Invoice_order')->where(array('did'=>$did, 'tid'=>$id))->delete();
		}*/
	
		$this->success('订单删除成功！');
	}
	
	/**
	 * 自动导入相关记录
	 * @param array $source 来源,数组或是用'逗号'隔开
	 */
	public function autoipt($did, $source = 'trade,niffer,reissue') {
		$source = strpos($source, ',') ? explode(',', $source) : $source;
		unset($_POST);
		$_POST['did'] = $did;

		if (in_array('trade', $source)) {  //订单
			$where = array('seller_nick'=>$this->nick);
			$where['credit'] = array('eq', '0');
			$where['status'] = 'WAIT_SELLER_SEND_GOODS';
			$ids = M('Trade')->where($where)->field('tid')->select();
			$_POST['id'] = '';
			foreach ($ids as $_v) {
				$_POST['id'] .=','.$_v['tid'];
			}
			$this->ipttrade(1); //导入配货单
		}

		if (in_array('niffer', $source)) { //换货记录
			$_POST['model'] = 'niffer';
			$ids = M('Service_niffer')->where(array('nick'=>$this->nick,'status'=>'已收货'))->field('id')->select();
			$_POST['id'] = '';
			foreach ($ids as $_v) {
				$_POST['id'] .=','.$_v['id'];
			}
			$this->iptserv(1); //导入配货单
		}

		if (in_array('reissue', $source)) { //补发记录
			$_POST['model'] = 'reissue';
			$ids = M('Service_reissue')->where(array('nick'=>$this->nick,'status'=>'跟踪中'))->field('id')->select();
			$_POST['id'] = '';
			foreach ($ids as $_v) {
				$_POST['id'] .=','.$_v['id'];
			}
			$this->iptserv(1); //导入配货单
		}
		
		return true;
	}
	
	/**
	 * 导入订单
	 * @param $return 是否返回跳转状态
	 */
	public function ipttrade($return = 0) {
		if (IS_POST) {
			$did = $this->_post('did', 'trim', ''); //配货单编号
			$id = $this->_post('id', 'trim', ''); //子订单编号
			if (!$did || !$id) $this->error('参数错误！');
			$Invoice_trade = M('Invoice_trade');
			$Invoice_order = M('Invoice_order');
			$Trade = M('Trade');
			$Order = M('Order');
			$ids = explode(',', trim($id, ','));
			foreach ($ids as $tid) {
				$orders = $Order->where(array('tid'=>$tid))->field('tid,oid,num_iid,sku_id,outer_iid,sku_properties_name,num')->select();
				if (!$orders) continue;
				$has_trade = $Invoice_trade->where(array('did'=>$did,'tid'=>$tid))->field('tid','source')->find();
				if (!$has_trade) {
					$Invoice_trade->add(array('did'=>$did,'tid'=>$tid,'source'=>'trade')); //添加配货交易表
				} elseif ($has_trade['source'] <> 'trade') {
					$Invoice_trade->where(array('did'=>$did,'tid'=>$tid))->save(array('source'=>'mix')); //更新类型
					//$Invoice_order->where(array('did'=>$did,'tid'=>$tid))->delete(); //删除配货订单
				}
				foreach ($orders as $order) {
					$has_order = $Invoice_order->where(array('did'=>$did,'tid'=>$order['tid'],'oid'=>$order['oid'],'sku_id'=>$order['sku_id']))->field('id')->find();
					if (!$has_order) {
						$order['properties_name'] = $order['sku_properties_name'];
						unset($order['sku_properties_name']);
						$order['did'] = $did;
						$order['created'] = NOW_TIME;
						$Invoice_order->add($order); //添加订单
					}
				}
				$Trade->where(array('tid'=>$tid))->save(array('invoice'=>NOW_TIME));
			}
			if (!$return) {
				$this->success('成功导入配货单');
			} else {
				return true;
			}
		}
		$id = $this->_get('tid', 'trim', ''); //订单编号
		!$id && $this->error('参数错误！');
		
		$infos = M('Invoice')->where(array('nick'=>$this->nick))->field('id,title')->limit('20')->order('id DESC')->select();
		foreach ($infos as $v) {
			$info[$v['id']] = $v['title'];
		}
		
		$this->assign("id", $id);
		$this->assign("info", $info);
		$this->display('import');
	}
	
	/**
	 * 导入售后
	 * @param $return 是否返回跳转状态
	 */
	public function iptserv($return = 0) {
		if (IS_POST) {
			$did = $this->_post('did', 'trim', ''); //配货单编号
			$id = $this->_post('id', 'trim', ''); //子订单编号
			$model = $this->_post('model', 'trim', ''); //售后类型
			if (!$did || !$id || !$model) $this->error('参数错误！');
			$table = 'Service_'.$model;
			$Invoice_trade = M('Invoice_trade');
			$Invoice_order = M('Invoice_order');
			$Service = M($table);
			$Order = M('Order');
			$ids = explode(',', trim($id, ','));
			foreach ($ids as $sid) {
				if ($model == 'reissue') {
					$oid = $Service->getFieldById($sid,'oid');
					$order = $Order->where(array('oid'=>$oid))->field('tid,oid,num_iid,sku_id,outer_iid,sku_properties_name,num')->find();
					$order['properties_name'] = $order['sku_properties_name'];
					unset($order['sku_properties_name']);
				} else {
					$order = $Service->where(array('id'=>$sid))->field('tid,oid,num_iid,sku_id,outer_iid,properties_name,num')->find();
				}
				$has_trade = $Invoice_trade->where(array('did'=>$did,'tid'=>$order['tid']))->field('tid')->find();
				if (!$has_trade) {
					$Invoice_trade->add(array('did'=>$did,'tid'=>$order['tid'],'source'=>$model)); //添加配货交易表
				} elseif ($has_trade['source'] <> $model) {
					$Invoice_trade->where(array('did'=>$did,'tid'=>$order['tid']))->save(array('source'=>'mix')); //更新类型
				}
				$has_order = $Invoice_order->where(array('did'=>$did,'tid'=>$order['tid'],'oid'=>$order['oid'],'sku_id'=>$order['sku_id']))->field('id')->find();
				if (!$has_order) { //添加配货订单表
					$order['did'] = $did;
					$order['created'] = NOW_TIME;
					$Invoice_order->add($order);
					$Service->where(array('id'=>$sid))->save(array('invoice'=>NOW_TIME));
				}
			}
			if (!$return) {
				$this->success('成功导入配货单');
			} else {
				return true;
			}
		}
		$id = $this->_get('id', 'trim', ''); //子订单编号
		$model = $this->_get('model', 'trim', ''); //售后类型
		if (!$id || !$model) $this->error('参数错误！');
		
		$infos = M('Invoice')->where(array('nick'=>$this->nick))->field('id,title')->limit('20')->order('id DESC')->select();
		foreach ($infos as $v) {
			$info[$v['id']] = $v['title'];
		}
		
		$this->assign("id", $id);
		$this->assign("model", $model);
		$this->assign("info", $info);
		$this->display('import');
	}
}
?>
