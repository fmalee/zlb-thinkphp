<?php
/**
 * 订单评价
 */
class ShopAction extends TbbaseAction {
	
	/**
	 * 商家首页
	 */
	public function index() {
		if ($_GET['ajax']) {
			$where = $order = $map = array();
			$title = $this->_post('title', 'trim', '');
			$title && $where['title']= array('like', "%$title%");
			$nick = $this->_post('nick', 'trim', '');
			$nick && $where['nick']= array('like', "%$nick%");
			$location = $this->_post('location', 'trim', '');
			$location && $where['location']= $location;
			$source = $this->_post('source', 'trim', '');
			$source && $where['source']= $source;
			$keywords = $this->_post('keyword', 'trim', '');  //关键字
			if ($keywords) {
				$where['title|nick|memo'] = array('like', "%$keywords%");
			}
			
			$page = $this->_post('page', 'intval', 1); //设置页数
			$pagesize = $this->_post('rows', 'intval', 40); //设置页码
			$sort = $this->_post('sort', 'trim', '');  //排序字段
			$desc = $this->_post('order', 'trim', '');  //排序方式
			if($sort && $desc) {
				$order[$sort] = $desc;
			} else {
				$order['created'] = 'DESC';
			}
			$Shop = M('Shop');
			$total = $Shop->where($where)->count();    //计算总数
			$limt = ($page-1)*$pagesize. ',' . $pagesize;  //当前记录
			
			$infos = $Shop->where($where)->limit($limt)->order($order)->select();
			foreach ($infos as $key=>$info) {
				$goods =  M('Goods')->where(array('nick'=>$info['nick']))->count();  //统计商品数量
				$infos[$key]['goods'] = $goods;
				$infos[$key]['created'] = format_date($info['created'], 0); //格式化时间
			}
			//TODO 商家分类
			
			$data = array('total'=>$total, 'rows'=>$infos ? $infos : array()); //无记录输出空数组
			$this->ajaxReturn($data);
		}
		$this->parent_name = '商家管理';
		$this->seo = "商家管理 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 编辑商家
	 * @param intval $id
	 */
	public function edit() {
		if (IS_POST) {
			$id = $this->_post('id', 'intval', '');
			$Shop = D('Shop');
			$_POST['business'] = implode(',', $_POST['business']); //合并服务
			
			if ($Shop->where(array('id'=>$id))->create()) {
				if ($id) {  //更新数据
					unset($_POST['id']);
					$Shop->where(array('id'=>$id))->save();
				} else {  //新增数据
					$Shop->add();
				}
				if ($_POST['nick']) { //更新店铺信息
					$Shop->syn_Shop($_POST['nick']);
				}
				$this->success('成功添加商家信息！');
			} else {
				$this->error($Shop->getError());
			}
		}
		$id = $this->_get('id', 'intval', '');
		if (!$id) {
			$info['source'] = 1; //网销
			$info['location'] = '星座';
			$info['business'] = '图片,数据包';
		} else {
			$info = M('Shop')->where(array('id'=>$id))->find();
		}
		$this->assign("info", $info);
		$this->display();
	}
	
	/**
	 * 删除商家
	 * @param intval $delid
	 */
	public function del() {
		$delid = $this->_request('delid', 'trim', '');
		!$delid && $this->error("请选择要删除的商家");
		
		$ids = explode(',', trim($delid, ','));
		foreach ($ids as $id) {
			$nick = M('Shop')->getFieldById($id, 'nick');
			M('Shop')->where(array('id'=>$id))->delete();
			M('Goods')->where(array('nick'=>$nick))->delete();
		}
		$this->success('商家删除成功！');
	}
	

	/**
	 * 下载商家产品
	 */
	public function goods() {
		if (!isset($_GET['nick']) || empty($_GET['nick'])) showmessage(L('illegal_action'), HTTP_REFERER);
		$nick = trim($_GET['nick']);
		$page = isset($_GET['page']) && intval($_GET['page']) ? intval($_GET['page']) : 1;
		$pageSize = 200;
		$where = "nicks=$nick and page_no=$page and page_size=$pageSize";
		$fields = "num_iid,title,nick,pic_url,cid,price,type,volume";
	
		$params['ql'] = "select $fields from items where $where";
		$items = $this->top->tql($params, SESSIONKEY);
		$total = $items['total_results'];
		$items = $items['items']['item'];
	
		$total_page = ceil($total / $pageSize);
		if ($total > 0) {
			$g_db = pc_base::load_model('taobao_goods_model');
			$table_name = $g_db->table_name;
			foreach ($items as $item) {
				$item['id'] = $item['num_iid'];
				unset($item['num_iid']);
				$fields = array_keys($item);
				$field = "`".implode ('`,`', $fields)."`";
				$values = array_values($item);
				$value = "'".implode ("','", $values)."'";
				$update = $this->db->sqls($item, ',');
				$sql = "INSERT INTO $table_name ($field) VALUES ($value) ON DUPLICATE KEY UPDATE $update";
				$this->db->query($sql);
			}
			if ($total_page > $page) {
				$str = "正在同步交易数据".($page * $pageSize).'/'.$total.'<script type="text/javascript">location.href="?m=taobao&c=shop&a=goods&nick='.$nick.'&page='.($page + 1).'&pc_hash='.$_SESSION ['pc_hash'].'"</script>';
				$url = '';
				showmessage($str, '');
			}
		}
		$str = '商品更新成功<script type="text/javascript">location.href="?m=taobao&c=shop&a=init"</script>';
		$url = '';
		showmessage($str, '');
	}
	
	/**
	 * 编辑店铺信息
	 * TODO 上传有问题
	 */
	public function profiles() {
		if (IS_POST) { //更新店铺信息
			//F('POST', $_POST);
			//$message = D('Shop')->update_Shop($_POST);
			//if ($message) $this->assign('message', $message);
		}
		$this->nick = "英摩卡";
		$shop = D('Shop')->syn_Shop($this->nick, 'title, desc, bulletin');
		$this->assign("shop", $shop);
		$this->seo = "店铺基本设置 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 默认上传设置
	 */
	public function setting() {
		if (IS_POST) {
			$data['setting'] = serialize($_POST['setting']);
			$data['has_setting'] = 1;
			M('User')->where(array('nick'=>$this->nick))->save($data);
			
			$this->assign("is_success", 1);
		}
		$cats_list = M('Cats')->where(array("model"=>"seller", "nick"=>$this->nick, "parent_cid"=>array('gt', 0)))->Field('cid,name')->select();
		foreach ($cats_list as $cat) {
			$cid = $cat['cid'];
			$cats[$cid] = $cat['name'];
		}
		$templates_list = D('Item')->synTemplates();
		foreach ($templates_list as $template) {
			if (!$template['shop_type']) continue;
			$template_id = $template['template_id'];
			$item_templates[$template_id] = $template['template_name'];
		}
		$templates_list = D('Logistics')->synTemplates("template_id, template_name");
		foreach ($templates_list as $template) {
			$template_id = $template['template_id'];
			$post_templates[$template_id] = $template['name'];
		}
		$setting = M('User')->getFieldByNick($this->nick, 'setting');
		$setting = unserialize($setting);
		
		//默认设置
		!$setting['brand'] && $setting['brand'] = 'other/其它品牌';
		!$setting['has_invoice'] && $setting['has_invoice'] = 0;
		!$setting['sell_promise'] && $setting['sell_promise'] = 1;
		!$setting['has_discount'] && $setting['has_discount'] = 1;
		!$setting['sub_stock'] && $setting['sub_stock'] = 0;
		!$setting['auction_point'] && $setting['auction_point'] = 5;
		
		$this->assign("cats", $cats);
		$this->assign("item_templates", $item_templates);
		$this->assign("post_templates", $post_templates);
		$this->assign("setting", $setting);
		$this->parent_name = '上传设置';
		$this->seo = "上传设置 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 设置默认店铺
	 */
	public function choose() {
		$user_id = $this->_get('uid', 'trim', '');
		!$user_id && $this->error('错误的参数');
		
		M('User')->where(array("userid"=>$this->userid))->save(array("major"=>0)); //取消全部默认
		M('User')->where(array("userid"=>$this->userid, 'user_id'=>$user_id))->save(array("major"=>1));
		
		$this->success(默认店铺设置成功);
	}
	
	/**
	 * 解除绑定
	 * TODO 等待其他模块完成
	 * @param intval $delid
	 */
	public function cancel() {
		$delid = $this->_request('delid', 'trim', '');
		!$delid && $this->error("请选择要删除的店铺");
	
		$ids = explode(',', trim($delid, ','));
		foreach ($ids as $id) {
			$nick = M('User')->getFieldByUser_id($id, 'nick');
			//需要删除的量比较大，做成同步订单退款等类似功能
			//M('Shop')->where(array('nick'=>$nick))->delete(); //删除店铺
			//M('Goods')->where(array('nick'=>$nick))->delete(); //删除店铺
			//M('Trade')->where(array('nick'=>$nick))->delete(); //删除店铺
			//M('Order')->where(array('nick'=>$nick))->delete(); //删除店铺
			//M('User')->where(array('id'=>$id))->delete(); //最后删除店铺
			//M('Invoice_order')->where(array('did'=>$id))->delete();
			//M('Invoice_trade')->where(array('did'=>$id))->delete();
		}
		$this->success('店铺删除成功！');
	}
	
	/**
	 * 多店铺管理
	 */
	public function manager() {
		$users = M('User')->where(array('userid'=>$this->userid))->field('user_id,nick,avatar,last_visit,sessionKey,seller_credit')->select();
		$shops = array();
		foreach ($users as $user) {
			$user['seller_credit'] = unserialize($user['seller_credit']);
			$shop = M('Shop')->where(array('nick'=>$user['nick']))->field('sid,shop_title,pic_path,created,modified')->find();
			$shops[] = array_merge($user, $shop);
		}
		$this->assign("appkey", C('APP_KEY'));
		$this->assign("shops", $shops);
		$this->seo = "店铺基本设置 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 授权登录
	 * 获取授权登录信息
	 */
	public function container() {
		if (!isset($_GET['top_appkey']) || !isset($_GET['top_parameters'])) $this->error('无效的授权信息，请重试!');
		$check = D('Shop')->check_container($_GET); //检查授权信息
		if (!$check) {
			$this->error('无效的授权信息，请重试!');
		} else {
			$this->success('授权成功！');
		}
	}
}
?>
