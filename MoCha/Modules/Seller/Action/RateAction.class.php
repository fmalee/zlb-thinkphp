<?php
/**
 * 订单评价
 */
class RateAction extends TbbaseAction {
	
	/**
	 * 评价列表首页
	 */
	public function index() {
		$where = array();
		$where['rate_type'] = ($_GET['rate_type'] == "give") ? "give" : "get";
		$where['role'] = $role = ($_GET['role'] == "seller") ? "seller" : "buyer";
		if (isset($_GET['result']) && $_GET['result']) $where['result'] = $_GET['result'];
		if ($where['result'] && !in_array($where['result'], array('good', 'neutral', 'bad'))) $this->error("参数错误！"); //验证评价结果参数
		if (isset($_GET['start_date']) &&$_GET['start_date']) $where['start_date'] = $_GET['start_date'];
		if (isset($_GET['end_date']) &&$_GET['end_date']) $where['end_date'] = $_GET['end_date'];
		if (strtotime($where['end_date']) > strtotime($where['end_date'])) $this->error("开始时间要大于结束时间！");   //验证评价起止时间
		if (isset($_GET['keyword']) && !empty($_GET['keyword'])) {
			$type_array = array('tid','num_iid');
			$type = intval($_GET['type']);
			$keyword = strip_tags(trim($_GET['keyword']));
			$type = $type_array[$type];
			$where[$type] = $keyword;
		}
		$page = $this->_get('page', 'intval', 1);
		$pagesize = 40;
		$results = D('Rate')->get_Rates($where, $page, $pagesize);
		$total = $results['total'];
		$rates = $results['rates'];
		
		$Page = new Page($total, $pagesize);
		// 设置分页显示
		//$list   = $Form->limit($Page->firstRow. ',' . $Page->listRows)->order('id desc')->select();
		$Page->setConfig('header', '条评价');
		$Page->setConfig('first', '<<');
		$Page->setConfig('last', '>>');
		$page = $Page->show();
		
		foreach ($rates as $rate) {
			$nicks[] = $rate['nick'];
		}
		$credit = $role."_credit";
		$fields = $credit.", type";
		$users = D('User')->syn_users($nicks, $fields);
		
		$this->assign("credit", $credit);
		$this->assign("keyword", $keyword);
		$this->assign("type", $_GET['type']);
		$this->assign("role", $where['role']);
		$this->assign("rate_type", $where['rate_type']);
		$this->assign("result", $where['result']);
		$this->assign("page", $page);
		$this->assign("rates", $rates);
		$this->assign("users", $users);
		
		$this->seo = "评价管理 - 卖家中心";
		$this->display();
	}
}
?>
