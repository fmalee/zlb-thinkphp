<?php
/**
 * 订单评价
 */
class LogisticsAction extends TbbaseAction {
	
	
	public function index() {
		
	}
	
	/**
	 * 地址库列表
	 */
	public function address() {
		$def = $_GET['def'];
		$infos = D('Logistics')->synAddress($def);
		
		$this->assign("infos", $infos);
		$this->parent_name = "地址库";
		$this->seo = "地址库 - 卖家中心";
		$this->display();
	}
	
	/**
	 * 编辑地址库
	 */
	public function addressedit() {
		$contact_id = $this->_get('contact_id', 'trim', '');
		if (isset($_POST['dosubmit']) && !empty($_POST['dosubmit'])) {
			$info = $_POST['info'];
			$info['phone'] = $info['phone'] ? implode ('-', $info ['phone']) : "";
			$info['phone'] =trim($info['phone'], '-');
			$areas = explode(",", $info['area']);
			$info['province'] = $areas[0];
			$info['city'] = $areas[1];
			$info['country'] = $areas[2];
			unset($info['area']);
			
			if ($contact_id) {
				$result = $this->db->modifyAddress($contact_id, $info);
			} else {
				$result = $this->db->addAddress($info);
			}
			if ($result) {
				showmessage('成功修改地址', HTTP_REFERER, '', 'edit');
			} else {
				showmessage('执行操作出错！', HTTP_REFERER);
			}
		}
		if ($contact_id) {
			$Area = D('Area');
			$info = D('Logistics')->synAddress("", $contact_id);
		
			if ($info) $info['phone'] = explode('-', $info['phone']);
			
			$province = $Area->where(array('name'=>$info['province']))->getField('id');
			$city = $Area->where(array('name'=>$info['city']))->getField('id');
			$country = $Area->where(array('name'=>$info['country']))->getField('id');
			$def = $province['id'].",".$city['id'].",".$country['id'];
		}
		
		$this->assign("info", $info);
		$this->display();
	}
	
	/**
	 * 删除地址库
	 */
	public function address_delete() {
		if (! isset($_GET ['contact_id'] ) || empty ( $_GET ['contact_id'] )) return false;
		$contact_id = $_GET ['contact_id'];
		$result = $this->db->removeAddress($contact_id);
		if (!$result) return false;
		exit('1');
	}
	
	/**
	 * AJAX获取区域列表
	 */
	public function ajax_area2() {
		$db = pc_base::load_model ('taobao_area_model');
		$id = $_GET ['id'] ? $_GET ['id'] : 1;
		$r = $db->select (array('name' => $id), "id,name,zip");
		foreach ( $r as $v ) {
			$data[$v['name']] = array ('id' =>$v['id'], 'zip' =>$v['zip']);
		}
		setcache('1', $data, "taobao");
		header('Content-Type: application/json; charset=UTF-8');
		echo json_encode($data);
	}
	
	/**
	 * AJAX获取区域列表
	 */
	public function ajax_area() {
		$db = pc_base::load_model('taobao_area_model');
		$id = $_GET ['id'] ? $_GET ['id'] : 1;
		$r = $db->select (array('parent_id' => $id), "id,name,zip");
		foreach ( $r as $v ) {
			$data[$v['id']] = array ('name' =>$v['name'], 'zip' =>$v['zip']);
		}
		header('Content-Type: application/json; charset=UTF-8');
		echo json_encode($data);
	}
	
	/**
	 * 物流流转信息查询
	 */
	public function trace() {
		$tid = $_GET['tid'];
		if (!$tid) return false;
		
		$trace = D('Logistics')->synTrace($tid, '英摩卡');
		$trace['tid'] = $tid;
		$this->assign("trace", $trace);
		$this->display();
	}
	
	/**
	 * 查询物流信息
	 * 金蝶版
	 */
	public function kingdee() {
		$company = $this->_get('company', 'trim', ''); //物流公司
		!$company && $this->error('参数错误');
		$sid = $this->_get('sid', 'trim', ''); //物流编号
		$info = D('Logistics')->getDelivery($company, $sid, 1);
		$this->assign("info", $info);
		$this->display();
	}
	
	/**
	 * 物流查询
	 * 暂时用kuaidi100的iframe
	 */
	public function search() {
		$title = "已卖出的宝贝 - 卖家中心";
		include template('taobao', 'logistics');
	}
	
	/**
	 * 获取并更新物流公司信息
	 * 表中有kuaidi100的信息，慎用
	 */
	public function update_company() {
		$companies = $this->db->synCompany();
		foreach ($companies as $company) {
			$this->db->insert($company);
		}
	}
	
	/**
	 * 获取并更新地区表
	 * 不需要经常更新
	 */
	public function update_areas() {
		$a_db = pc_base::load_model('taobao_area_model');
		$result = $a_db->synArea();
		
		if ($result) {
			showmessage('成功修改地址', HTTP_REFERER, '', 'edit');
		} else {
			showmessage('执行操作出错！', HTTP_REFERER);
		}
	}
}
?>
