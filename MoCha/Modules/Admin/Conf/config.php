<?php
return array(
    'URL_MODEL' => 0,
	'ADMIN_LOG' => true, //启用后台管理操作日志
	'LOGIN_TIMES' => 8, //后台最大登陆失败次数
	'SESSION_AUTO_START' => true,
    'USER_AUTH_KEY' => 'authId', //用户认证SESSION标记
    'ADMIN_AUTH_KEY' => 'administrator',
);