<?php

/**
 * 会员中心
 */
class IndexAction extends AdminAction {
	public function _initialize() {
		parent::_initialize();
    }

    /**
     * 管理中心首页 
     */
    public function index() {
		$message = array();
        if (is_dir('./install')) {
            $message[] = array(
                'type' => 'error',
                'content' => "您还没有删除 install 文件夹，出于安全的考虑，我们建议您删除 install 文件夹。",
            );
        }
        if (APP_DEBUG == true) {
            $message[] = array(
                'type' => 'error',
                'content' => "您网站的 DEBUG 没有关闭，出于安全考虑，我们建议您关闭程序 DEBUG。",
            );
        }
        if (!function_exists("curl_getinfo")) {
            $message[] = array(
                'type' => 'error',
                'content' => "系统不支持 CURL ,将无法采集商品数据。",
            );
        }
        $this->assign('message', $message);
        $system_info = array(
            'mocha_version' => 'MoCha v' . C('MOCHA_VERSION') . ' RELEASE '. C('MOCHA_RELEASE') .' [<a href="http://www.coromachi.com/" class="blue" target="_blank">查看最新版本</a>]',
            'server_domain' => $_SERVER['SERVER_NAME'] . ' [ ' . gethostbyname($_SERVER['SERVER_NAME']) . ' ]',
            'server_os' => PHP_OS,
            'web_server' => $_SERVER["SERVER_SOFTWARE"],
            'php_version' => PHP_VERSION,
            'mysql_version' => mysql_get_server_info(),
            'upload_max_filesize' => ini_get('upload_max_filesize'),
            'max_execution_time' => ini_get('max_execution_time') . '秒',
            'safe_mode' => (boolean) ini_get('safe_mode') ?  L('yes') : L('no'),
            'zlib' => function_exists('gzclose') ?  L('yes') : L('no'),
            'curl' => function_exists("curl_getinfo") ? L('yes') : L('no'),
            'timezone' => function_exists("date_default_timezone_get") ? date_default_timezone_get() : L('no')
        );
        $this->assign('seo_path', '管理中心');
        $this->assign('seo_title', '首页');
        $this->assign('system_info', $system_info);
        $this->display();
    }
    
    /**
     * 会员登陆
     */
    public function login() {
    	$this->display();
    }
    
    /**
     * 会员注销
     */
    public function logout() {
    	session('admin', null);
    	$this->success('成功退出系统！', U('index/login'));
    	exit;
    }
    
    /**
     * 会员登陆验证 
     */
    public function checkLogin() {
    	$username = $this->_post('username', 'trim');
    	$password = $this->_post('password', 'trim');
        if (!$username || !$password) {
            $this->error("请填写用户名或者密码！");
        }
        //安全锁
        $Times = M('Times');
        $rtime = $Times->getByUsername($username);
        if($rtime['times'] > 4) {
        	$minute = 60 - floor((NOW_TIME - $rtime['logintime']) / 60);
        	$this->error("登录次数过多，请1小时后重试！");
        }
        $Admin = M("Admin");
        //查询账户
        $info = $Admin->getByUsername($username);
        if (!$info) $this->error("用户不存在！");
        //验证用户密码
        $password = md5(md5(trim($password)).$info['encrypt']);
        $ip = get_client_ip();
        if($info['password'] != $password) {
        	if($rtime && $rtime['times'] < 5) {
        		$num = 5 - intval($rtime['times']);
        		$Times->where(array('username'=>$username))->save(array('ip'=>$ip, 'times'=>$rtime['times']+1));
        	} else {
        		$Times->add(array('username'=>$username, 'ip'=>$ip, 'logintime'=>NOW_TIME, 'times'=>1));
        		$num = 5;
        	}
        	$this->error("密码输入错误！还有".$num."次尝试机会");
        }
        $Times->where(array('username'=>$username))->delete();
        
        if($info['status'] != 1) $this->error("用户被锁定"); //如果用户被锁定
        session('admin', array(
        	'userid' => $info['userid'],
        	'username' => $info['username'],
        	'roleid' => $info['roleid'],
        ));
        M('Admin')->where(array('id'=>$info['userid']))->save(array('lastlogintime'=>time(), 'lastloginip'=>$ip));
        $forward = $_GET['forward'] ? $_GET['forward'] : $_POST['forward'];
        $this->success("登陆成功！", $forward ? $forward : U("Admin/Index/index"));
    }
}

?>
