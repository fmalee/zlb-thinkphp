<?php

class SettingAction extends AdminAction {

    public function _initialize() {
        parent::_initialize();
        $this->_mod = D('Setting');
    }

    public function index() {
    	if (IS_POST) {
    		set_config($_POST['setting']); // 保存进config文件
    		$this->success("设置保存成功");
    	} else {
    		$setting = include CONF_PATH . 'config.php';
    		$this->assign('setting', $setting);
    		$this->assign('seo_path', '全局');
    		$this->assign('seo_title', '站点设置');
    		$this->display();
    	}
        
    }
    
    //TODO: 邮件设置
    public function mail() {
        $this->display();
    }
    
    //TODO: 淘宝设置
    public function top() {
    	
    	$this->display();
    }
    
    //TODO: 水印设置
    public function wastermark() {
    	$this->display();
    }
    
    public function edit() {
        $setting = $this->_post('setting', ',');
        foreach ($setting as $key => $val) {
            $val = is_array($val) ? serialize($val) : $val;
            $this->_mod->where(array('name' => $key))->save(array('data' => $val));
        }
        $type = $this->_post('type', 'trim', 'index');
        $this->success(L('operation_success'));
    }

    public function ajax_mail_test() {
        $email = $this->_get('email', 'trim');
        !$email && $this->ajaxReturn(0);
        //发送
        $mailer = mailer::get_instance();
        if ($mailer->send($email, L('send_test_email_subject'), L('send_test_email_body'))) {
            $this->ajaxReturn(1);
        } else {
            $this->ajaxReturn(0);
        }
    }

}