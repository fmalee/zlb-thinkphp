<?php
return array(
    /* 路由规则配置 */
	'URL_MODEL' => 2,
	'URL_ROUTER_ON'   => true, //开启路由
	'URL_CASE_INSENSITIVE' => false, // 默认false 表示URL区分大小写 true则表示不区分大小写
	'URL_HTML_SUFFIX' => '', // URL伪静态后缀设置
	'URL_PATHINFO_DEPR' => '/', //PATHINFO模式下，各参数之间的分割符号
	'URL_ROUTE_RULES' => array( //定义路由规则
		//配货单
		'/^Seller\/Invoice\/(\d+)$/' => 'Seller/Invoice/trade?id=:1',
		'/^Seller\/Invoice\/read\/(\d+)$/' => 'Seller/Invoice/read?id=:1',
		'/^Seller\/Goods\/(\d+)$/'   => 'Seller/Goods/read?id=:1',
		'/^Seller\/Item\/(\d+)$/'   => 'Seller/Item/read?id=:1',
	),
);