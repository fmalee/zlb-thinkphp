<?php
return array(
    /* 项目设定 */
    'APP_STATUS' => 'debug', // 应用调试模式状态 调试模式开启后有效 默认为debug 可扩展 并自动加载对应的配置文件
    'SHOW_PAGE_TRACE' => 0, //显示调试信息
    'LOG_RECORD' => true, //是否记录日志信
    'LOG_LEVEL'  =>'EMERG,ALERT,CRIT,ERR,WARN', // 允许记录的日志级别
    'APP_FILE_CASE' => true, // 是否检查文件的大小写 对Windows平台
    'APP_AUTOLOAD_PATH' => 'COM', // 自动加载机制的自动搜索路径,注意搜索顺序
	'TAGLIB_PRE_LOAD' => 'mc,html', //自动加载标签
	'LOAD_EXT_CONFIG' => 'route,version', //扩展配置
	
    'APP_GROUP_LIST' => 'Seller,Admin,Member', // 项目分组设定,多个组之间用逗号分隔,例如'Home,Admin'
    'APP_GROUP_MODE' => 1, // 分组模式 0 普通分组 1 独立分组，本项目不允许使用普通分组
	'DEFAULT_GROUP' => 'Member', // 默认分组
	'APP_GROUP_PATH' => 'Modules', // 分组目录 独立分组模式下面有效
    /* 数据缓存设置 */
    'DATA_CACHE_SUBDIR' => true, // 使用子目录缓存 (自动根据缓存标识的哈希创建子目录)
    'DATA_PATH_LEVEL' => 3, // 子目录缓存级别
	
	'COOKIE_PREFIX' => 'mc_', // Cookie前缀

	'OUTPUT_ENCODE'=>false, //出现330错误后关闭压缩
	
	/* 模板引擎设置 */
    //'TMPL_ACTION_ERROR' => APP_PATH . 'Template/Public/error.html', // 默认错误跳转对应的模板文件
    //'TMPL_ACTION_SUCCESS' => APP_PATH . 'Template/Public/success.html', // 默认成功跳转对应的模板
	
    //'VAR_AJAX_SUBMIT' => 'inajax', // 默认的AJAX提交变量
	
	/* 多语言支持 */
    //'LANG_SWITCH_ON' => false,
	//'DEFAULT_LANG' => 'zh-cn', // 默认语言
	//'LANG_AUTO_DETECT' => false, // 自动侦测语言
	//'LANG_LIST' => 'zh-cn', //必须写可允许的语言
	/* 数据库连接 */
	'DB_HOST' => 'localhost',
	'DB_NAME' => 'zlb',
	'DB_USER' => 'root',
	'DB_PWD' => '15158899866',
	'DB_PORT' => '3306',
	'DB_PREFIX' => 'mc_',
	
	/* 网站自定义参数 */
	'SITE_NAME' => '摩卡网商', //网站名称
	'SITE_DOMAIN' => 'http://t.coromachi.com', //网站域名
	'SITE_STATUS' => '1', //网站状态
	'SITE_IPBAN' => '0', //启用黑名单
	'IMG_PATH' => 'http://t.coromachi.com/static/image/', //图片路径
	'JS_PATH' => 'http://t.coromachi.com/static/js/', //JS路径
	'CSS_PATH' => 'http://t.coromachi.com/static/css/', //CSS路径
	'UPLOAD_PATH' => './Public/', //上传附件路径
	'UPLOAD_URL' => 'http://t.coromachi.com/uploadfile/', //附件URL
	'UPLOAD_FILE_RULE' => 'uniqid', //上传文件名命名规则 例如可以是 time uniqid com_create_guid 等 必须是一个无需任何参数的函数名 可以使用自定义函数
	'UPLOAD_MAX_SIZE' => '20480000', //允许上传附件大小
	'UPLOAD_ALLOW_EXT' => 'jpg,gif,png,jpeg,swf', //允许上传附件类型
	'AUTHCODE' => 'zfyIOGxD@#', //authcode加密函数密钥
	'HTML_PATH' => '/Html/', //HTML静态文件目录
	'TEMPLATE_PATH' => APP_PATH . 'Template/', //模板路径

    /* 淘宝开放平台设定 */
	//'APP_KEY' => '21026410',
	//'SECRET_KEY' => 'a867837bce8df4bc6d4e6a11723bda10',
	'APP_KEY' => '12395060',
	'SECRET_KEY' => 'b33bee16fd5622daf8aacc7000f4f1f6',
);