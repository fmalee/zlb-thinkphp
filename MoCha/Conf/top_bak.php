<?php
return array(
    /* 淘宝开放平台设定 */
	'GETEWAY_URL' => 'http://gw.api.taobao.com/router/rest',
	'TQL_URL' => 'http://gw.api.taobao.com/tql/2.0/json',
	'STREAM_URL' => 'http://stream.api.taobao.com/stream',
	'ASYN_URL' => 'http://127.0.0.1/index.php?m=taobao&c=stream&a=asyn',
	'DISCARD_URL' => 'http://127.0.0.1/index.php?m=taobao&c=stream&a=discardinfo',
	'APP_KEY' => '21026410',
	'SECRET_KEY' => 'a867837bce8df4bc6d4e6a11723bda10',
	'FORMAT' => 'json',
	'SIGNMETHOD' => 'md5',
	'API_VER' => '2.0',
	'SDK_VER' => 'top-sdk-php-20120627',
	'CHECK' => true,
);