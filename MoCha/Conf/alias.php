<?php

/**
 * 别名定义
 * Some rights reserved：abc3210.com
 * Contact email:admin@abc3210.com
 */
return array(
	//淘宝开放平台
	"Taoapi" => LIBRARY_PATH.'COM/Tb/Taoapi.class.php',
	//淘宝开放平台
	"Top" => LIBRARY_PATH.'COM/Tb/Top.class.php',
	//分页
	"Page" => LIBRARY_PATH.'ORG/Util/Page.class.php',
    //邮件
    'PHPMailer' => LIBRARY_PATH.'ORG/Util/class.phpmailer.php',
    //Pclzip
    'Pclzip' => LIBRARY_PATH.'ORG/Util/Pclzip.class.php',
    //UploadFile
    "UploadFile" => LIBRARY_PATH.'ORG/Util/UploadFile.class.php',
    //文件操作类 Dir
    "Dir" => LIBRARY_PATH.'ORG/Util/Dir.class.php',
    //树
    "Tree" => LIBRARY_PATH.'ORG/Util/Tree.class.php',
    //RBAC
    "RBAC" => LIBRARY_PATH.'ORG/Util/RBAC.class.php',
    //Input 输入数据管理类
    "Input" => LIBRARY_PATH.'ORG/Util/Input.class.php',
    //Image 图像处理类
    "Image" => LIBRARY_PATH.'ORG/Util/Image.class.php',
    //Form表单
    "Form" => LIBRARY_PATH.'ORG/Util/Form.class.php',
    //Html静态生成类
    "Html" => LIBRARY_PATH.'ORG/Util/Html.class.php',
    //Url地址
    "Url" => LIBRARY_PATH.'ORG/Util/Url.class.php',
    //Content
    "Content" => LIBRARY_PATH.'ORG/Util/Content.class.php',
    //模块安装
    "Module" => LIBRARY_PATH.'ORG/Util/Module.class.php',
    //附件
    "Attachment" => LIBRARY_PATH.'ORG/Util/Attachment.class.php',
    //Ftp
    "Ftp" => LIBRARY_PATH.'ORG/Util/Ftp.class.php',
);
?>
