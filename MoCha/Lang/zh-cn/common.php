<?php
return array(
	/* 淘宝系统变量 */
	//trade.shipping_type
	'free'=>'卖家包邮',
	'ems'=>'EMS',
	'ORDINARY'=>'平邮',
	'post'=>'平邮',
	'FAST'=>'快递',
	'express'=>'快递',
	'virtual'=>'虚拟物品',
);
?>