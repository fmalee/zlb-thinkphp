<?php
/**
 * 监听类
 */
ini_set('max_execution_time', '0'); //防止超时
//ignore_user_abort(true); //不中断进程
date_default_timezone_set('PRC'); //设置时区
ini_set("error_reporting","E_ALL & ~E_NOTICE"); //设置错误报告

require('Listen.php'); //加载类文件
$listen = new Listen(0, 5);

define('ListenPID', getmypid());
$start_time = time();
$rate = 1000000 * 0.01; // 微秒 / 消息接受速度 (0.01约每秒100条

$listen->msg('PID:'.ListenPID);  //记录进程PID
$listen->living($start_time, 1);  //记录开始监听时间

$i = 0;
while (true) {
	$listen->msg('连接远程服务器...');
	$listen->connect();
	
	if (!$listen->check()) {
		$listen->msg('远程服务器链接失败,重连...');
		sleep(3);
		continue;
	} else {
		$listen->msg('连接远程服务器成功！');
	}
	
	$listen->msg('开始身份验证...');
	$listen->auth();
	
	while (!$listen->is_eof()) {
		$_now = time();
		$need_exit = false;
		$data = $listen->get_msg();
		if ($data === null) {
			// continue ; // null过多可能包含错误信息
		} elseif ($data === false) {
			$listen->msg('消息包有损失');
		} else {
			// 处理接收到有效数据包
			$code = $data['code'];
			$msg = $data['msg'];
			
			switch ($data ['code']) {
				case 200 : // 连接成功,准备读取数据流
					//$listen->lock();  //锁定进程
					$listen->msg('授权通过,开始接收消息包...');
					$listen->discard();  //轮询丢失的消息包
					break;
					
				case 201 : // 服务端会在没有消息产生情况下，间隔一分钟发送一个心跳包。
					// $listen->msg('201');
					//$listen->msg('心跳中,有'.(int)$msg . '个消息包');
					break;

				case 202 : // 业务消息包
					//$listen->msg('接收到业务消息包');
					$listen->msg(json_encode($msg));
					$listen->asyn($msg); //业务处理
					break;

				case 203 : // 有丢包，需要调用API补全(15分钟的数据，可以接受)
					$listen->msg('业务消息有丢包:' . date('Y-m-d H:i:s', $msg['begin']/1000) . ' / '. date('Y-m-d H:i:s', $msg['end']/1000));
						
					//系统消息包只接收最近15分钟的数据，使用轮询方式自行获取
					//$listen->discardinfo($msg);  //丢包处理
					break;

				case 101 : // 连接到达最大时间
					$listen->msg('连接过久，断开重连');
					$listen->close();
					continue 3; // 重连
					break;

				case 102 : // 服务端在升级
					$listen->msg('服务端升级，需断开 '. $msg.'秒后重连');
					$listen->close();
					sleep((int) $msg); // 间隔
					continue 3; // 重连
					break;

				case 103 : // 由于某些原因服务端出现了一些问题，需要断开客户端
					$listen->msg('服务端未知，需断开 '. $msg.'秒后重新连接' );
					$listen->close();
					sleep((int)$msg); // 间隔
					continue 3; // 重连
					break;

				case 104 : //由于客户端发起了重复的连接请求，服务端会把前一个连接主动断开
					$listen->msg ('前一个连接被覆盖');
					$need_exit = true;
					break 2;
					//break;

				case 105 : //网络状况不太好，或者app接收消息太慢。导致服务端有大量的消息积压，这种情况下服务端会断开连接。
					$listen->msg('接受速度过慢或者网络差 R:'.$rate);
					$listen->close();
					if ($rate >= 1000) $rate *= 0.5; // 提高接收速度
					sleep(3); // 间隔
					continue 3; // 重连
					break;

				case 106 : //由于同一个类型另外一个请求指定了，那么旧的连接中这个类型就会被踢出去，这个类型的消息会发送到新的连接上
					$listen->msg('106被踢出');
					break;
			}
				
			$i++;
		}
		
		$start_time = $listen->living($start_time);  //记录最后存活时间
		if (!$start_time) {
			$listen->msg('后台强制退出');
			exit();
		}
		
		usleep($rate); // stream read continue
	}
	$listen->close();
	$listen->msg('连接关闭');
	if ($need_exit) {
		$listen->msg('连接被覆盖');
		exit();
	}
}
exit('连接被覆盖');
?>
